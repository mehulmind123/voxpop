//
//  ApplicationConfig.h
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#ifndef ApplicationConfig_h
#define ApplicationConfig_h

#define CBundleId                               @"com.voxpop"



/*======== Google =========*/
#define GoogleClientID              @"475422106179-mvpv121782cmdo2m60f43qtvqtam5qfg.apps.googleusercontent.com"



/*======== LinkedIn =========*/
#define CLinkedInCallBack                       @"LinkedInCallBack"
#define CLinkedInAccessToken                    @"LinkedInAccessToken"
#define CLinkedInState                          @"8200785251"
#define CLinkedInAppId                          @"5160935"

#define CLinkedInClientId                       @"812pihclzhcyve"
#define CLinkedInClientSecret                   @"xrMWJOGJ1lNptxxN"
#define CLinkedInRedirectURL                    @"https://www.voxpopapp.com/linkedin_callback.php"  //...Live
//#define CLinkedInRedirectURL                    @"http://192.168.1.59/voxpop_new/linkedin_callback.php"   //...Local
//#define CLinkedInRedirectURL                    @"http://180.211.104.102/voxpop_new/linkedin_callback.php"    //...VPN



#endif /* ApplicationConfig_h */

//
//  ApplicationMessages.h
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#ifndef ApplicationMessages_h
#define ApplicationMessages_h

/*======== Loader Messages =========*/
#define CMessageCongratulation              @"Congratulations!"
#define CMessageSorry                       @"Sorry!"
#define CMessageLoading                     @"Loading..."
#define CMessageSearching                   @"Searching..."
#define CMessageVerifying                   @"Verifying..."
#define CMessageWait                        @"Please Wait..."
#define CMessageUpdating                    @"Updating..."
#define CMessageAuthenticating              @"Authenticating..."
#define CMessageErrorInternetNotAvailable   @"Intenet Connection Not Available!\n Please Try Again Later."
#define CMessageNoResultFound               @"Nothing to see here yet!"
#define CErrorTapToRetry                    @"Something wrong here...\ntap anywhere to refresh the page."



/*======== LRF =========*/

#define CMessageBlankEmail                  @"email address can not be blank."
#define CMessageValidEmail                  @"Please enter a valid email address."
#define CMessageBlankPassword               @"password can not be blank."
#define CMessageEmailPasswordNotReconize    @"email and/or password is not recognized."
#define CMessageVerifyAccount               @"Your account is not verified. Please verify your account."
#define CMessageSendCode                    @"Send verification code"

#define CMessageBlankUserName               @"user name can not be blank."
#define CMessageBlankConfirmPassword        @"Please confirm your password."
#define CMessageAlphaNumbricPassword        @"Please enter minimum 6 characters alphanumeric password."
#define CMessageConfirmPasswordNotMatch     @"Pair of New Password and Confirm Password doesn’t match."
#define CMessageBlankDOB                    @"Please select your date of birth."
#define CMessageBlankGender                 @"Please select your gender."
#define CMessageBlankTopic                  @"Please select atleast one topic."
#define CMessageBlankZipCode                @"zipcode can not be blank."
#define CMessageBlankPersonalProfile        @"Please complete your profile."
#define CMessageSelectTermsAndCondition     @"Please agree to our terms & Conditions and Privacy Policy"

#define CMessageEmailAlreadyExist           @"The email you are trying to register with is already associated with us. Please try another email."

#define CMessageSendVerificationCode        @"We have sent you the verification code on your registered email address."

#define CMessageEmailNotRegister            @"The email you entered is not registered with us."

#define CMessageSendResetPasswordLink       @"We have sent you a link to reset your password on your email. Please check."

#define CMessageBlankVerificationCode       @"verification code can not be blank."
#define CMessageWrongVerificationCode       @"Please enter correct verification code."

#define CMessageBlankOldPassword            @"Old Password can not be blank."
#define CMessageWrongOldPassword            @"Please enter correct Old Password."

#define CMessageBlankNewPassword            @"New Password can not be blank."
#define CMessageAlphaNumbricNewPassword     @"New Password must be minimum 6 character alphanumeric."
#define CMessagePasswordChanageSuccess      @"Password changed successfully."

#define CMessageLogout                      @"Are you sure you want to logout? You will not receive any push notifications for the  surveys."

#define CMessageSelectTopic                 @"Please select atleast 1 topic."

#define CMessageSurveyTaken                 @"Sorry, you can not view/post comment on the survey until you take the survey."



/*======== Other =========*/
#define CMessageBlockCustomerTitle          @""
#define CMessageBlockCustomerMessage        @"Are you sure you want to block this customer? You will not receive any surveys from this customer."

#define CMessageDeleteCommentTitle          @""
#define CMessageDeleteCommentMessage        @"Are you sure you want to delete this comment?"

#define CMessageQuitSurveyTitle             @""
#define CMessageQuitSurveyMessage           @"Are you sure you want to quit? Your survey answers will not be saved if you proceed."

#define CMessageSurveyAcknowledge1Title      @""
#define CMessageSurveyAcknowledge1Message    @"Thank you very much for expressing your valued opinion on the matter! See what other users are saying."

#define CMessageSurveyAcknowledge2Title      @""
#define CMessageSurveyAcknowledge2Message    @"Thank you very much for expressing your valued opinion on the matter! We noticed that you took a survey on a topic not previously chosen by you. Would you like to add this topic to your Topics of Interest?"

#define CMessageSurveyMissedAnsTitle        @""
#define CMessageSurveyMissedAnsMessage      @"We want to hear your opinion on all of our questions, kindly respond to the ones you may have missed."

#define CMessageRemoveAccessCode            @"Are you sure you want to remove this access code from the list?"
#define CMessageUnblockCustomer             @"Are you sure you want to unblock this customer?"

#define CMessageSelectAnswer                @"Please select at least one answer."

#define CMessageProfileUpdate               @"Profile updated successfully"

#define CMessageUnableToPostComment         @"Sorry, you can not view/post comment on the survey until you take the survey."

#define CMessageLiveSurvey                  @"You are invited to view live surveys on Topics  of Interest other than your own that you may wish to offer your opinion"

#define CMessageCompletedSurvey             @"You are invited to view the last 30 completed surveys on Topics of Interest other than your own"


#endif /* ApplicationMessages_h */

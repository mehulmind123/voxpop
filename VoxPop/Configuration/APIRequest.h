//
//  APIRequest.h
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

//#define BASEURL         @"http://192.168.1.59/voxpop_new/public/api/v1/"
//#define BASEURL         @"http://192.168.1.59/voxpop_new/api/v1/"
//#define BASEURL         @"http://180.211.104.102/voxpop_new/public/api/v1/"
//#define BASEURL         @"http://180.211.104.102/voxpop_new/api/v1/"  // VPN URL

//#define BASEURL         @"http://192.168.1.59/voxpop_new/api/v1/"  //  Local URL
//#define BASEURL         @"https://www.voxpopapp.com/api/v1/"

//#define BASEURL         @"https://www.voxpopapp.com/api/v3/"

#define BASEURL           @"https://www.voxpopapp.com/api/v4/" //  Live URL New
//#define BASEURL         @"http://192.168.1.29/voxpop/api/v1/"
//#define BASEURL         @"http://itrainacademy.in/voxpop/api/v3/"  //  itrainacademy URL



//********** Request, Response Constant
#define CVersion    @"1"
#define CLimit      @20

#define CJsonStatus             @"status"
#define CJsonUserStatus         @"user_status"
#define CJsonStatusCode         @"status_code"
#define CJsonMessage            @"message"
#define CJsonData               @"data"
#define CJsonTimestamp          @"timestamp"
#define CJsonMeta               @"meta"


#define CStatusZero             0
#define CStatusOne              1
#define CStatusTwo              2
#define CStatusThree            3
#define CStatusFour             4
#define CStatusFive             5
#define CStatusNine             9
#define CStatusTen              10
#define CStatusTwoHundred       200  // OK
#define CStatusTwoHundredOne    201  // Created
#define CStatusFourHundred      400  // Error
#define CStatusFourHundredOne   401  // Unauthorized
#define CStatusFourHundredTwo   402  // User Email Not Verified
#define CStatusFourHundredTen   410  // Under Maintanance



//********** API Tag

#define CAPITagGetCMSDetails                            @"cms"
#define CAPITagGetGeneralDetails                        @"general-data"

#define CAPITagPostTopics                               @"topics"
#define CAPITagPostAddTopics                            @"topics/add"

#define CAPITagPostSignIn                               @"login"
#define CAPITagPostSignup                               @"sign-up"


#define CAPITagPostResendCode                           @"resend-code"
#define CAPITagPostForgotPassword                       @"forgot-password"
#define CAPITagPostVerifyEmail                          @"verify-email"

#define CAPITagPostVerifyOTP                            @"verify-otp"
#define CAPITagPostResetPassword                        @"reset-password"
#define CAPITagPostChangePassword                       @"change-password"
#define CAPITagPostEditProfile                          @"change-profile"
#define CAPITagGetProfile                               @"profile"

#define CAPITagPostLinkSocialAccount                    @"link-account"
#define CAPITagPostCheckSocialAccount                   @"social-link-account"


#define CAPITagPostAddAccessCode                        @"access-code/add"
#define CAPITagGetDeleteAccessCode                      @"access-code/delete"
#define CAPITagPostAccessCode                           @"access-code"

#define CAPITagPostBlockedCustomer                      @"blocked-customer"

#define CAPITagPostAddDeviceToken                       @"device-token"
#define CAPITagPostDeleteDeviceToken                    @"delete-device-token"

#define CAPITagPostChangeAllowNotification              @"update-notification"

#define CAPITagPostAllSurvey                            @"all-survey"
#define CAPITagPostMySurvey                             @"my-survey"
#define CAPITagPostNewSurvey                            @"suggested-surveys"
#define CAPITagPostAddComment                           @"comment/add"
#define CAPITagPostSurveyComments                       @"survey-comments"
#define CAPITagPostDeleteComments                       @"comment/delete"

#define CAPITagPostAddBlockCustomer                     @"blocked-customer/add"
#define CAPITagGetBlockCustomerList                     @"blocked-customer"
#define CAPITagPostBlockUnblockCustomer                 @"customer-block-unblock"
#define CAPITagPostAddTopicsFromSurvey                  @"survey/topic/add"
#define CAPITagPostSubmitSurvey                         @"survey/add"
#define CAPITagGetSurveyDetails                         @"survey/detail"
#define CAPITagGetSurveyResults                         @"survey-result"
#define CAPITagGetUserRespondedCount                    @"user-responded-count"





typedef void(^SocialUserInfoBlock)(id userInfo, NSError *error);


@interface APIRequest : NSObject

+ (id)request;

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error;

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error andHandler:(void(^)(BOOL retry))handler;

- (NSError *)errorFromDataTask:(NSURLSessionDataTask *)task;

- (BOOL)isJSONDataValidWithResponse:(id)response;

- (BOOL)isJSONStatusValidWithResponse:(id)response;

- (BOOL)isUserNotVerifiedWithResponse:(id)response;

- (BOOL)isUserNotVerifiedWithResponseUsingUserStatus:(id)response;

- (BOOL)checkResponseStatus:(id)response;

#pragma mark -
#pragma mark - General

- (void)loadGeneralData_completed:(void (^)(id responseObject, NSError *error))completion;

- (void)loadCMS_completed:(void (^)(id responseObject, NSError *error))completion;

- (void)loadTopicsListWithOffset:(NSInteger)offset completed:(void (^)(id responseObject, NSError *error))completion;

#pragma mark -
#pragma mark - Socail SignUp

- (void)signUpWithFacebookFromViewController:(UIViewController *)vc withCompletion:(SocialUserInfoBlock)uBlock;

- (void)signUpWithGoogleFromViewController:(UIViewController *)vc withCompletion:(SocialUserInfoBlock)uBlock;

- (void)loginWithLinkedIn:(NSURL *)url completed: (void (^)(id responseObject,NSError *error))completion;

- (void)getLinkedInUserProfile:(void (^)(id responseObject,NSError *error))completion;

- (void)checkSocialAccount:(NSString*)socialId socialType:(NSNumber*)socialType email:(NSString*)email  completed:(void (^)(id responseObject, NSError *error))completion;

- (void)getLinkedInEmail:(void (^)(id responseObject, NSError *error))completion;

#pragma mark -
#pragma mark - LRF

- (void)signInWithEmail:(NSString *)email andPassword:(NSString *)password completed:(void (^)(id responseObject, NSError *error))completion;

- (void)signupWithEmail:(NSString *)email andPassword:(NSString *)password andUserName:(NSString *)userName andDOB:(NSString*)dob andGender:(NSString*)gender andAccessCode:(NSString*)accessCode andSelectedTopics:(NSString*)selectedTopics andMrgStatus:(NSString*)mrgStatus andZipCode:(NSString*)zipCode socialId:(NSString*)socialId socialType:(NSNumber*)socialType completed:(void (^)(id responseObject, NSError *error))completion;

- (void)forgotPasswordWithEmail:(NSString *)email completed:(void (^)(id responseObject, NSError *error))completion;

- (void)changePasswordWithNewPassword:(NSString *)newPassword andOldPassword:(NSString *)oldPassword completed:(void(^)(id responseObject, NSError *error))completion;


- (void)verifyEmailAccount:(NSString*)email verificationCode:(NSString *)verificationCode completed:(void(^)(id responseObject, NSError *error))completion;


- (void)resendCode:(NSString *)email completed:(void(^)(id responseObject, NSError *error))completion;

- (void)editProfileWithUserName:(NSString *)userName dob:(NSString *)editDOB andMaritalStatus:(NSInteger)maritalStatusId completed:(void (^)(id responseObject, NSError *error))completion;


- (void)userProfile_completed:(void (^)(id responseObject, NSError *error))completion;

- (void)addAccessCode:(NSString*)accessCode completed:(void (^)(id responseObject, NSError *error))completion;

- (void)deleteAccessCode:(NSInteger)accessCodeId completed:(void (^)(id responseObject, NSError *error))completion;

- (void)loadAccessCodeWithOffset:(NSInteger)offSet completed:(void (^)(id responseObject, NSError *error))completion;

- (void)linkSocialAccount:(NSString*)socialId socialType:(NSNumber*)socialType  completed:(void (^)(id responseObject, NSError *error))completion;

- (void)loadBlockCustomer_completed:(void (^)(id responseObject, NSError *error))completion;

- (void)editInterestedTopic:(NSString*)strTopics completed:(void (^)(id responseObject, NSError *error))completion;

- (void)changeAllowedNotification:(NSNumber*)iAllowedNotification completed:(void (^)(id responseObject, NSError *error))completion;

- (void)addDeviceToken:(NSString*)token completed:(void (^)(id responseObject, NSError *error))completion;

- (void)deleteDeviceToken:(NSString*)token completed:(void (^)(id responseObject, NSError *error))completion;

- (void)getBlockUserList_completed:(void (^)(id responseObject, NSError *error))completion;

- (void)blockUnblockUser:(NSNumber*)userId status:(BOOL)isBlock completed:(void (^)(id responseObject, NSError *error))completion;

#pragma mark -
#pragma mark - Survey

- (NSURLSessionDataTask *)getAllSurveyWithType:(NSInteger)type  timestamp:(NSString*)timeStamp completed:(void (^)(id responseObject, NSError *error))completion;

- (NSURLSessionDataTask *)getMySurveyWithTimestamp:(NSString*)timeStamp completed:(void (^)(id responseObject, NSError *error))completion;

- (NSURLSessionDataTask *)getNewSurveyWithTimestamp:(NSString*)timeStamp completed:(void (^)(id responseObject, NSError *error))completion;

- (NSURLSessionDataTask *)getSurveyCommentListWithSurveyId:(NSNumber *)surveyId timestamp:(NSString *)timeStamp completed:(void (^)(id responseObject, NSError *error))completion;

- (void)submitSurveyWithSurveyId:(NSNumber *)surveyId surveyQA:(NSArray *)surveyQA completed:(void (^)(id responseObject, NSError *error))completion;

- (void)getSurveyDetails:(NSNumber *)surveyId completed:(void (^)(id responseObject, NSError *error))completion;

- (void)getSurveyResults:(NSNumber *)surveyId completed:(void (^)(id responseObject, NSError *error))completion;

- (void)addSurveyCommentWithSurveyId:(NSNumber*)surveyId  comment:(NSString*)comment completed:(void (^)(id responseObject, NSError *error))completion;

- (void)deleteComment:(NSNumber*)commentId completed:(void (^)(id responseObject, NSError *error))completion;

- (void)addTopicFromSurveyWithSurveyId:(NSNumber *)surveyId completed:(void (^)(id responseObject, NSError *error))completion;

- (NSURLSessionDataTask *)getUserRespondedCount:(NSNumber*)surveyId completed:(void (^)(id responseObject, NSError *error))completion;


#pragma mark -
#pragma mark - Save In Local

- (void)saveLoginUserToLocal:(id)responseObject;

@end

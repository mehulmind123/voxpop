
//
//  APIRequest.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "APIRequest.h"

static APIRequest *request = nil;

@interface APIRequest () <GIDSignInUIDelegate, GIDSignInDelegate>
{
    UIViewController *currentVC;
    
    SocialUserInfoBlock uInfoBlock;
}
@end

@implementation APIRequest

+ (id)request
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
                  {
                      request = [[APIRequest alloc] init];
                      [[MIAFNetworking sharedInstance] setBaseURL:BASEURL];
                      
                      NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:[[[[MIAFNetworking sharedInstance] sessionManager] responseSerializer] acceptableStatusCodes]];
                      
                      [indexSet addIndex:400];
                      [indexSet addIndex:401];
                      [indexSet addIndex:402];
                      
                      if(IS_IPHONE_SIMULATOR)
                      {

                          [indexSet addIndex:201];
                        
                          [indexSet addIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(300, 300)]];
                      }
                      
                      
                      
                      [[[MIAFNetworking sharedInstance] sessionManager] responseSerializer].acceptableStatusCodes = indexSet;
                      
                    });
    
    return request;
}



- (void)failureWithAPI:(NSString *)api andError:(NSError *)error
{
    NSLog(@"API Error(%@) == %@", api, error);

    if(IS_IPHONE_SIMULATOR)
    {
        if (error.code != -999 && ![error.localizedDescription isEqualToString:@"cancelled"] && ![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
            [CustomAlertView iOSAlert:@"" withMessage:error.localizedDescription onView:[UIApplication topMostController]];
    }
}

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error andHandler:(void(^)(BOOL retry))handler
{
    NSLog(@"%@ API Error == %@", api, error);
    
    if (error.code != -999 && ![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
    {
        
        [[UIApplication topMostController] alertWithAPIErrorTitle:@"ERROR!" message:@"An error has occured. Please check your network connection or try again." handler:^(NSInteger index, NSString *btnTitle)
        {
            
            if (handler) handler(index == 0);
            
        }];
    }
}

- (NSError *)errorFromDataTask:(NSURLSessionDataTask *)task
{
    return ((NSHTTPURLResponse *)task.response).statusCode == 200 ? nil : [NSError errorWithDomain:@"" code:((NSHTTPURLResponse *)task.response).statusCode userInfo:nil];
}

- (BOOL)isJSONDataValidWithResponse:(id)response
{
    id data = [response valueForKey:CJsonData];
    
    if (!data)
    {
      return [self isJSONStatusValidWithResponse:response];
    }
    
    
    if ([data isEqual:[NSNull null]]) {
        return NO;
    }
    
    if ([data isKindOfClass:[NSString class]]) {
        if (((NSString *)data).length == 0)
            return NO;
    }
    
    if ([data isKindOfClass:[NSArray class]]) {
        if (((NSArray *)data).count == 0)
            return NO;
    }
    
    if ([data isKindOfClass:[NSDictionary class]]) {
        if (((NSDictionary *)data).count == 0)
            return NO;
    }
    
    return [self isJSONStatusValidWithResponse:response];
}

- (BOOL)isJSONStatusValidWithResponse:(id)response
{
    if (!response)
        return NO;
    
    NSNumber *status = [[response valueForKey:CJsonMeta] numberForInt:CJsonStatus];
    
    if ([@CStatusTwoHundred isEqual:status])
        return YES;
    else if ([status isEqual:@CStatusFourHundred] && ![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
    {
        [CustomAlertView iOSAlert:@"" withMessage:[[response valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:[UIApplication topMostController]];
        
        return NO;
    }
    
    return NO;
    
}
- (BOOL)isUserNotVerifiedWithResponse:(id)response
{
    if (!response)
        return NO;
    
    NSNumber *status = [response numberForInt:CJsonStatusCode];
    
    return [status isEqual:@CStatusFourHundredTwo];
    
}
- (BOOL)isUserNotVerifiedWithResponseUsingUserStatus:(id)response
{
    if (!response)
        return NO;
    
    NSNumber *status = [[response valueForKey:CJsonMeta] numberForJson:@"user_status"];
    
     return [status isEqual:@4];;
    
}

- (BOOL)checkResponseStatus:(id)response
{
    switch ([[response valueForKey:CJsonMeta] intForKey:CJsonUserStatus])
    {
        case CStatusTwo:
        {
            //...Inactive User
            if ([UIApplication userId])
            {
                [appDelegate logoutUser];
                
                if(![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
                    [CustomAlertView iOSAlert:@"Inactive User" withMessage:nil onView:[UIApplication topMostController]];
            }
            
            return YES;
            break;
        }
        case CStatusNine:
        {
            //...Under Maintenance
            [appDelegate logoutUser];
            return YES;
            break;
        }
        default:
        {
            return NO;
            break;
        }
    }
    
    
}

- (BOOL)isSuccessHTTPStatusWithDataTask:(NSURLSessionDataTask *)task withResponse:(id)response
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
    
    if (httpResponse.statusCode == CStatusFourHundredOne)
    {
        //...Inactive User , delete user , Token expired
        if ([UIApplication userId])
        {
            [appDelegate logoutUser];
            
            if(![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
                [CustomAlertView iOSAlert:@"Inactive User" withMessage:nil onView:[UIApplication topMostController]];
        }
        
        return NO;
    }
    else if (![@[@CStatusTwoHundred,@CStatusFourHundredTwo] containsObject:[NSNumber numberWithInteger:httpResponse.statusCode]])
    {
        if(![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
            
            [CustomAlertView iOSAlert:@"" withMessage:[response stringValueForJSON:CJsonMessage] onView:[UIApplication topMostController]];
        
        return NO;
        
    }
//    else if([self checkResponseStatus:response])
//            return NO;

    
    return YES;
}

#pragma mark -
#pragma mark - General

- (void)loadGeneralData_completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] GET:CAPITagGetGeneralDetails parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject])
         {
             if ([self isJSONDataValidWithResponse:responseObject])
             {
                 NSDictionary *dict = [responseObject valueForKey:CJsonData];
                 
                 //[self saveReligionInLocal:[dict objectForKey:@"religion"]];
                 //[self saveRaceInLocal:[dict objectForKey:@"race"]];
                 [self saveMaritalStatusInLocal:[dict objectForKey:@"marital_status"]];
             }
             
             if (completion)
                 completion(responseObject, nil);
   
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagGetGeneralDetails andError:error];
         
         if (completion)
             completion(nil, error);
     }];

    
}
- (void)loadCMS_completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] GET:CAPITagGetCMSDetails parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject])
         {
             [CUserDefaults setValue:[responseObject valueForKey:CJsonData] forKey:UserDefaultCMS];
             [CUserDefaults synchronize];
             
             if(completion)
                 completion(responseObject, nil);
   
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagGetCMSDetails andError:error];
         if (completion)
             completion(nil, error);
     }];
}


- (void)loadTopicsListWithOffset:(NSInteger)offset completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagPostTopics parameters:@{@"offset":[NSNumber numberWithInteger:offset]} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject])
         {
             if ([self isJSONDataValidWithResponse:responseObject])
             {
                 if (offset == 0)
                 {
                     [TBLCategory deleteAllObjects];
                     [TBLSubCategory deleteAllObjects];
                 }
                 [self saveCategoryInLocal:[responseObject valueForKey:CJsonData]];
             }
             
             if (completion)
                 completion(responseObject, nil);
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}


#pragma mark
#pragma mark - Social SignUp

- (void)signUpWithFacebookFromViewController:(UIViewController *)vc withCompletion:(SocialUserInfoBlock)uBlock
{
    uInfoBlock = uBlock;

    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
//    [loginManager logOut];
    
    [loginManager logInWithPermissions:@[@"public_profile", @"email"] fromViewController:vc handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if (result.token)
         {
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, name, picture, email"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id userinfo, NSError *error)
              {
                  if (self->uInfoBlock)
                  {
                      self->uInfoBlock(userinfo, error);
                      self->uInfoBlock = nil;
                  }
              }];
         }
         else
         {
             if (result.isCancelled)
                 NSLog(@"Cancelled Facebook Login: %@", result);
             
             if (self->uInfoBlock)
             {
                 self->uInfoBlock(nil, error);
                 self->uInfoBlock = nil;
             }
         }
     }];
}

- (void)signUpWithGoogleFromViewController:(UIViewController *)vc withCompletion:(SocialUserInfoBlock)uBlock
{
    if (vc)
    {
        currentVC = vc;
        uInfoBlock = uBlock;
        
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        GIDSignIn *signIn = [GIDSignIn sharedInstance];
        [signIn signOut];
        signIn.shouldFetchBasicProfile = YES;
        signIn.delegate = self;
        signIn.uiDelegate = self;
        
        [signIn signIn];
    }
}

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [currentVC presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    [currentVC dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    if (uInfoBlock)
    {
        uInfoBlock(user, error);
        uInfoBlock = nil;
    }
}


- (void)loginWithLinkedIn:(NSURL *)url completed: (void (^)(id responseObject,NSError *error))completion
{
    NSString const *LinkedInPermissions = @"r_liteprofile%20r_emailaddress%20w_member_social";

    NSString *strUrl = [NSString stringWithFormat:@"https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code=%@&redirect_uri=%@&client_id=%@&client_secret=%@&scope=%@&state=linkedin117413",[[[url absoluteString] componentsSeparatedByString:[NSString stringWithFormat:@"%@://oauth-response?code=",CBundleId]] objectAtIndex:1],CLinkedInRedirectURL,CLinkedInClientId,CLinkedInClientSecret,LinkedInPermissions];
    
    [[MIAFNetworking sharedInstance] GET:strUrl parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (completion)
            completion(responseObject,nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (completion)
            completion(nil,error);
    }];
}

- (void)getLinkedInUserProfile:(void (^)(id responseObject,NSError *error))completion
{

   NSString *strUrl = @"https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,emailAddress,profilePicture(displayImage~:playableStreams))";
    
    NSString *userURL = [strUrl stringByAppendingString:@"&oauth2_access_token=%@"];
    NSString *fullUserURL = [NSString stringWithFormat:userURL, [[NSUserDefaults standardUserDefaults] valueForKey:CLinkedInAccessToken]];
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:fullUserURL]];
    [request setHTTPMethod:@"GET"];
    NSLog(@"Network Request  : GET %@",fullUserURL);
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          NSLog(@"Response:%@ %@\n", response, error);
                                          if(error == nil)
                                          {
                                              id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                              NSLog(@"receive data %@",responseObject);
                                              
                                              completion(responseObject,nil);
                                              
                                          }
                                          else
                                          {
                                              completion(nil,error);
                                          }
                                          
                                      }];
    
    [dataTask resume];
}
- (void)getLinkedInEmail:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSString *strUrl = @"https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))";
    
    NSString *userURL = [strUrl stringByAppendingString:@"&oauth2_access_token=%@"];
    NSString *fullUserURL = [NSString stringWithFormat:userURL, [[NSUserDefaults standardUserDefaults] valueForKey:CLinkedInAccessToken]];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:fullUserURL]];
    [request setHTTPMethod:@"GET"];
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          NSLog(@"Response:%@ %@\n", response, error);
                                          if(error == nil)
                                          {
                                              id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                              NSLog(@"receive data %@",responseObject);
                                              
                                              completion(responseObject,nil);
                                              
                                          }
                                          else
                                          {
                                              completion(nil,error);
                                          }
                                          
                                      }];
    
    [dataTask resume];
    
}


- (void)checkSocialAccount:(NSString*)socialId socialType:(NSNumber*)socialType email:(NSString*)email  completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
  
    [[MIAFNetworking sharedInstance] POST:CAPITagPostCheckSocialAccount
                               parameters:@{@"social_id": socialId,
                                            @"social_type":socialType,
                                            @"email":email.isBlankValidationPassed ? email :@""} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             completion(responseObject, nil);
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         NSString *extractedExpr = CAPITagPostCheckSocialAccount;
         [self failureWithAPI:extractedExpr andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self checkSocialAccount:socialId socialType:socialType email:email completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
    
}
#pragma mark -
#pragma mark - LRF

- (void)signInWithEmail:(NSString *)email andPassword:(NSString *)password completed:(void (^)(id responseObject, NSError *error))completion
{
    
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email,
                           @"password":password};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostSignIn parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
               completion(responseObject, nil);
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostSignIn andError:error andHandler:^(BOOL retry)
         {
             if(retry)
                 [self signInWithEmail:email andPassword:password completed:completion];
             
         }];
         
         if (completion)
             completion(nil, error);
     }];

    
}

- (void)signupWithEmail:(NSString *)email andPassword:(NSString *)password andUserName:(NSString *)userName andDOB:(NSString*)dob andGender:(NSString*)gender andAccessCode:(NSString*)accessCode andSelectedTopics:(NSString*)selectedTopics andMrgStatus:(NSString*)mrgStatus andZipCode:(NSString*)zipCode socialId:(NSString*)socialId socialType:(NSNumber*)socialType completed:(void (^)(id responseObject, NSError *error))completion
{

    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email,
                           @"password":password,
                           @"user_name":userName,
                           @"dob":dob?:@"",
                           @"gender":gender?:@"",
                           @"access_code":accessCode,
                           @"topic":selectedTopics,
                           @"zip_code":zipCode,
                           @"marital_status_id":[NSNumber numberWithInt:[mrgStatus intValue]],
                           @"timezone":[[NSTimeZone defaultTimeZone] abbreviation],
                           @"social_id":socialId.isBlankValidationPassed ? socialId : @"",
                           @"social_type": (socialType != nil) ? socialType : @"" };
   
    [[MIAFNetworking sharedInstance] POST:CAPITagPostSignup parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             [self isJSONStatusValidWithResponse:responseObject];
             completion(responseObject, nil);
         }
         

     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostSignup andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self signupWithEmail:email andPassword:password andUserName:userName andDOB:dob andGender:gender andAccessCode:accessCode andSelectedTopics:selectedTopics andMrgStatus:mrgStatus andZipCode:zipCode socialId:socialId socialType:socialType completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];

}

- (void)forgotPasswordWithEmail:(NSString *)email completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostForgotPassword parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
        
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostForgotPassword andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self forgotPasswordWithEmail:email completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];
    

}

- (void)changePasswordWithNewPassword:(NSString *)newPassword andOldPassword:(NSString *)oldPassword completed:(void(^)(id responseObject, NSError *error))completion
{
    
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"password":newPassword,
                           @"old_password":oldPassword};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostChangePassword parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostChangePassword andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                [self changePasswordWithNewPassword:newPassword andOldPassword:oldPassword completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];

}


- (void)verifyEmailAccount:(NSString*)email verificationCode:(NSString *)verificationCode completed:(void(^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email,
                           @"verification_code":verificationCode};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostVerifyEmail parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             if ([self isJSONDataValidWithResponse:responseObject])
                 [self saveLoginUserToLocal:responseObject];
             
                 completion(responseObject, nil);

             
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostVerifyEmail andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self verifyEmailAccount:email verificationCode:verificationCode completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];

}

- (void)resendCode:(NSString *)email completed:(void(^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostResendCode parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostResendCode andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self resendCode:email completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];

}


- (void)editProfileWithUserName:(NSString *)userName dob:(NSString *)editDOB andMaritalStatus:(NSInteger)maritalStatusId completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"user_name":userName,
                           @"dob":editDOB,
                           @"marital_status_id":maritalStatusId?[NSNumber numberWithInteger:maritalStatusId]:@""};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostEditProfile parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if ([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             if ([self isJSONDataValidWithResponse:responseObject])
                 [self saveLoginUserToLocal:responseObject];
             
             completion(responseObject, nil);
         }
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostEditProfile andError:error andHandler:^(BOOL retry)
          {
              if (retry) {
                  [self editProfileWithUserName:userName dob:editDOB andMaritalStatus:maritalStatusId completed:completion];
              }
          }];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)userProfile_completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] GET:CAPITagGetProfile parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject])
         {
             if([self isJSONDataValidWithResponse:responseObject])
                 [self saveLoginUserToLocal:responseObject];
             
             if(completion)
                 completion(responseObject, nil);
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagGetProfile andError:error];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)addAccessCode:(NSString*)accessCode completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostAddAccessCode parameters:@{@"access_code":accessCode} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             if ([self isJSONDataValidWithResponse:responseObject])
                 [self saveAccessCodeInLocal:@[[responseObject valueForKey:CJsonData]]];
             
             completion(responseObject, nil);
         }
         
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostAddAccessCode andError:error andHandler:^(BOOL retry)
         {
             if(retry)
                 [self addAccessCode:accessCode completed:completion];
             
         }];
         
         if (completion)
             completion(nil, error);
     }];
    
}
- (void)linkSocialAccount:(NSString*)socialId socialType:(NSNumber*)socialType  completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostLinkSocialAccount parameters:@{@"social_id":socialId,@"social_type":socialType} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             completion(responseObject, nil);
         }
         
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostLinkSocialAccount andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self linkSocialAccount:socialId socialType:socialType completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];
    
}


- (void)deleteAccessCode:(NSInteger)accessCodeId completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] GET:[NSString stringWithFormat:@"%@/%ld",CAPITagGetDeleteAccessCode,accessCodeId] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagGetDeleteAccessCode andError:error andHandler:^(BOOL retry)
         {
             if(retry)
                 [self deleteAccessCode:accessCodeId completed:completion];
             
         }];
         if (completion)
             completion(nil, error);
     }];
    
}
- (void)loadAccessCodeWithOffset:(NSInteger)offSet completed:(void (^)(id responseObject, NSError *error))completion
{
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostAccessCode parameters:@{@"offset":[NSNumber numberWithInteger:offSet]} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             if ([self isJSONDataValidWithResponse:responseObject])
                 [self saveAccessCodeInLocal:[responseObject valueForKey:CJsonData]];
             
             completion(responseObject, nil);
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagPostAccessCode andError:error];
         if (completion)
             completion(nil, error);
     }];
    
}

- (void)loadBlockCustomer_completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagPostBlockedCustomer parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
      
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagPostBlockedCustomer andError:error];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)editInterestedTopic:(NSString*)strTopics completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostAddTopics parameters:@{@"topic":strTopics} success:^(NSURLSessionDataTask *task, id responseObject)
     {
    
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostAddTopics andError:error andHandler:^(BOOL retry)
         {
             if (request) [self editInterestedTopic:strTopics completed:completion];
             
         }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)changeAllowedNotification:(NSNumber*)iAllowedNotification completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagPostChangeAllowNotification parameters:@{@"is_notify":iAllowedNotification} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagPostChangeAllowNotification andError:error];
         if (completion)
             completion(nil, error);
     }];
}

- (void)addDeviceToken:(NSString*)token completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"token":token,
                           @"device_type":@2,
                           @"timezone":[[NSTimeZone defaultTimeZone] abbreviation]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostAddDeviceToken parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagPostAddDeviceToken andError:error];
         if (completion)
             completion(nil, error);
     }];
}

- (void)deleteDeviceToken:(NSString*)token completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"token":token,
                           @"device_type":@2};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostDeleteDeviceToken parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagPostDeleteDeviceToken andError:error];
         if (completion)
             completion(nil, error);
     }];
}



- (void)getBlockUserList_completed:(void (^)(id responseObject, NSError *error))completion
{
    
    [[MIAFNetworking sharedInstance] GET:CAPITagGetBlockCustomerList parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagGetBlockCustomerList andError:error];
         if (completion)
             completion(nil, error);
     }];
}

- (void)blockUnblockUser:(NSNumber*)userId status:(BOOL)isBlock completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"id":userId,
                           @"is_block":[NSNumber numberWithBool:isBlock]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostBlockUnblockCustomer parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagPostBlockUnblockCustomer andError:error];
         if (completion)
             completion(nil, error);
     }];
}


#pragma mark -
#pragma mark - Survey

- (NSURLSessionDataTask *)getAllSurveyWithType:(NSInteger)type  timestamp:(NSString*)timeStamp completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"survey_type":[NSNumber numberWithInteger:type],
                           @"timestamp":timeStamp,
                           @"limit":CLimit};
    
   return  [[MIAFNetworking sharedInstance] POST:CAPITagPostAllSurvey parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             completion(responseObject, nil);
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagPostAllSurvey andError:error];
         if (completion)
             completion(nil, error);
     }];
}

- (NSURLSessionDataTask *)getMySurveyWithTimestamp:(NSString*)timeStamp completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"timestamp":timeStamp,
                        @"limit":CLimit};
    
    return  [[MIAFNetworking sharedInstance] POST:CAPITagPostMySurvey parameters: dict success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
                 {
                     completion(responseObject, nil);
                 }
                 
             } failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 
                 [self failureWithAPI:CAPITagPostMySurvey andError:error];
                 if (completion)
                     completion(nil, error);
             }];

}

- (NSURLSessionDataTask *)getNewSurveyWithTimestamp:(NSString*)timeStamp completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"timestamp":timeStamp,
                           @"limit":CLimit};
    
    return  [[MIAFNetworking sharedInstance] POST:CAPITagPostNewSurvey parameters: dict success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
                 {
                     completion(responseObject, nil);
                 }
                 
             } failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 
                 [self failureWithAPI:CAPITagPostNewSurvey andError:error];
                 if (completion)
                     completion(nil, error);
             }];
    
}


- (NSURLSessionDataTask *)getSurveyCommentListWithSurveyId:(NSNumber*)surveyId  timestamp:(NSString*)timeStamp completed:(void (^)(id responseObject, NSError *error))completion
{
    
    NSDictionary *dict = @{@"survey_id":surveyId,
                           @"timestamp":timeStamp};
    
    return  [[MIAFNetworking sharedInstance] POST:CAPITagPostSurveyComments parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
                 {
                     completion(responseObject, nil);
                 }
                 
             } failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 
                 [self failureWithAPI:CAPITagPostSurveyComments andError:error];
                 if (completion)
                     completion(nil, error);
             }];
}

- (void)submitSurveyWithSurveyId:(NSNumber *)surveyId surveyQA:(NSArray *)surveyQA completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    NSDictionary *dict = @{@"survey_id":surveyId,
                           @"surveyQA":surveyQA};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostSubmitSurvey parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         if ([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagPostSubmitSurvey andError:error andHandler:^(BOOL retry) {
             if (retry) {
                 [self submitSurveyWithSurveyId:surveyId surveyQA:surveyQA completed:completion];
             }
         }];
         if (completion)
             completion(nil, error);
     }];
}

- (void)getSurveyDetails:(NSNumber *)surveyId completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] GET:[NSString stringWithFormat:@"%@/%@", CAPITagGetSurveyDetails, surveyId] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagGetSurveyDetails andError:error];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)getSurveyResults:(NSNumber *)surveyId completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] GET:[NSString stringWithFormat:@"%@/%@",CAPITagGetSurveyResults,surveyId] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagGetSurveyResults andError:error];
         if (completion)
             completion(nil, error);
     }];
}


- (void)addSurveyCommentWithSurveyId:(NSNumber*)surveyId  comment:(NSString*)comment completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"survey_id":surveyId,
                           @"comment":comment};
    
     [[MIAFNetworking sharedInstance] POST:CAPITagPostAddComment parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
                 {
                     completion(responseObject, nil);
                 }
                 
             } failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 
                 [self failureWithAPI:CAPITagPostAddComment andError:error];
                 if (completion)
                     completion(nil, error);
             }];
}


- (void)deleteComment:(NSNumber*)commentId completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"id":commentId};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostDeleteComments parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             completion(responseObject, nil);
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagPostDeleteComments andError:error];
         if (completion)
             completion(nil, error);
     }];
}

- (void)addTopicFromSurveyWithSurveyId:(NSNumber *)surveyId completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"survey_id":surveyId};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagPostAddTopicsFromSurvey parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagPostAddTopicsFromSurvey andError:error andHandler:^(BOOL retry) {
             if (retry) [self addTopicFromSurveyWithSurveyId:surveyId completed:completion];
             else if (completion) completion(nil, error);
         }];
     }];
}

- (NSURLSessionDataTask *)getUserRespondedCount:(NSNumber*)surveyId completed:(void (^)(id responseObject, NSError *error))completion
{
    
   return [[MIAFNetworking sharedInstance] GET:[NSString stringWithFormat:@"%@/%@",CAPITagGetUserRespondedCount, surveyId] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             completion(responseObject, nil);
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagGetUserRespondedCount andError:error];
         if (completion)
             completion(nil, error);
     }];
}


#pragma mark -
#pragma mark - Save In Local

- (void)saveLoginUserToLocal:(id)responseObject
{
    appDelegate.loginUser = [self userWithDictionary:[responseObject valueForKey:CJsonData]];
    
    
    if (appDelegate.loginUser)
    {
        NSDictionary *dictMeta = [responseObject valueForKey:CJsonMeta];
        if ([dictMeta valueForKey:@"token"])
        {
            [CUserDefaults setObject:[dictMeta stringValueForJSON:@"token"] forKey:UserDefaultLoginToken];
            [CUserDefaults synchronize];
        }
        
        [UIApplication setUserId:[[responseObject valueForKey:CJsonData] stringValueForJSON:@"id"]];
    }
}



- (void)saveCategoryInLocal:(NSArray*)arrCategory
{
    for (NSDictionary *dict in arrCategory)
    {
        TBLCategory *category = [TBLCategory findOrCreate:@{@"category_id":[dict numberForJson:@"id"]}];
        
        category.category_name      = [dict stringValueForJSON:@"category_name"];
        category.interested         = [dict booleanForKey:@"interested"];
        category.sub_category_count = [dict integerForKey:@"sub_category_count"];
        category.user_interested_count = [dict integerForKey:@"user_category_count"];
        
        NSArray *arrSubCategory = [dict objectForKey:@"sub_categories"];
        
        
        for (NSDictionary *dictSub in arrSubCategory)
        {
            TBLSubCategory *subCategory = [TBLSubCategory findOrCreate:@{@"category_id":[dictSub numberForJson:@"id"]}];
            
            subCategory.category_name      = [dictSub stringValueForJSON:@"category_name"];
            subCategory.interested         = [dictSub booleanForKey:@"interested"];
            subCategory.user_interested_count = [dictSub integerForKey:@"user_category_count"];
            
            [category addSub_categoriesObject:subCategory];
        }
    }
    
    [[[Store sharedInstance] mainManagedObjectContext] save];
}

- (void)saveRaceInLocal:(NSArray*)arrRace
{
    for (NSDictionary *dict in arrRace)
    {
        TBLRace  *race = [TBLRace findOrCreate:@{@"race_id":[dict numberForJson:@"id"]}];
        
        race.race_type      = [dict stringValueForJSON:@"race_type"];
        
    }
    
    [[[Store sharedInstance] mainManagedObjectContext] save];

}

- (void)saveReligionInLocal:(NSArray*)arrReligion
{
    for (NSDictionary *dict in arrReligion)
    {
        TBLReligion  *religion = [TBLReligion findOrCreate:@{@"religion_id":[dict numberForJson:@"id"]}];
        
        religion.religion_type      = [dict stringValueForJSON:@"religion_type"];
        
    }
    
    [[[Store sharedInstance] mainManagedObjectContext] save];
    
}

- (void)saveMaritalStatusInLocal:(NSArray*)arrMaritalStatus
{
    for (NSDictionary *dict in arrMaritalStatus)
    {
        TBLMarital  *maritalStatus = [TBLMarital findOrCreate:@{@"marital_id":[dict numberForJson:@"id"]}];
        
        maritalStatus.name      = [dict stringValueForJSON:@"name"];
       
    }
    
    [[[Store sharedInstance] mainManagedObjectContext] save];
    
}

- (void)saveAccessCodeInLocal:(NSArray*)arrAccessCode
{
    for (NSDictionary *dictCode in arrAccessCode)
    {
        TBLAccessCode *accessCode = [TBLAccessCode findOrCreate:@{@"access_id":[dictCode stringValueForJSON:@"id"]}];
        accessCode.code = [dictCode stringValueForJSON:@"code"];
        accessCode.name = [dictCode stringValueForJSON:@"code_name"];
       // accessCode.status = [dictCode booleanForKey:@"status"];
        [appDelegate.loginUser addAccess_codeObject:accessCode];
        
    }
    
    [[Store sharedInstance].mainManagedObjectContext save];

    
}

#pragma mark -
#pragma mark - Helper Method

- (TBLUser *)userWithDictionary:(NSDictionary *)dictUser
{
    TBLUser *user;
    
    if (dictUser)
    {
        user = (TBLUser *)[TBLUser findOrCreate:@{@"user_id":[dictUser stringValueForJSON:@"id"]}];
        
        //....UserInfo
        user.email                      = [dictUser stringValueForJSON:@"email"];
        user.verified                   = [[dictUser stringValueForJSON:@"verified"] isEqualToString:@"Yes"];
        user.user_name                  = [dictUser stringValueForJSON:@"user_name"];
        user.zip_code                   = [dictUser stringValueForJSON:@"zip_code"];
        user.dob                        = [dictUser stringValueForJSON:@"dob"];
        user.gender                     = [dictUser stringValueForJSON:@"gender"];
        user.status                     = [dictUser integerForKey:@"status"];
        user.iAllowedNotification       = [dictUser booleanForKey:@"is_notify"];
        user.is_facebook_linked         = [dictUser booleanForKey:@"is_facebook_linked"];
        user.is_linkedIn_linked         = [dictUser booleanForKey:@"is_linkedIn_linked"];
        user.is_google_linked           = [dictUser booleanForKey:@"is_google_linked"];
        
        if([[dictUser valueForKey:@"race"] count])
        {
            NSDictionary *dictRace = dictUser[@"race"];
            
            TBLRace *race = [TBLRace findOrCreate:@{@"race_id":[dictRace stringValueForJSON:@"id"]}];
            race.race_type = [dictRace stringValueForJSON:@"race_type"];
            
            [user setRace:race];
        }

        if([[dictUser valueForKey:@"religion"] count])
        {
            NSDictionary *dictReligion = dictUser[@"religion"];
            
            TBLReligion *religion = [TBLReligion findOrCreate:@{@"religion_id":[dictReligion stringValueForJSON:@"id"]}];
            religion.religion_type = [dictReligion stringValueForJSON:@"religion_type"];
            
            [user setReligion:religion];
        }

        if([[dictUser valueForKey:@"marital_status"] count])
        {
            NSDictionary *dictMarital = dictUser[@"marital_status"];
            
            TBLMarital *marital = [TBLMarital findOrCreate:@{@"marital_id":[dictMarital stringValueForJSON:@"id"]}];
            marital.name = [dictMarital stringValueForJSON:@"name"];
            [user setMarital_status:marital];
        }
        
        NSArray *arrAccessCode = [dictUser valueForKey:@"access_codes"];
        
        for (NSDictionary *dictCode in arrAccessCode)
        {
            TBLAccessCode *accessCode = [TBLAccessCode findOrCreate:@{@"access_id":[dictCode stringValueForJSON:@"id"]}];
            accessCode.code = [dictCode stringValueForJSON:@"code"];
            accessCode.name = [dictCode stringValueForJSON:@"code_name"];
            accessCode.status = [dictCode booleanForKey:@"status"];
            [user addAccess_codeObject:accessCode];
            
        }

        [[Store sharedInstance].mainManagedObjectContext save];
    }
    
    return user;
}



@end

//
//  MIRegisterViewController.m
//  VoxPop
//
//  Created by mac-00014 on 8/24/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIRegisterViewController.h"
#import "MIVerificationViewController.h"
#import "MISelectTopicViewController.h"
#import "MIPersonalProfileViewController.h"

@interface MIRegisterViewController ()
{
    NSArray *arrSelectedTopics;
    NSMutableDictionary *dicPreData;
}
@end

@implementation MIRegisterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"REGISTER";

    if (self.iObject)
    {
        txtEmail.text = [self.iObject stringValueForJSON:@"email"];
        txtUserName.text = [self.iObject stringValueForJSON:@"user_name"];
    }
}



#pragma mark -
#pragma mark - Action Event

- (IBAction)btnSelectTopicClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    MISelectTopicViewController *selectTopicVC = [MISelectTopicViewController initWithXib];
    selectTopicVC.selectType = SelectTypeTopic;
    selectTopicVC.iObject  = arrSelectedTopics.count ? @1 : @0;
    if(arrSelectedTopics.count)
        [self updateSelectedCatagoryStatus];
    [selectTopicVC setBlock:^(NSArray *arrSelectedTopic, NSError *error)
    {
        self->arrSelectedTopics = arrSelectedTopic;
        self->txtSelectTopic.text = arrSelectedTopic.count ? @"Topics selected" : @"";
    }];
    
    [self.navigationController pushViewController:selectTopicVC animated:YES];
}
- (IBAction)btnPersonalProfileClicked:(UIButton *)sender
{
    [self resignKeyboard];
    
    MIPersonalProfileViewController *personalProfileVC = [MIPersonalProfileViewController initWithXib];
    personalProfileVC.dicPreData = dicPreData;
    [personalProfileVC setBlock:^(NSMutableDictionary *dic, NSError *error)
     {
         self->textDOB = dic[@"DOB"];
         self->textGender = dic[@"Gender"];
         self->textZipCode = dic[@"ZipCode"];
         self->textMarriageStatus = dic[@"MarriageStatus"];
         self->txtProfileSelect.text = dic.count ? @"Profile completed" : @"";
         self->dicPreData = dic;
     }];
    
    [self.navigationController pushViewController:personalProfileVC animated:YES];
}

- (IBAction)btnTermsAndConditionClicked:(UIButton *)sender
{
    [self resignKeyboard];
    btnCheck.selected = !btnCheck.selected;
}

- (IBAction)btnSignUpClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    if (![txtUserName.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankUserName onView:self];
    else if (![txtEmail.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankEmail onView:self];
    else if (![txtEmail.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageValidEmail onView:self];
    else if (![txtPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankPassword onView:self];
    else if (![txtPassword.text isAlphaNumericValidationPassed] || txtPassword.text.length < 6)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageAlphaNumbricPassword onView:self];
    else if (![txtConfirmPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankConfirmPassword onView:self];
    else if (![txtPassword.text isEqualToString:txtConfirmPassword.text])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageConfirmPasswordNotMatch onView:self];
//    else if (![txtDOB.text isBlankValidationPassed])
//        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankDOB onView:self];
//    else if (![txtGender.text isBlankValidationPassed])
//        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankGender onView:self];
    else if (![txtSelectTopic.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankTopic onView:self];
    else if (![textZipCode isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankPersonalProfile onView:self];
    else if (!btnCheck.selected)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageSelectTermsAndCondition onView:self];
    else
    {
        
        NSString *strSelectTopic = [[arrSelectedTopics valueForKeyPath:@"category_id"]componentsJoinedByString:@","];
        
        [[APIRequest request] signupWithEmail:txtEmail.text
                                  andPassword:txtPassword.text
                                  andUserName:txtUserName.text
                                       andDOB:textDOB
                                    andGender:textGender.lowercaseString
                                andAccessCode:txtAccessCode.text
                            andSelectedTopics:strSelectTopic
                                 andMrgStatus:textMarriageStatus
                                   andZipCode:textZipCode
                                     socialId:self.iObject ? [self.iObject stringValueForJSON:@"social_id"] : @"" socialType:self.iObject ? [self.iObject numberForJson:@"social_type"] : nil   completed:^(id responseObject, NSError *error)
        {
            if([[APIRequest request] isJSONDataValidWithResponse:responseObject] && [[responseObject valueForKey:CJsonData] integerForKey:CJsonStatus] == CStatusFour)
            {
                
                [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:self buttonTitle:@"Ok" handler:^(UIAlertAction *action)
                 {
                     MIVerificationViewController *verificationVC = [MIVerificationViewController initWithXib];
                     [verificationVC setIObject:self->txtEmail.text];
                     if([[[responseObject valueForKey:CJsonMeta] allKeys] containsObject:@"verify_code"])
                     [verificationVC setVerifyCode:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:@"verify_code"]];
                     [self.navigationController pushViewController:verificationVC animated:YES];
                 }];

            }
            else if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:self buttonTitle:@"Ok" handler:nil];
            }
            
            
        }];
    }
}

#pragma mark
#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == txtUserName && [string containsString:@" "]) {
        return NO;
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (txtPassword == textField && ![txtPassword.text isEqual: @""]) {
        if (![txtPassword.text isAlphaNumericValidationPassed] || txtPassword.text.length < 6) {
            [CustomAlertView iOSAlert:@"" withMessage:CMessageAlphaNumbricPassword onView:self];
            return false;
        } else {
            return true;
        }
    }
    else if (txtConfirmPassword == textField && ![txtConfirmPassword.text isEqual: @""]){
        if (![txtPassword.text isEqualToString:txtConfirmPassword.text]) {
            [CustomAlertView iOSAlert:@"" withMessage:CMessageConfirmPasswordNotMatch onView:self];
            return false;
        } else {
            return true;
        }
    }
    return true;
}

#pragma mark
#pragma mark - Helper Method

- (void)updateSelectedCatagoryStatus
{
    
    [arrSelectedTopics enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
    {
        if([obj isKindOfClass:[TBLCategory class]])
        {
            TBLCategory *cat = (TBLCategory*)obj;
            cat.interested  = 1;
        }
        else
        {
            TBLSubCategory *cat = (TBLSubCategory*)obj;
            cat.interested  = 1;
        }
        
        if(self->arrSelectedTopics.count-1 == idx)
            [[[Store sharedInstance] mainManagedObjectContext] save];
        
    }];
    
   // [TBLCategory updateNSManagedObject:@{@"interested":@1} predicate:[NSPredicate predicateWithFormat:@"SELF IN %@", arrSelectedTopics] context:nil];
    
    //[TBLSubCategory updateNSManagedObject:@{@"interested":@1} predicate:[NSPredicate predicateWithFormat:@"SELF IN %@", arrSelectedTopics] context:nil];
    
}

@end

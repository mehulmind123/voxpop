//
//  MIPersonalProfileViewController.m
//  VoxPop
//
//  Created by mac-00012 on 05/06/19.
//  Copyright © 2019 Jignesh-0007. All rights reserved.
//

#import "MIPersonalProfileViewController.h"

@interface MIPersonalProfileViewController ()
{
    TBLMarital *selectedMaritalStatus;
}
@end

@implementation MIPersonalProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"PERSONAL PROFILE";
    
    // Configure DOB field
    [txtDOB setDatePickerWithDateFormat:@"MM/dd/yyyy" defaultDate:nil];
    [txtDOB setDatePickerMode:UIDatePickerModeDate];
    [txtDOB setMaximumDate:[NSDate date]];
    
    // Configure Gender field
    [txtGender setPickerData:@[@"Male", @"Female", @"Other"]];
    
    // Configure Marital Status
    NSArray *arrMarital = [TBLMarital fetch:nil sortedBy:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]] ;
    
    [txtMarriageStatus setPickerData:[arrMarital valueForKeyPath:@"name"]  update:^(NSString *text, NSInteger row, NSInteger component) {
        self->selectedMaritalStatus = arrMarital[row];
    }];
    
    // set botton text font
    UIFont * _Nullable extractedExpr = CFontSolidoBook(17);
    lblBottomText.font = extractedExpr;
    
    // Prefill Data
    if (_dicPreData) {
        
        NSArray *arrMarital = [TBLMarital fetch:[NSPredicate predicateWithFormat:@"marital_id==%@",_dicPreData[@"MarriageStatus"]] sortedBy:nil];
        
        if (arrMarital.count > 0) {
            TBLMarital *objMarital = arrMarital.firstObject;
            txtDOB.text = _dicPreData[@"DOB"];
            txtGender.text = _dicPreData[@"Gender"];
            txtMarriageStatus.text = objMarital.name;
            txtZipCode.text = _dicPreData[@"ZipCode"];
        }
        
    }}

#pragma mark -
#pragma mark - Action Event

- (IBAction)btnSaveClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    if (![txtDOB.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankDOB onView:self];
    else if (![txtGender.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankGender onView:self];
    else if (![txtZipCode.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankZipCode onView:self];
    else if (![txtZipCode.text isAlphaNumericValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankZipCode onView:self];
    else
    {
        if (self.block)
        {
            NSMutableDictionary * dic = [[NSMutableDictionary alloc] init];
            dic[@"DOB"] = txtDOB.text;
            dic[@"Gender"] = txtGender.text;
            dic[@"MarriageStatus"] = [NSString stringWithFormat:@"%lld",selectedMaritalStatus.marital_id];
            dic[@"ZipCode"] = txtZipCode.text;
            self.block(dic, nil);
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark
#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == txtZipCode) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if ([newString length] > 5)
            return NO;
    }
    
    return YES;
}

@end

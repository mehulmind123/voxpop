//
//  MIEditProfileViewController.h
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIEditProfileViewController : MIParentViewController
{
    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtMarriageStatus;
    IBOutlet UITextField *txtDOB;
}
@end

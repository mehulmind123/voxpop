//
//  MIVerificationViewController.h
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"


@interface MIVerificationViewController : MIParentViewController
{
    IBOutlet UITextField *txtVerificationCode;
}

@property (nonatomic, strong) NSString *verifyCode;

@end

//
//  MIEditProfileViewController.m
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIEditProfileViewController.h"

@interface MIEditProfileViewController ()
{
    TBLMarital  *selectedMaritalStatus;
    TBLRace     *selectedRace;
    TBLReligion *selectedReligion;
}
@end

@implementation MIEditProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"EDIT PROFILE";
    
    // Configure User Name
    txtUserName.text = appDelegate.loginUser.user_name;
    
    // Configure DOB field
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *dob = [dateFormatter dateFromString:appDelegate.loginUser.dob];
    [txtDOB setDatePickerWithDateFormat:@"MM/dd/yyyy" defaultDate:dob];
    [txtDOB setDatePickerMode:UIDatePickerModeDate];
    [txtDOB setMaximumDate:[NSDate date]];
    
    // Configure Marital Status
    txtMarriageStatus.text = appDelegate.loginUser.marital_status.name.isBlankValidationPassed ?appDelegate.loginUser.marital_status.name : @"N/A" ;
    selectedMaritalStatus = appDelegate.loginUser.marital_status;
    
    NSArray *arrMarital = [TBLMarital fetch:nil sortedBy:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]] ;
    
    [txtMarriageStatus setPickerData:[arrMarital valueForKeyPath:@"name"]  update:^(NSString *text, NSInteger row, NSInteger component) {
        self->selectedMaritalStatus = arrMarital[row];
    }];
}


#pragma mark -
#pragma mark - Action Event

- (IBAction)btnSaveClicked:(UIButton*)sender
{
    if (![txtUserName.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankUserName onView:self];
    else if (![txtDOB.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankDOB onView:self];
    else
    {

        
        [[APIRequest request] editProfileWithUserName:txtUserName.text dob:txtDOB.text
                                     andMaritalStatus:selectedMaritalStatus?selectedMaritalStatus.marital_id:0
                                            completed:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
             {
                 if (self.block)
                     self.block(@1,nil);
                 
                 [CustomAlertView iOSAlert:@"" withMessage:CMessageProfileUpdate onView:self buttonTitle:@"Ok" handler:^(UIAlertAction *action) {
                     [self.navigationController popViewControllerAnimated:YES];
                 }];
             }
         }];
    }
}


#pragma mark
#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == txtUserName && [string containsString:@" "]) {
        return NO;
    }
    
    return YES;
}

@end

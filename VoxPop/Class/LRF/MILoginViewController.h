//
//  MILoginViewController.h
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MILoginViewController : MIParentViewController
{
    IBOutlet UILabel *lblCreateAccount;
    
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    
    IBOutlet UIButton *btnCreateOne;
}
@end

//
//  MIRegisterViewController.h
//  VoxPop
//
//  Created by mac-00014 on 8/24/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIRegisterViewController : MIParentViewController<UITextFieldDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIButton *btnCheck;
    
    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtConfirmPassword;
    NSString *textDOB;
    NSString *textGender;
    NSString *textZipCode;
    NSString *textMarriageStatus;
    IBOutlet UITextField *txtProfileSelect;
    IBOutlet UITextField *txtAccessCode;
    IBOutlet UITextField *txtSelectTopic;
}
@end

//
//  MIVerificationViewController.m
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIVerificationViewController.h"

@interface MIVerificationViewController ()

@end

@implementation MIVerificationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"ENTER VERIFICATION CODE";
    self.navigationItem.hidesBackButton = YES;
    
    [self updateVerifyCode:_verifyCode];
}

- (void)updateVerifyCode:(NSString*)code
{
    
    txtVerificationCode.text = code.isBlankValidationPassed ? code : @"";
}
#pragma mark -
#pragma mark - Action Event

- (IBAction)btnSubmitClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    if (![txtVerificationCode.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankVerificationCode onView:self];
    
    else
    {
        [[APIRequest request] verifyEmailAccount:self.iObject verificationCode:txtVerificationCode.text completed:^(id responseObject, NSError *error)
         {
             if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                 [appDelegate signinUser];
             
                              
        }];
        
    }
}

- (IBAction)btnResendClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    [[APIRequest request] resendCode:self.iObject completed:^(id responseObject, NSError *error)
    {     
        if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:self buttonTitle:@"OK" handler:^(UIAlertAction *action)
            {
                if([[[responseObject valueForKey:CJsonMeta] allKeys] containsObject:@"verify_code"])
                    [self setVerifyCode:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:@"verify_code"]];
                
                [self updateVerifyCode:_verifyCode];
                
            }];
        }
        
    }];
    
}

@end

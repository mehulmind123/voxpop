//
//  MILoginViewController.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MILoginViewController.h"
#import "MIRegisterViewController.h"
#import "MIForgotPasswordViewController.h"
#import "MIVerificationViewController.h"
#import "MIChangePasswordViewController.h"
#import "MIEditProfileViewController.h"
#import "MISelectTopicViewController.h"

@interface MILoginViewController ()

@end

@implementation MILoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    //....Attributed String
//    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?11:(Is_iPhone_6_PLUS)?15:13;
//    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:lblCreateAccount.text];
//    [attString addAttribute:NSFontAttributeName value:CFontRobotoRegular(fontSize) range:NSMakeRange(0, lblCreateAccount.text.length)];
//    [attString addAttributes:@{NSFontAttributeName:CFontRobotoMedium(fontSize),
//                               NSUnderlineStyleAttributeName:@(NSUnderlineStyleThick)} range:[lblCreateAccount.text rangeOfString:@"CREATE ONE NOW"]];
//
//    [lblCreateAccount setAttributedText:attString];
    [self resignKeyboard];
    [btnCreateOne setUnderlineHeight:1.0 andColor:ColorWhite_FFFFFF];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark - Action Event

- (IBAction)btnFacebookClicked:(UIButton*)sender
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [[APIRequest request] signUpWithFacebookFromViewController:self withCompletion:^(id userInfo, NSError *error)
     {
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         if (error)
         {
             NSString *title = [NSString stringWithFormat:@"Facebook(%ld)", (long)error.code];
             [CustomAlertView iOSAlert:title withMessage:error.localizedDescription onView:self];
         }
         else if (userInfo)
         {
             NSLog(@"%@",userInfo);
             NSString *email = [userInfo[@"email"] isBlankValidationPassed]?userInfo[@"email"]:@"";
              NSString *firstName = [userInfo[@"first_name"] isBlankValidationPassed]?userInfo[@"first_name"]:@"";
              NSString *lastName = [userInfo[@"last_name"] isBlankValidationPassed]?userInfo[@"last_name"]:@"";

             NSString *fbId = [userInfo[@"id"] isBlankValidationPassed]?userInfo[@"id"]:@"";

             
             [[APIRequest request] checkSocialAccount:fbId socialType:@1 email:email completed:^(id responseObject, NSError *error)
              {
                  if([[APIRequest request] isUserNotVerifiedWithResponse:responseObject])
                  {
                      
                      [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
                  }
                  else  if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                  {
                      if ([[[responseObject valueForKey:CJsonMeta] numberForJson:@"redirect"] isEqual:@1])
                      {
                          MIRegisterViewController *signUpVC = [MIRegisterViewController initWithXib];
                          signUpVC.iObject = @{@"social_id": fbId,
                                               @"social_type":@1,
                                               @"email":email,
                                               @"user_name":[NSString stringWithFormat:@"%@%@",firstName,lastName]};
                          [self.navigationController pushViewController:signUpVC animated:YES];
                      }
                      else
                      {
                          [[APIRequest request] saveLoginUserToLocal:responseObject];
                          [appDelegate signinUser];
                      }
                  }
                  
                  
              }];
             
         }
     }];
}
- (IBAction)btnGoogleClicked:(UIButton*)sender
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [[APIRequest request] signUpWithGoogleFromViewController:self withCompletion:^(GIDGoogleUser *userInfo, NSError *error)
     {
          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         if (error)
         {
             /*
              Do not prompt error message for following error.
              
              Error Domain=com.google.GIDSignIn Code=-5 "The user canceled the sign-in flow." UserInfo={NSLocalizedDescription=The user canceled the sign-in flow.}
              */
             if (error.code == -5) return;
             
             NSString *title = [NSString stringWithFormat:@"Google(%ld)", (long)error.code];
             [CustomAlertView iOSAlert:title withMessage:error.localizedDescription onView:self];
         }
         else if (userInfo)
         {
             NSString *firstName =  [userInfo.profile.givenName isBlankValidationPassed]? userInfo.profile.givenName:@"";
             NSString *lastName =  [userInfo.profile.familyName isBlankValidationPassed]?userInfo.profile.familyName:@"";
             NSString *email = [userInfo.profile.email isBlankValidationPassed]?userInfo.profile.email:@"";
             
             NSString *googleId = [userInfo.userID isBlankValidationPassed]?userInfo.userID:@"";
             
             [[APIRequest request] checkSocialAccount:googleId socialType:@3 email:email completed:^(id responseObject, NSError *error)
              {
                  if([[APIRequest request] isUserNotVerifiedWithResponse:responseObject])
                  {
                      
                      [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
                      
                  }
                  else if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                  {
                    if ([[[responseObject valueForKey:CJsonMeta] numberForJson:@"redirect"] isEqual:@1])
                      {
                          MIRegisterViewController *signUpVC = [MIRegisterViewController initWithXib];
                          signUpVC.iObject = @{@"social_id": googleId,
                                               @"social_type":@3,
                                               @"email":email,
                                               @"user_name":[NSString stringWithFormat:@"%@%@",firstName,lastName]};
                          [self.navigationController pushViewController:signUpVC animated:YES];
                      }
                      else
                      {
                          [[APIRequest request] saveLoginUserToLocal:responseObject];
                          [appDelegate signinUser];
                      }
                  }
                  
                  
              }];
             
         }
     }];
}
- (IBAction)btnLinkedInClicked:(UIButton*)sender
{
   
    [appDelegate loginWithLinkedIn:^(id user, NSError *error)
     {
         if (user && !error)
         {
             [[APIRequest request] getLinkedInEmail:^(id responseEmail, NSError *error) {
                
                 NSString *firstName = [[[user valueForKey:@"firstName"]?[user valueForKey:@"firstName"]:@"" valueForKey:@"localized"] valueForKey:@"en_US"];
                 NSString *lastName = [[[user valueForKey:@"lastName"]?[user valueForKey:@"lastName"]:@"" valueForKey:@"localized"] valueForKey:@"en_US"];
                 NSString *linkedInId = [user stringValueForJSON:@"id"]?[user stringValueForJSON:@"id"]:@"";
                 
                 NSString *email;
                 if (responseEmail) {
                     email = responseEmail[@"elements"][0][@"handle~"][@"emailAddress"];
                 } else {
                     email = @"";
                 }
                 
                 [[APIRequest request] checkSocialAccount:linkedInId socialType:@2 email:email completed:^(id responseObject, NSError *error)
                  {
                      if([[APIRequest request] isUserNotVerifiedWithResponse:responseObject])
                      {
                          [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
                          
                      }
                      else if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                      {
                          if ([[[responseObject valueForKey:CJsonMeta] numberForJson:@"redirect"] isEqual:@1])
                          {
                              MIRegisterViewController *signUpVC = [MIRegisterViewController initWithXib];
                              signUpVC.iObject = @{@"social_id": linkedInId,
                                                   @"social_type":@2,
                                                   @"email":email,
                                                   @"user_name":[NSString stringWithFormat:@"%@%@",firstName,lastName]};
                              [self.navigationController pushViewController:signUpVC animated:YES];
                          }
                          else
                          {
                              [[APIRequest request] saveLoginUserToLocal:responseObject];
                              [appDelegate signinUser];
                          }
                      }
    
                  }];
             }];
             
         }
         
     }];
    
}

- (IBAction)btnSignInClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    if(IS_IPHONE_SIMULATOR)
    {
        
        if(![txtEmail.text isBlankValidationPassed])
            txtEmail.text = @"michalgreg2013@gmail.com";
    
        
        if(![txtPassword.text isBlankValidationPassed])
            txtPassword.text = @"123456";
    }
    
    if (![txtEmail.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankEmail onView:self];
    
    else if (![txtEmail.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageValidEmail onView:self];
    
    else if (![txtPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankPassword onView:self];
    
    else
    {
        
        [[APIRequest request] signInWithEmail:txtEmail.text
                                  andPassword:txtPassword.text completed:^(id responseObject, NSError *error)
         {
             if([[APIRequest request] isUserNotVerifiedWithResponse:responseObject])
             {
                 [CustomAlertView iOSAlertWithTwoButton:@"" withMessage:CMessageVerifyAccount firstButtonTitle:CMessageSendCode secondButtonTitle:@"Cancel" onView:self handler:^(UIAlertAction *action)
                  {
                      if ([action.title isEqualToString:CMessageSendCode])
                      {
                          [[APIRequest request] resendCode:txtEmail.text completed:^(id responseObject, NSError *error)
                           {
                               if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                               {
                                   [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:self buttonTitle:@"Ok" handler:^(UIAlertAction *action)
                                    {
                                        MIVerificationViewController *verificationVC = [MIVerificationViewController initWithXib];
                                        verificationVC.iObject = txtEmail.text;
                                        if([[[responseObject valueForKey:CJsonMeta] allKeys] containsObject:@"verify_code"])
                                            [verificationVC setVerifyCode:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:@"verify_code"]];
                                        [self.navigationController pushViewController:verificationVC animated:YES];
                                    }];                                   
                                   
                               }
                               
                               
                           }];
                          
                      }
                  }];
                 
                 
             }
             else if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
             {
                 [[APIRequest request] saveLoginUserToLocal:responseObject];
                 [appDelegate signinUser];
             }
             
             
         }];

    }
    
    
}

- (IBAction)btnForgotPasswordClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    MIForgotPasswordViewController *forgotVC = [MIForgotPasswordViewController initWithXib];
    [self.navigationController pushViewController:forgotVC animated:YES];
}

- (IBAction)btnRegisterClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    MIRegisterViewController *signUpVC = [MIRegisterViewController initWithXib];
    [self.navigationController pushViewController:signUpVC animated:YES];
}

@end

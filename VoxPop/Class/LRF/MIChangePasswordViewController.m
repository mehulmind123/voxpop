//
//  MIChangePasswordViewController.m
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIChangePasswordViewController.h"

@interface MIChangePasswordViewController ()

@end

@implementation MIChangePasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"CHANGE PASSWORD";
}


#pragma mark -
#pragma mark - Action Event

- (IBAction)btnSaveClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    if (![txtOldPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankOldPassword onView:self];
    
    if (![txtOldPassword.text isAlphaNumericValidationPassed] || txtOldPassword.text.length < 6)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageWrongOldPassword onView:self];
    
    else if (![txtNewPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankNewPassword onView:self];
    
    else if (![txtNewPassword.text isAlphaNumericValidationPassed] || txtNewPassword.text.length < 6)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageAlphaNumbricNewPassword onView:self];
    
    else if (![txtConfirmPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankConfirmPassword onView:self];
    
    else if (![txtConfirmPassword.text isEqualToString:txtNewPassword.text])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageConfirmPasswordNotMatch onView:self];
    
    else
    {
        
        
        [[APIRequest request] changePasswordWithNewPassword:txtNewPassword.text
                                             andOldPassword:txtOldPassword.text completed:^(id responseObject, NSError *error)
        {
            if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] valueForKey:CJsonMessage] onView:self buttonTitle:@"Ok" handler:^(UIAlertAction *action)
                 {
                     [self.navigationController popViewControllerAnimated:YES];
                 }];

            }
            
            
        }];
    }
}
@end

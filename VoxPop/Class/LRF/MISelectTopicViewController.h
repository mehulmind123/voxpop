//
//  MISelectTopicViewController.h
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

typedef enum : NSUInteger
{
    SelectTypeTopic,
    SelectTypeSubTopic
    
} SelectType;

@interface MISelectTopicViewController : MIParentViewController
{
    IBOutlet UICollectionView *collView;
    
    IBOutlet UIView *vFooter;
    
    IBOutlet UILabel *lblContactUs;
    
    IBOutlet UIButton *btnDone;
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UILabel *lblNoResult;
    IBOutlet UIButton *btnTapToRetry;
    
    IBOutlet NSLayoutConstraint *cnDoneTop;
    IBOutlet UILabel *lblScrollMore;
}

@property (nonatomic, assign) SelectType selectType;
@property (nonatomic, strong) NSMutableArray *arrTopics;
@end

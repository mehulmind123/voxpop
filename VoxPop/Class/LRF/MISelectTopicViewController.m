//
//  MISelectTopicViewController.m
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MISelectTopicViewController.h"
#import "MISelectTopicCollectionViewCell.h"


@interface MISelectTopicViewController () 
{
    NSInteger iOffset;
}
@end

@implementation MISelectTopicViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (self.selectType == SelectTypeSubTopic)
        if (self.block) self.block(nil, nil);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    //....Attributed String
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?13:(Is_iPhone_6_PLUS)?17:15;
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:lblContactUs.text];
    [attString addAttribute:NSFontAttributeName value:CFontSolidoBook(fontSize) range:NSMakeRange(0, lblContactUs.text.length)];
    [attString addAttributes:@{NSFontAttributeName:CFontSolidoBold(fontSize),
                               NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:[lblContactUs.text rangeOfString:@"contact us"]];
    [lblContactUs setAttributedText:attString];
    
    [collView registerNib:[UINib nibWithNibName:@"MISelectTopicCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MISelectTopicCollectionViewCell"];
    
    
    //....
    if (self.selectType == SelectTypeTopic)
    {
        self.title = @"SELECT TOPICS OF INTEREST";
        
        if([self.iObject boolValue])
        {
            [self loadTopicDataFromLocal];
            [vFooter setHidden:NO];
        }
        else
            [self loadTopicDataFromServer];
    }
    else
    {
        _arrTopics = [_arrTopics sortedArrayUsingDescriptors:[NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"category_name" ascending:YES], nil]].mutableCopy;
        [btnDone hideByHeight:YES];
        [cnDoneTop setConstant:0.0];
        [vFooter setHidden:NO];
    }
}

#pragma mark -
#pragma mark - Load Data

- (void)loadTopicDataFromServer
{
    if (iOffset == 0) {
        [collView setHidden:YES];
        [lblNoResult setHidden:YES];
        [btnTapToRetry setHidden:YES];
        [activityIndicator startAnimating];
    }
    
    [[APIRequest request] loadTopicsListWithOffset:iOffset completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
         {
             iOffset = [[responseObject valueForKey:CJsonMeta] integerForKey:@"new_offset"];
             [self loadTopicDataFromServer];
         }
         else if (error)
         {
             [activityIndicator stopAnimating];
             
             iOffset = 0;
             [lblNoResult setText:CErrorTapToRetry];
             [lblNoResult setHidden:NO];
             [btnTapToRetry setHidden:NO];
         }
         else if (!error)
         {
             [activityIndicator stopAnimating];
             
             [self loadTopicDataFromLocal];
             if (_arrTopics.count > 0)
             {
                 [collView setHidden:NO];
                 [vFooter setHidden:NO];
             }
             else
             {
                 iOffset = 0;
                 [lblNoResult setText:CMessageNoResultFound];
                 [lblNoResult setHidden:NO];
             }
         }
     }];
}

- (void)loadTopicDataFromLocal
{
    _arrTopics = [TBLCategory fetch:nil sortedBy:@[[NSSortDescriptor sortDescriptorWithKey:@"category_name" ascending:YES]]].mutableCopy;
    [collView reloadData];
}



#pragma mark -
#pragma mark - Action Event

- (IBAction)btnTapToRetryClicked:(UIButton*)sender
{
    [self loadTopicDataFromServer];
}

- (IBAction)btnDoneClicked:(UIButton*)sender
{
    NSArray *arrCategory = [TBLCategory fetchAllObjects];
    
    NSMutableArray *arrTemp = [NSMutableArray new];
    
    [arrCategory enumerateObjectsUsingBlock:^(TBLCategory *category, NSUInteger idx, BOOL * _Nonnull stop)
     {
         if (category.interested)
             [arrTemp addObject:category];
         
         [category.sub_categories.allObjects enumerateObjectsUsingBlock:^(TBLSubCategory *subCategory, NSUInteger idx, BOOL * _Nonnull stop)
          {
              if (subCategory.interested)
                  [arrTemp addObject:subCategory];
          }];
     }];
    
    if (!arrTemp.count || !_arrTopics.count)
        [CustomAlertView iOSAlert:@"" withMessage:CMessageSelectTopic onView:self];
    else
    {
        if (self.block)
        {
            self.block(arrTemp, nil);
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            NSString *strSelectTopic = [[arrTemp valueForKeyPath:@"category_id"]componentsJoinedByString:@","];
            
            [[APIRequest request] editInterestedTopic:strSelectTopic completed:^(id responseObject, NSError *error)
             {
                 if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                     [self.navigationController popViewControllerAnimated:YES];
             }];
        }
    }
}

- (IBAction)btnContactUsClicked:(UIButton*)sender
{
    [appDelegate openMailComposerWithEmailId:@"support@voxpopapp.com" subject:@"Contact us" content:@"" viewController:self];
}




#pragma mark
#pragma mark - UICollectionView Delegate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _arrTopics.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((CScreenWidth-30)/2, 53);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MISelectTopicCollectionViewCell";
    MISelectTopicCollectionViewCell *cell = [collView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (self.selectType ==  SelectTypeTopic)
    {
        TBLCategory *category = _arrTopics[indexPath.row];
        
        [cell.lblTitle setText:category.category_name];
        [cell.lblCount setText:[NSString stringWithFormat:@"%lld", category.user_interested_count]];
        [cell.btnNext setHidden:!category.sub_category_count];
        [cell.viewCount hideByHeight:(category.user_interested_count <= 0)];
        
        if ( category.interested)
        {
            [cell.btnCheck setSelected:YES];
            [cell.lblTitle setTextColor:ColorWhite_FFFFFF];
            [cell.cnBottomImgBack setConstant:0];
            [cell.imgVBack setImage:[[UIImage imageNamed:@"topicSelect"] stretchableImageWithLeftCapWidth:100 topCapHeight:15]];
            [cell.imgVBack layoutIfNeeded];
            
        }
        else
        {
            [cell.btnCheck setSelected:NO];
            [cell.lblTitle setTextColor:ColorWhite_FFFFFF];
            [cell.cnBottomImgBack setConstant:6];
            [cell.imgVBack setImage:[[UIImage imageNamed:@"topicDeselect"] stretchableImageWithLeftCapWidth:100 topCapHeight:15]];
            [cell.imgVBack layoutIfNeeded];
        }
        
        [cell.btnCheck touchUpInsideClicked:^{
            
            cell.btnCheck.selected = !cell.btnCheck.selected;
            category.interested = cell.btnCheck.selected;
            
            if ( category.interested)
            {
                [cell.btnCheck setSelected:YES];
                [cell.lblTitle setTextColor:ColorWhite_FFFFFF];
                [cell.cnBottomImgBack setConstant:0];
                [cell.imgVBack setImage:[[UIImage imageNamed:@"topicSelect"] stretchableImageWithLeftCapWidth:100 topCapHeight:15]];
                [cell.imgVBack layoutIfNeeded];
                
            }
            else
            {
                [cell.btnCheck setSelected:NO];
                [cell.lblTitle setTextColor:ColorWhite_FFFFFF];
                [cell.cnBottomImgBack setConstant:6];
                [cell.imgVBack setImage:[[UIImage imageNamed:@"topicDeselect"] stretchableImageWithLeftCapWidth:100 topCapHeight:15]];
                [cell.imgVBack layoutIfNeeded];
            }
            
            for (TBLSubCategory *subCategory in category.sub_categories.allObjects)
                subCategory.interested = cell.btnCheck.selected;
            [[[Store sharedInstance] mainManagedObjectContext] save];
        }];

        return cell;
    }
    else
    {
        [cell.btnNext hideByWidth:YES];
        
        TBLSubCategory *subCategory = _arrTopics[indexPath.row];
        
        [cell.lblTitle setText:subCategory.category_name];
        [cell.lblCount setText:[NSString stringWithFormat:@"%lld", subCategory.user_interested_count]];
        [cell.viewCount hideByHeight:(subCategory.user_interested_count <= 0)];
        
        if (subCategory.interested)
        {
            [cell.btnCheck setSelected:YES];
            [cell.lblTitle setTextColor:ColorWhite_FFFFFF];
            [cell.cnBottomImgBack setConstant:0];
            [cell.imgVBack setImage:[[UIImage imageNamed:@"topicSelect"] stretchableImageWithLeftCapWidth:100 topCapHeight:15]];
            [cell.imgVBack layoutIfNeeded];
        }
        else
        {
            [cell.btnCheck setSelected:NO];
            [cell.lblTitle setTextColor:ColorWhite_FFFFFF];
            [cell.cnBottomImgBack setConstant:6];
            [cell.imgVBack setImage:[[UIImage imageNamed:@"topicDeselect"] stretchableImageWithLeftCapWidth:100 topCapHeight:15]];
            [cell.imgVBack layoutIfNeeded];
        }
        
        [cell.btnCheck touchUpInsideClicked:^{
            cell.btnCheck.selected = !cell.btnCheck.selected;
            
            subCategory.interested = !subCategory.interested;
            
            if (subCategory.interested)
            {
                [cell.btnCheck setSelected:YES];
                [cell.lblTitle setTextColor:ColorWhite_FFFFFF];
                [cell.cnBottomImgBack setConstant:0];
                [cell.imgVBack setImage:[[UIImage imageNamed:@"topicSelect"] stretchableImageWithLeftCapWidth:100 topCapHeight:15]];
                [cell.imgVBack layoutIfNeeded];
            }
            else
            {
                [cell.btnCheck setSelected:NO];
                [cell.lblTitle setTextColor:ColorWhite_FFFFFF];
                [cell.cnBottomImgBack setConstant:6];
                [cell.imgVBack setImage:[[UIImage imageNamed:@"topicDeselect"] stretchableImageWithLeftCapWidth:100 topCapHeight:15]];
                [cell.imgVBack layoutIfNeeded];
            }
            
            [[[Store sharedInstance] mainManagedObjectContext] save];
        }];
        
        return cell;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectType == SelectTypeTopic)
    {
        TBLCategory *category = _arrTopics[indexPath.row];
        if (category.sub_categories.allObjects.count)
        {
            MISelectTopicViewController *selectSubTopicVC = [MISelectTopicViewController initWithXib];
            selectSubTopicVC.selectType = SelectTypeSubTopic;
            selectSubTopicVC.title = category.category_name.uppercaseString;
            selectSubTopicVC.arrTopics = category.sub_categories.allObjects.mutableCopy;
            
            [selectSubTopicVC setBlock:^(id object, NSError *error)
             {
                 MISelectTopicCollectionViewCell *cell = (MISelectTopicCollectionViewCell *) [self->collView cellForItemAtIndexPath:indexPath];
                 
                 [cell.btnCheck setSelected:[[category.sub_categories.allObjects valueForKeyPath:@"interested"] containsObject:@1]];
                 
                 category.interested = cell.btnCheck.selected;
                 [[[Store sharedInstance] mainManagedObjectContext] save];
                 
             }];
            
            [self.navigationController pushViewController:selectSubTopicVC animated:YES];
        }
    }
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == _arrTopics.count -1){
        [lblScrollMore setHidden:true];
    } else {
        [lblScrollMore setHidden:false];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    for (UICollectionViewCell *cell in [collView visibleCells]) {
        NSIndexPath *indexPath = [collView indexPathForCell:cell];

        if (indexPath.row == _arrTopics.count -1){
            [lblScrollMore setHidden:true];
        }
        else if (collView.contentOffset.x != 0 & collView.contentOffset.y != 0 & indexPath.row != _arrTopics.count -1) {
            [lblScrollMore setHidden:false];
        }
    }
}
@end

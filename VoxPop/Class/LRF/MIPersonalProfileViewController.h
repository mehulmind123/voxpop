//
//  MIPersonalProfileViewController.h
//  VoxPop
//
//  Created by mac-00012 on 05/06/19.
//  Copyright © 2019 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"


@interface MIPersonalProfileViewController : MIParentViewController
{
    IBOutlet UITextField *txtMarriageStatus;
    IBOutlet UITextField *txtZipCode;
    IBOutlet UITextField *txtDOB;
    IBOutlet UITextField *txtGender;
    IBOutlet UILabel *lblBottomText;
}
 @property (nonatomic, strong) NSMutableDictionary *dicPreData;
@end

//
//  MIForgotPasswordViewController.m
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIForgotPasswordViewController.h"
#import "MIVerificationViewController.h"

@interface MIForgotPasswordViewController ()

@end

@implementation MIForgotPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"FORGOT PASSWORD";
}

#pragma mark -
#pragma mark - Action Event

- (IBAction)btnSubmitClicked:(UIButton*)sender
{
    [self resignKeyboard];
    
    if (![txtEmail.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankEmail onView:self];
    
    else if (![txtEmail.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageValidEmail onView:self];
    
    else
    {
        
        [[APIRequest request] forgotPasswordWithEmail:txtEmail.text completed:^(id responseObject, NSError *error)
        {
            if([[APIRequest request] isUserNotVerifiedWithResponse:responseObject])
            {
                [CustomAlertView iOSAlertWithTwoButton:@"" withMessage:CMessageVerifyAccount firstButtonTitle:CMessageSendCode secondButtonTitle:@"Cancel" onView:self handler:^(UIAlertAction *action)
                 {
                     if ([action.title isEqualToString:CMessageSendCode])
                     {
                         [[APIRequest request] resendCode:txtEmail.text completed:^(id responseObject, NSError *error)
                          {
                              if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                              {
                                  MIVerificationViewController *verificationVC = [MIVerificationViewController initWithXib];
                                  verificationVC.iObject = txtEmail.text;
                                  if([[[responseObject valueForKey:CJsonMeta] allKeys] containsObject:@"verify_code"])
                                      [verificationVC setVerifyCode:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:@"verify_code"]];
                                  [self.navigationController pushViewController:verificationVC animated:YES];

                              }
                              
                             
                         }];
                
                     }
                 }];
                

            }
            else if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [CustomAlertView iOSAlert:@"" withMessage:CMessageSendResetPasswordLink onView:self buttonTitle:@"Ok" handler:^(UIAlertAction *action)
                 {
                     [self.navigationController popViewControllerAnimated:YES];
                 }];

            }
                        
        }];
        
    }
}

@end

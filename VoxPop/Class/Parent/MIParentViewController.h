//
//  MIParentViewController.h
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSUInteger, StopAnimationType)
{
    StopAnimationTypeDataNotFound,
    StopAnimationTypeErrorTapToRetry,
    StopAnimationTypeRemove
};

@interface MIParentViewController : UIViewController

@property (nonatomic, assign) BOOL showTabBar;

@property (nonatomic, assign) BOOL isPresentedVC;

@property (nonatomic, strong) id iObject;

@property (nonatomic, copy) void(^touchUpInsideViewClicked)(void);

- (void)setStatusBarHidden:(BOOL)hidden;

- (void)setStatusBarStyle:(UIStatusBarStyle)style;

- (void)resignKeyboard;

- (void)startLoadingAnimationInView:(UIView *)view;

- (void)stopLoadingAnimationInView:(UIView *)view type:(StopAnimationType)stopAnimationType touchUpInsideClickedEvent:(void(^)(void))completion;

- (void)showNoDataFoundInView:(UIView*)view;
- (void)removeNodataFoundInView:(UIView*)view;

@end

//
//  MIParentViewController.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIParentViewController ()
{
    NSArray *leftBarButtonItems;
    
    NSArray *rightBarButtonItems;
    
    BOOL isStatusBarHidden;
    
    UIStatusBarStyle statusBarStyle;
}
@end

@implementation MIParentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    statusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureNavigationBar];
    
    if (![appDelegate isTabBarViewControllerNull]) {
        appDelegate.tabBarViewController.tabBarView.hidden = !self.showTabBar;
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (self.showTabBar)
    {
        CGFloat sHeight = self.navigationController.navigationBar.hidden || self.navigationController.navigationBar.translucent?0.0f:[[UIApplication sharedApplication] statusBarFrame].size.height;
        CGFloat nHeight = self.navigationController.navigationBar.hidden || self.navigationController.navigationBar.translucent?0.0f:44.0f;
        CGFloat tHeight = 49.0f;
        
        CViewSetHeight(self.view, CScreenHeight - sHeight - nHeight - tHeight);
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self resignKeyboard];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




#pragma mark -
#pragma mark - General Method

- (void)configureNavigationBar
{
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //.....Generic config of UINavigationBar.....
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    if (self.view.tag == 100)
    {
       
        //...Screen in which navigation bar is white and transparent is NO.
        [self setStatusBarStyle:UIStatusBarStyleDefault];
        
        UIImage *image = [[UIImage imageNamed:@"navBackBlack"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if (IS_Ios9) [self.navigationController.navigationBar setBackIndicatorTransitionMaskImage:image];
        [self.navigationController.navigationBar setBackIndicatorImage:image];
        
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:CFontSolidoMedium((Is_iPhone_4 || Is_iPhone_5)?19:21), NSForegroundColorAttributeName:ColorBlack_202020}];
        
        [self.navigationController.navigationBar setBarTintColor:ColorWhite_FFFFFF];
        [self.navigationController.navigationBar setTintColor:ColorBlack_202020];
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
        [self.navigationController.navigationBar setTranslucent:NO];
    }
    else
    {
        
        //...Screen in which back button color is white and transparent is YES.
        [self setStatusBarStyle:UIStatusBarStyleLightContent];

        UIImage *image = [[UIImage imageNamed:@"navBackWhite"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if (IS_Ios9) [self.navigationController.navigationBar setBackIndicatorTransitionMaskImage:image];
        [self.navigationController.navigationBar setBackIndicatorImage:image];
        
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:CFontSolidoMedium((Is_iPhone_4 || Is_iPhone_5)?19:21), NSForegroundColorAttributeName:ColorWhite_FFFFFF}];
        
        [self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
        [self.navigationController.navigationBar setTintColor:ColorWhite_FFFFFF];
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
       
        [self.navigationController.navigationBar setTranslucent:YES];
        
    }
}

- (void)resignKeyboard
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}




#pragma mark -
#pragma mark - UIStatusBar

- (void)setStatusBarHidden:(BOOL)hidden
{
    isStatusBarHidden = hidden;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)setStatusBarStyle:(UIStatusBarStyle)style
{
    statusBarStyle = style;
    [self setNeedsStatusBarAppearanceUpdate];
    [[UIApplication sharedApplication] setStatusBarStyle:style];
}

- (BOOL)prefersStatusBarHidden
{
    return isStatusBarHidden;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return statusBarStyle;
}




#pragma mark -
#pragma mark - Helper Method

- (void)startLoadingAnimationInView:(UIView *)view
{
    UIView *animationView = (UIView *)[view viewWithTag:1000];
    
    if (!animationView)
    {
        animationView = [[UIView alloc] initWithFrame:CGRectZero];
        animationView.translatesAutoresizingMaskIntoConstraints = NO;
        animationView.backgroundColor = [UIColor whiteColor];
        animationView.tag = 1000;
        
       
        //....
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activityIndicator setTranslatesAutoresizingMaskIntoConstraints:NO];
        [activityIndicator setColor:ColorBlue_0090F3];
        [activityIndicator setHidesWhenStopped:YES];
        [activityIndicator setCenter:animationView.center];
        [activityIndicator startAnimating];
        [activityIndicator setTag:1001];
        
        [animationView addSubview:activityIndicator];
        [animationView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [animationView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        
        //....
        [view addSubview:animationView];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
        
    }
    else
    {
        //...
        UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView*) [view viewWithTag:1001];
        if(activityIndicator)
            [activityIndicator startAnimating];
        
        //...
        UIImageView *imageView = (UIImageView*) [animationView viewWithTag:1002];
        
        if (imageView)
            [imageView setHidden:YES];
        
        //...
        UILabel *lblTitle = (UILabel*)[animationView viewWithTag:1003];
        
        if(lblTitle)
            [lblTitle setHidden:YES];

    }
}

- (void)stopLoadingAnimationInView:(UIView *)view type:(StopAnimationType)stopAnimationType touchUpInsideClickedEvent:(void(^)())completion
{
    UIView *animationView = (UIView *)[view viewWithTag:1000];
    
    if (animationView)
    {
        if(stopAnimationType == StopAnimationTypeRemove)
            [animationView removeFromSuperview];
        
        else
        {
            //...
            UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView*) [view viewWithTag:1001];
            if(activityIndicator)
                [activityIndicator stopAnimating];
            
            
            UIImageView *imageView = (UIImageView*) [animationView viewWithTag:1002];
            
            //...
            if(!imageView)
            {
                //...
                imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
                [imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
                [imageView setTag:1002];
                
                
                [animationView addSubview:imageView];
                [animationView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
                [animationView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeCenterY multiplier:0.75 constant:0]];
                
            }
            else
                [imageView setHidden:NO];
            
            
            //...
            UILabel *lblTitle = (UILabel*)[animationView viewWithTag:1003];
            
            if(!lblTitle)
            {
                
                lblTitle = [[UILabel alloc] initWithFrame:CGRectZero];
                [lblTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
                [lblTitle setTag:1003];
                [lblTitle setTextColor:ColorGray_857E7E];
                [lblTitle setFont:CFontSolidoMedium(20)];
                [lblTitle setTextAlignment:NSTextAlignmentCenter];
                [lblTitle setNumberOfLines:0];
                
                [animationView addSubview:lblTitle];
                [animationView addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeLeft multiplier:1 constant:12]];
                [animationView addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeRight multiplier:1 constant:-12]];
                [animationView addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeBottom multiplier:1 constant:16]];
                
            }
            else
                [lblTitle setHidden:NO];
            
            switch (stopAnimationType)
            {
                case StopAnimationTypeDataNotFound:
                {
                    [imageView setImage:[UIImage imageNamed:@"nofeed"]];
                    [lblTitle setText:CMessageNoResultFound];
                    
                    break;
                }
                case StopAnimationTypeErrorTapToRetry:
                {
                    [imageView setImage:[UIImage imageNamed:@"worried"]];
                    [lblTitle setText:CErrorTapToRetry];
                    
                    [self setTouchUpInsideViewClicked:completion];
                    [animationView setUserInteractionEnabled:completion?YES:NO];
                    
                    [animationView.gestureRecognizers enumerateObjectsUsingBlock:^(__kindof UIGestureRecognizer *obj, NSUInteger idx, BOOL *stop) {
                        if ([obj isKindOfClass:[UITapGestureRecognizer class]]) {
                            [animationView removeGestureRecognizer:obj];
                            *stop = YES;
                        }
                    }];
                    
                    if (completion)
                    {
                        [animationView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizer:)]];
                    }
                    
                    break;
                }
                    
                default:
                {
                    
                    break;
                }
                    
            }
            
        }
        
    }
    
}

- (void)tapGestureRecognizer:(UITapGestureRecognizer *)tapGesture
{
    if (self.touchUpInsideViewClicked) {
        self.touchUpInsideViewClicked();
    }
}

- (void)showNoDataFoundInView:(UIView*)view
{
    
    UIView *noDataView = (UIView *)[view viewWithTag:2000];
    
    if (!noDataView)
    {
        noDataView = [[UIView alloc] initWithFrame:CGRectZero];
        noDataView.translatesAutoresizingMaskIntoConstraints = NO;
        noDataView.backgroundColor = [UIColor whiteColor];
        noDataView.tag = 2000;
        //....
        [view addSubview:noDataView];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:noDataView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:noDataView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:noDataView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:noDataView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
        
        
        UIImageView *imageView = (UIImageView*) [noDataView viewWithTag:2002];
        
        //...
        if(!imageView)
        {
            //...
            imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
            [imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
            [imageView setTag:2002];
            
            
            [noDataView addSubview:imageView];
            [noDataView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:noDataView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
            [noDataView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:noDataView attribute:NSLayoutAttributeCenterY multiplier:0.75 constant:0]];
            
        }
        
        
        
        //...
        UILabel *lblTitle = (UILabel*)[noDataView viewWithTag:2003];
        
        if(!lblTitle)
        {
            
            lblTitle = [[UILabel alloc] initWithFrame:CGRectZero];
            [lblTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
            [lblTitle setTag:2003];
            [lblTitle setTextColor:ColorGray_857E7E];
            [lblTitle setFont:CFontSolidoMedium(20)];
            [lblTitle setTextAlignment:NSTextAlignmentCenter];
            [lblTitle setNumberOfLines:0];
            
            [noDataView addSubview:lblTitle];
            [noDataView addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:noDataView attribute:NSLayoutAttributeLeft multiplier:1 constant:12]];
            [noDataView addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:noDataView attribute:NSLayoutAttributeRight multiplier:1 constant:-12]];
            [noDataView addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeBottom multiplier:1 constant:16]];
            
        }
        
        [imageView setImage:[UIImage imageNamed:@"nofeed"]];
        [lblTitle setText:CMessageNoResultFound];

    }
}

- (void)removeNodataFoundInView:(UIView*)view
{
    UIView *noDataView = (UIView *)[view viewWithTag:2000];
    
    if(noDataView)
        [noDataView removeFromSuperview];
}

#pragma mark -
#pragma mark - Action Event





@end

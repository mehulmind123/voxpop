//
//  MIGenericTextField.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIGenericTextField.h"

@implementation MIGenericTextField

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (instancetype)init
{
    self = [super init];
    if (self) [self initialize];
    return self;
}

- (void)initialize
{
    if (Is_iPhone_4 || Is_iPhone_5)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize - 2)];
    else if (Is_iPhone_6_PLUS)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize + 2)];
    
    switch (self.tag)
    {
        case 100:
        {
            [self addLeftPaddingWithWidth:CViewHeight(self)/1.5];
            [self addRightPaddingWithWidth:CViewHeight(self)/4];
            [self setPlaceHolderColor:ColorBlueSky_249CFE];
            [self setTextColor:ColorBlueSky_249CFE];
            
            break;
        }
        case 101:
        {
            [self addLeftPaddingWithWidth:CViewHeight(self)/1.5];
            [self setRightButton:[UIImage imageNamed:@"dropdown"] withSize:CGSizeMake(CViewHeight(self), CViewHeight(self))];
            [self setPlaceHolderColor:ColorBlueSky_249CFE];
            [self setTextColor:ColorBlueSky_249CFE];
            
            break;
        }
        case 102:
        {
            [self addLeftPaddingWithWidth:CViewHeight(self)/4];
            [self setRightButton:[UIImage imageNamed:@"dropdown"] withSize:CGSizeMake(CViewHeight(self)/1.7, CViewHeight(self))];
            [self setTextColor:ColorBlueSky_249CFE];
            break;
        }
        case 103:
        {
            [self addLeftPaddingWithWidth:CViewHeight(self)/1.5];
            [self setRightButton:[UIImage imageNamed:@"next4"] withSize:CGSizeMake(CViewHeight(self), CViewHeight(self))];
            [self setPlaceHolderColor:ColorBlueSky_249CFE];
            [self setTextColor:ColorBlueSky_249CFE];
        }
        default:
        {
            break;
        }
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    switch (self.tag)
    {
        case 100:
        {
            CGRect lFrame = self.leftView.frame;
            CGRect rFrame = self.rightView.frame;
            
            [self.leftView setFrame:CGRectMake(lFrame.origin.x, lFrame.origin.y, CViewHeight(self)/1.5, lFrame.size.height)];
            [self.rightView setFrame:CGRectMake(rFrame.origin.x, rFrame.origin.y, CViewHeight(self)/4, rFrame.size.height)];
            
            [self.layer setCornerRadius:CViewHeight(self)/2];
            break;
        }
        case 101:
        {
            CGRect lFrame = self.leftView.frame;
            CGRect rFrame = self.rightView.frame;
            
            [self.leftView setFrame:CGRectMake(lFrame.origin.x, lFrame.origin.y, CViewHeight(self)/1.5, lFrame.size.height)];
            [self.rightView setFrame:CGRectMake(rFrame.origin.x, rFrame.origin.y, CViewHeight(self), CViewHeight(self))];
            
            [self.layer setCornerRadius:CViewHeight(self)/2];
            break;
        }
        case 102:
        {
            CGRect lFrame = self.leftView.frame;
            CGRect rFrame = self.rightView.frame;
            
            [self.leftView setFrame:CGRectMake(lFrame.origin.x, lFrame.origin.y, CViewHeight(self)/4, lFrame.size.height)];
            [self.rightView setFrame:CGRectMake(rFrame.origin.x, rFrame.origin.y, CViewHeight(self)/1.7, CViewHeight(self))];
            
            [self.layer setCornerRadius:CViewHeight(self)/2];
            break;
        }
        default:
        {
            [self.layer setCornerRadius:CViewHeight(self)/2];
            break;
        }
    }
}

@end

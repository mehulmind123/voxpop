//
//  MIGenericTableView.m
//  VoxPop
//
//  Created by mac-00012 on 29/07/19.
//  Copyright © 2019 Jignesh-0007. All rights reserved.
//

#import "MIGenericTableView.h"

@implementation MIGenericTableView

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    if (!CGSizeEqualToSize(self.bounds.size, [self intrinsicContentSize]))
        [self invalidateIntrinsicContentSize];
}

- (CGSize)intrinsicContentSize
{
    CGSize intrinsicContentSize = self.contentSize;
    return intrinsicContentSize;
}

@end

//
//  MIBubbleViewLayout.h
//  VoxPop
//
//  Created by mac-0007 on 23/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MIBubbleViewLayoutDelegate <NSObject>

- (CGSize)collectionView:(UICollectionView *)collectionView itemSizeAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface MIBubbleViewLayout : UICollectionViewFlowLayout

- (id)initWithDelegate:(id <MIBubbleViewLayoutDelegate>)delegate;

@end

//
//  MIGenericTableView.h
//  VoxPop
//
//  Created by mac-00012 on 29/07/19.
//  Copyright © 2019 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MIGenericTableView : UITableView

@end

NS_ASSUME_NONNULL_END

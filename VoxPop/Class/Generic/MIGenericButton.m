//
//  MIGenericButton.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIGenericButton.h"

@interface MIGenericButton ()
{
    CAGradientLayer *gradient;
}
@end

@implementation MIGenericButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (instancetype)init
{
    self = [super init];
    if (self) [self initialize];
    return self;
}

- (void)initialize
{
    NSString *fontName = self.titleLabel.font.fontName;
    CGFloat fontSize = self.titleLabel.font.pointSize;
    
    if (Is_iPhone_4 || Is_iPhone_5)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 2)];
    else if (Is_iPhone_6_PLUS)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize + 2)];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    switch (self.tag)
    {
        case 100:
        {
            if (gradient == nil) {
                gradient = [CAGradientLayer layer];
                [gradient setColors:@[(id)ColorBlueSky_1DB4FF.CGColor,
                                      (id)ColorBlue_007FD4.CGColor]];
                [self.layer insertSublayer:gradient atIndex:0];
                
                [self.layer setShadowColor:ColorBlue_2F7AC1.CGColor];
                [self.layer setShadowOffset:CGSizeMake(0, -2)];
                [self.layer setShadowOpacity:1.0];
                [self.layer setShadowRadius:1];
                [self setBackgroundColor:[UIColor clearColor]];
            }
            
            [gradient setFrame:self.bounds];
            [gradient setCornerRadius:CViewHeight(self)/2];
            [self.layer setCornerRadius:CViewHeight(self)/2];
            
            break;
        }
        case 200:
        {
            if (gradient == nil) {
                gradient = [CAGradientLayer layer];
                [gradient setColors:@[(id)ColorBlue_0064ED.CGColor,
                                      (id)ColorBlue_1190E7.CGColor,
                                      (id)ColorBlue_0090F3.CGColor,
                                      (id)ColorBlue_00AAED.CGColor]];
                [self.layer insertSublayer:gradient atIndex:0];
                
                [self setBackgroundColor:[UIColor clearColor]];
            }
            
            [gradient setFrame:self.bounds];
            [self.layer setCornerRadius:CViewHeight(self)/2];
            [self setClipsToBounds:YES];
            
            break;
        }
        case 300:
        {
            if (gradient == nil) {
                gradient = [CAGradientLayer layer];
                [gradient setColors:@[(id)ColorBlue_0064ED.CGColor,
                                      (id)ColorBlue_1190E7.CGColor,
                                      (id)ColorBlue_0090F3.CGColor,
                                      (id)ColorBlue_00AAED.CGColor]];
                [self.layer insertSublayer:gradient atIndex:0];
                
                [self setBackgroundColor:[UIColor clearColor]];
            }
            
            [gradient setFrame:self.bounds];
            [self setClipsToBounds:YES];
            
            break;
        }
        default:
        {
            [self.layer setCornerRadius:CViewHeight(self)/2];
            break;
        }
    }
}

@end

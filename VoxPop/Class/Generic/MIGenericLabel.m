//
//  MIGenericLabel.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIGenericLabel.h"

@interface MIGenericLabel ()
{
    CAGradientLayer *gradient;
}
@end

@implementation MIGenericLabel

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (instancetype)init
{
    self = [super init];
    if (self) [self initialize];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self initialize];
    return self;
}

- (void)initialize
{
    if (Is_iPhone_4 || Is_iPhone_5)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize - 2)];
    else if (Is_iPhone_6_PLUS)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize + 2)];
    
    switch (self.tag)
    {
        default:
        {
            break;
        }
    }
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    switch (self.tag)
    {
        case 100:
        {
            break;
        }
        
        default:
        {
            break;
        }
    }
}

@end

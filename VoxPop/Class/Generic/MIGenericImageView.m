//
//  MIGenericImageView.m
//  VoxPop
//
//  Created by mac-0007 on 23/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIGenericImageView.h"

@implementation MIGenericImageView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (instancetype)init
{
    self = [super init];
    if (self) [self initialize];
    return self;
}

- (void)initialize
{
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    switch (self.tag)
    {
        case 100:
        {
            [self.layer setCornerRadius:CViewHeight(self)/2];
            [self.layer setBorderColor:ColorWhite_FFFFFF.CGColor];
            [self.layer setBorderWidth:2];
            break;
        }
        default:
        {
            break;
        }
    }
}


@end

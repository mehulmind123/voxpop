//
//  MIGenericView.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIGenericView.h"

@interface MIGenericView ()
{
    CAGradientLayer *gradient;
}
@end

@implementation MIGenericView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self initialize];
    return self;
}

- (void)initialize
{
    switch (self.tag)
    {
        default:
        {
            break;
        }
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    switch (self.tag)
    {
        case 100:
        {
            if (gradient == nil) {
                gradient = [CAGradientLayer layer];
                [gradient setColors:@[(id)ColorBlue_0064ED.CGColor,
                                      (id)ColorBlue_1190E7.CGColor,
                                      (id)ColorBlue_0090F3.CGColor,
                                      (id)ColorBlue_00AAED.CGColor]];
                
                [self.layer insertSublayer:gradient atIndex:0];
                [self.layer setShadowColor:ColorBlack_202020.CGColor];
                [self.layer setShadowOffset:CGSizeZero];
                [self.layer setShadowOpacity:0.6];
                [self.layer setShadowRadius:3];

                [self setBackgroundColor:[UIColor clearColor]];
                [gradient setCornerRadius:16];
            }
            
            [gradient setFrame:self.bounds];
            
            break;
        }
        case 200:
        {
            [self.layer setShadowColor:[UIColor lightGrayColor].CGColor];
            [self.layer setShadowOffset:CGSizeMake(0, 0)];
            [self.layer setShadowOpacity:0.8f];
            [self.layer setShadowRadius:5];
            [self.layer setCornerRadius:10];
            
            break;
        }
        case 300:
        {
            [self.layer setShadowColor:[UIColor grayColor].CGColor];
            [self.layer setShadowOffset:CGSizeMake(0, 0)];
            [self.layer setShadowOpacity:0.8f];
            [self.layer setShadowRadius:10];
            [self.layer setCornerRadius:25];
            
            break;
        }
        case 400:
        {
            if (gradient == nil) {
                gradient = [CAGradientLayer layer];
                [gradient setColors:@[(id)ColorBlue_0064ED.CGColor,
                                      (id)ColorBlue_1190E7.CGColor,
                                      (id)ColorBlue_0090F3.CGColor,
                                      (id)ColorBlue_00AAED.CGColor]];
                
                [self.layer insertSublayer:gradient atIndex:0];
                [self setBackgroundColor:[UIColor clearColor]];
                [gradient setCornerRadius:CViewHeight(self)/2];
            }
            
            [gradient setFrame:self.bounds];
            
            break;
        }
        case 500:
        {
            if (gradient == nil) {
                gradient = [CAGradientLayer layer];
                [gradient setColors:@[(id)ColorBlue_0064ED.CGColor,
                                      (id)ColorBlue_1190E7.CGColor,
                                      (id)ColorBlue_0090F3.CGColor,
                                      (id)ColorBlue_00AAED.CGColor]];
                
                [self.layer insertSublayer:gradient atIndex:0];
                [self.layer setShadowColor:ColorBlack_202020.CGColor];
                [self.layer setShadowOffset:CGSizeZero];
                [self.layer setShadowOpacity:0.6];
                [self.layer setShadowRadius:3];
                
                [self setBackgroundColor:[UIColor clearColor]];
                [gradient setCornerRadius:10];
            }
            
            [gradient setFrame:self.bounds];
            
            break;
        }
        default:
        {
            break;
        }
    }
}

@end

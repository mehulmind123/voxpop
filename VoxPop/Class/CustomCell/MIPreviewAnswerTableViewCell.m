//
//  MIPreviewAnswerTableViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 25/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIPreviewAnswerTableViewCell.h"

@implementation MIPreviewAnswerTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.lblQuestion.preferredMaxLayoutWidth = self.lblAnswer.preferredMaxLayoutWidth = CScreenWidth - 24 - 24;
}
@end

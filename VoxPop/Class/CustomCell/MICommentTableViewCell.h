//
//  MICommentTableViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MICommentTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblUsername;
@property (nonatomic, weak) IBOutlet UILabel *lblTime;
@property (nonatomic, weak) IBOutlet UILabel *lblComment;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (nonatomic, weak) IBOutlet UIButton *btnReport;
@end

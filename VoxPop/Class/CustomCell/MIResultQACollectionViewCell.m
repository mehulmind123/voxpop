//
//  MIResultQACollectionViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 28/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIResultQACollectionViewCell.h"
#import "MIResultAnswerTableViewCell.h"
#import "MIBarChartView.h"
#import "XYPieChart.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>

@interface MIResultQACollectionViewCell () <ORRingChartViewDatasource>
{
    NSArray *arrAnswers;
    NSArray *arrColors;
    
    MIBarChartView *barCharView;
//    XYPieChart *pieChart;
    ORRingChartView *ringChart;
}

@property (nonatomic, strong) NSArray *datasource;
@property (nonatomic, strong) NSArray *graidentColors;
@property (nonatomic, assign) BOOL clockwise;
@property (nonatomic, assign) BOOL neatInfoLine;
@property (nonatomic, assign) ORRingChartStyle style;

@property (nonatomic, strong) MIResultAnswerTableViewCell *prototypeCell;
@property (nonatomic, assign) NSInteger graphType;
@end


@implementation MIResultQACollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    arrColors = @[Color1_5B7EF4,
                  Color2_F671D2,
                  Color3_3FCF7D,
                  Color4_F66CAD,
                  Color5_F69016,
                  Color6_2B2B2B,
                  Color7_F42A67,
                  Color8_F8CA04,
                  Color9_6164CB,
                  Color10_2012BB];
    
    
    //...UITableView
    [_tblAnswers registerNib:[UINib nibWithNibName:@"MIResultAnswerTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIResultAnswerTableViewCell"];
    [_scrollQA.layer setCornerRadius:25];
}

- (void)configureCellWithInfo:(NSDictionary *)dictInfo indexPath:(NSIndexPath *)indexPath
{
    //......
    [self.lblQueNo setText:[NSString stringWithFormat:@"%ld", (long)indexPath.item + 1]];
    
    self.graphType = [dictInfo integerForKey:@"graph_type"];
    if ([dictInfo integerForKey:@"question_type"] == 1)
    {
        [self.lblQue setText:dictInfo[@"question"]];
        [self.btnQuestion setEnabled:NO];
    }
    else
    {
        [self.lblQue setText:@"Click here to watch video for the question."];
        [self.btnQuestion setEnabled:YES];
    }
    //....
    arrAnswers = [dictInfo valueForKey:@"answers"];
    [self.tblAnswers reloadData];
    
    //....Attributed String
    NSArray *selectedAnswer = [[arrAnswers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"responded == 1"]] valueForKeyPath:@"answer_text"];
    [_lblYourAns setText:[NSString stringWithFormat:@"Your Answer(s): %@", [selectedAnswer componentsJoinedByString:@", "]]];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:_lblYourAns.text];
    [attString addAttribute:NSForegroundColorAttributeName value:ColorGray_BBBBBB range:NSMakeRange(0, _lblYourAns.text.length)];
    [attString addAttribute:NSForegroundColorAttributeName value:ColorGray_666666 range:NSMakeRange(0, [_lblYourAns.text rangeOfString:@":"].location + 1)];
    [_lblYourAns setAttributedText:attString];

    [barCharView removeFromSuperview];
    [ringChart removeFromSuperview];
    
    //...
    if ( self.graphType == 1)
    {
        //... BarChart
        barCharView = [MIBarChartView barChartViewWithAnswers:arrAnswers];
        [barCharView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.viewContainer addSubview:barCharView];
        
        [self.viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:barCharView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.viewContainer attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
        [self.viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:barCharView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.viewContainer attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];
        [self.viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:barCharView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.viewContainer attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
        [self.viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:barCharView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.viewContainer attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
    }
    else
    {

        CGFloat pieChartSize = (CScreenWidth * 235) / 351;
        ringChart = [[ORRingChartView alloc] initWithFrame:CGRectMake(0, 0, pieChartSize, pieChartSize)];
        _datasource = [arrAnswers valueForKeyPath:@"percentage"];
        _graidentColors = @[@[Color1_5B7EF4],
                            @[Color2_F671D2],
                            @[Color3_3FCF7D],
                            @[Color4_F66CAD],
                            @[Color5_F69016],
                            @[Color6_2B2B2B],
                            @[Color7_F42A67],
                            @[Color8_F8CA04],
                            @[Color9_6164CB],
                            @[Color10_2012BB]];
        ringChart.dataSource = self;
        ringChart.style = 1;
        _neatInfoLine = true;
        ringChart.config.ringLineWidth = 0.5;
        ringChart.config.infoLineWidth = 1;
        ringChart.config.animateDuration = 1.5;
        [self.viewContainer addSubview:ringChart];
        [ringChart reloadData];
        
        [self.viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:ringChart attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.viewContainer attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
        [self.viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:ringChart attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.viewContainer attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];
        [self.viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:ringChart attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.viewContainer attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
        [self.viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:ringChart attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.viewContainer attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
    }
    
    
    [self.btnQuestion touchUpInsideClicked:^{
    
        NSString *youtubeIdentifier = [[[dictInfo stringValueForJSON:@"question"] componentsSeparatedByString:@"v="] lastObject];
        
        XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:youtubeIdentifier];
        videoPlayerViewController.preferredVideoQualities = @[@(XCDYouTubeVideoQualitySmall240), @(XCDYouTubeVideoQualityMedium360)];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewController.moviePlayer];
        [self.viewController presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
    }];
}


#pragma mark -- ORRingChartViewDatasource

- (NSInteger)numberOfRingsOfChartView:(ORRingChartView *)chartView {
    return _datasource.count;
}

- (CGFloat)chartView:(ORRingChartView *)chartView valueAtRingIndex:(NSInteger)index {
    return [_datasource[index] floatValue];
}

- (NSArray<UIColor *> *)chartView:(ORRingChartView *)chartView graidentColorsAtRingIndex:(NSInteger)index {
    return _graidentColors[index];
}

- (UIView *)chartView:(ORRingChartView *)chartView viewForTopInfoAtRingIndex:(NSInteger)index {
    UILabel *label = [chartView dequeueTopInfoViewAtIndex:index];
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?8:(Is_iPhone_6_PLUS)?12:10;
    if (!label) {
        label = [UILabel new];
        [label setFont:CFontRobotoBold(fontSize)];
        label.textColor = [UIColor blackColor];
    }
    label.text = [NSString stringWithFormat:@"%.02f%%", [_datasource[index] floatValue]];
    [label sizeToFit];
    return label;
}



#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrAnswers.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MIResultAnswerTableViewCell";
    MIResultAnswerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (MIResultAnswerTableViewCell *)prototypeCell
{
    static NSString *identifier = @"MIResultAnswerTableViewCell";
    
    if (!_prototypeCell)
        _prototypeCell = [_tblAnswers dequeueReusableCellWithIdentifier:identifier];
    
    return _prototypeCell;
}

- (void)configureCell:(MIResultAnswerTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictAnswer = arrAnswers[indexPath.row];
    [cell.viewColor setBackgroundColor:arrColors[indexPath.row]];
    [cell.lblAnsNo setText:[NSString stringWithFormat:@"%@%ld:", self.graphType == 1 ? @"A" :@"ANS " , (long)indexPath.row + 1]];
    [cell.lblAnswer setText:dictAnswer[@"answer_text"]];
}


#pragma mark -
#pragma mark - XCDYouTubeVideoPlayer Observer

- (void) moviePlayerPlaybackDidFinish:(NSNotification *)notification
{
    MPMovieFinishReason finishReason = [notification.userInfo[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] integerValue];
    NSString *reason = @"Unknown";
    
    switch (finishReason)
    {
        case MPMovieFinishReasonPlaybackEnded:
            reason = @"Playback Ended";
            break;
        case MPMovieFinishReasonPlaybackError:
            reason = @"Playback Error";
            break;
        case MPMovieFinishReasonUserExited:
            reason = @"User Exited";
            break;
    }
    
    NSLog(@"Finish Reason: %@", reason);
}
@end

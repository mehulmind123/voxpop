//
//  MITableViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 26/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MITableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblSubtitle;
@property (nonatomic, weak) IBOutlet UIButton *btnLockUnlock;
@property (nonatomic, weak) IBOutlet UIButton *btnNextRemove;
@property (nonatomic, weak) IBOutlet UISwitch *switchNotification;
@end

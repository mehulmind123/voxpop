//
//  MISurveyTableViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 22/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MISurveyTableViewCell.h"
#import "MITopicCollectionViewCell.h"

@interface MISurveyTableViewCell () <MIBubbleViewLayoutDelegate>
{
    CAGradientLayer *_gradient;
    NSMutableArray *arrTopics;
    NSIndexPath *indexPathSurvey;
}
@end

@implementation MISurveyTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _gradient = [CAGradientLayer layer];
    [_viewContainer.layer insertSublayer:_gradient atIndex:0];
    [_viewContainer setBackgroundColor:ColorWhite_FFFFFF];
    [_viewContainer.layer setCornerRadius:16];
    
    //...UICollectionView
    MIBubbleViewLayout *layout = [[MIBubbleViewLayout alloc] initWithDelegate:self];
    [layout setMinimumLineSpacing:6.0f];
    [layout setMinimumInteritemSpacing:6.0f];
    
    [_collVTopics setCollectionViewLayout:layout];
    [_collVTopics setAllowsMultipleSelection:YES];
    [_collVTopics registerNib:[UINib nibWithNibName:@"MITopicCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MITopicCollectionViewCell"];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [_btnBlockCustomer.layer setCornerRadius:CViewHeight(_btnBlockCustomer)/2];
    
    [_viewContainer layoutIfNeeded];
    [_gradient setFrame:_viewContainer.layer.bounds];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)configureSurveyWithData:(NSDictionary *)dictData andIndexPath:(NSIndexPath *)indexPath
{
    
    [_lblSurveyTitle setText:[dictData stringValueForJSON:@"title"]];
    [_lblResponded setText:[NSString stringWithFormat:@"%@ users\nresponded",[dictData stringValueForJSON:@"total_responded_users"]]];
    
    [_btnComments setHidden:![dictData booleanForKey:@"is_comment_enabled"]];
    
    
    NSDictionary *dictSub = dictData[@"subscriber"];
    [_lblSubscriber setText:[dictSub stringValueForJSON:@"name"]];
    [_lblContactName setText:[dictSub stringValueForJSON:@"contact_name"]];
    [_imgVSubscriber sd_setImageWithURL:[appDelegate imageURL:[dictSub stringValueForJSON:@"profile_picture"] withSize:_imgVSubscriber.frame.size]];

    
    
    NSDictionary *dictCat = dictData[@"category"];
    if([dictCat count])
    {
        NSMutableArray *arrTempTopic = [NSMutableArray new];
        [arrTempTopic addObject:dictCat];
        [arrTempTopic addObjectsFromArray:[dictCat valueForKey:@"sub_categories"]];
        
        [self configureCollectionView:arrTempTopic];
    }
    else
        [_cnHeightCollVTopics setConstant:0];
    
    //....Attributed String
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?11:(Is_iPhone_6_PLUS)?15:13;
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:_lblExpiration.text];
//    [attString addAttribute:NSFontAttributeName value:CFontRobotoLight(fontSize) range:NSMakeRange(0, _lblExpiration.text.length)];
//    [attString addAttribute:NSFontAttributeName value:CFontRobotoMedium(fontSize) range:NSMakeRange(0, [_lblExpiration.text rangeOfString:@":"].location + 1)];
    [_lblExpiration setAttributedText:attString];
    
    [self configureSurveyThemeWithIndexPath:indexPath];

}
- (void)configureSurveyThemeWithIndexPath:(NSIndexPath *)indexPath
{
    indexPathSurvey = indexPath;
    if (indexPathSurvey.row % 3 == 0)
    {
        [_gradient setColors:@[(id)ColorPink_CA71F3.CGColor,
                               (id)ColorPink_F371D0.CGColor]];
        
        [_btnBlockCustomer setBackgroundColor:ColorPink_BB3DF0];
        
        [_btnPremise.layer setShadowColor:ColorPink_CE41A9.CGColor];
        [_btnPremise.layer setShadowOffset:CGSizeMake(2, 2)];
        [_btnPremise.layer setShadowOpacity:1.0];
        [_btnPremise.layer setShadowRadius:0];
        
        [_btnSurvey.layer setShadowColor:ColorPink_CE41A9.CGColor];
        [_btnSurvey.layer setShadowOffset:CGSizeMake(2, 2)];
        [_btnSurvey.layer setShadowOpacity:1.0];
        [_btnSurvey.layer setShadowRadius:0];
        
        [_btnComments.layer setShadowColor:ColorPink_CE41A9.CGColor];
        [_btnComments.layer setShadowOffset:CGSizeMake(2, 2)];
        [_btnComments.layer setShadowOpacity:1];
        [_btnComments.layer setShadowRadius:0];
    }
    else if (indexPathSurvey.row % 3 == 1)
    {
        [_gradient setColors:@[(id)ColorGreen_24E9A6.CGColor,
                               (id)ColorBlueSky_06A8F1.CGColor]];
        
        [_btnBlockCustomer setBackgroundColor:ColorGreen_3ABD83];
        
        [_btnPremise.layer setShadowColor:ColorBlue_3490C2.CGColor];
        [_btnPremise.layer setShadowOffset:CGSizeMake(2, 2)];
        [_btnPremise.layer setShadowOpacity:1.0];
        [_btnPremise.layer setShadowRadius:0];
        
        [_btnSurvey.layer setShadowColor:ColorBlue_3490C2.CGColor];
        [_btnSurvey.layer setShadowOffset:CGSizeMake(2, 2)];
        [_btnSurvey.layer setShadowOpacity:1.0];
        [_btnSurvey.layer setShadowRadius:0];
        
        [_btnComments.layer setShadowColor:ColorBlue_3490C2.CGColor];
        [_btnComments.layer setShadowOffset:CGSizeMake(2, 2)];
        [_btnComments.layer setShadowOpacity:1.0];
        [_btnComments.layer setShadowRadius:0];
    }
    else
    {
        [_gradient setColors:@[(id)ColorOrange_FBC28B.CGColor,
                               (id)ColorPinkTone_F48F9D.CGColor]];
        
        [_btnBlockCustomer setBackgroundColor:ColorOrange_D28C61];
        
        [_btnPremise.layer setShadowColor:ColorPinkTone_D36B7A.CGColor];
        [_btnPremise.layer setShadowOffset:CGSizeMake(2, 2)];
        [_btnPremise.layer setShadowOpacity:1.0];
        [_btnPremise.layer setShadowRadius:0];
        
        [_btnSurvey.layer setShadowColor:ColorPinkTone_D36B7A.CGColor];
        [_btnSurvey.layer setShadowOffset:CGSizeMake(2, 2)];
        [_btnSurvey.layer setShadowOpacity:1.0];
        [_btnSurvey.layer setShadowRadius:0];
        
        [_btnComments.layer setShadowColor:ColorPinkTone_D36B7A.CGColor];
        [_btnComments.layer setShadowOffset:CGSizeMake(2, 2)];
        [_btnComments.layer setShadowOpacity:1];
        [_btnComments.layer setShadowRadius:0];
    }
}
- (void)configureCollectionView:(NSArray *)topics
{
    arrTopics = [[NSMutableArray alloc] initWithArray:topics];
    
    CGFloat x = 0.0f;
    CGFloat y = 0.0f;
    NSInteger numberOfItems = arrTopics.count;
    CGSize iSize;
    
    for (int itemIndex = 0; itemIndex < numberOfItems; itemIndex ++)
    {
        iSize = [[[arrTopics[itemIndex] stringValueForJSON:@"category_name"] uppercaseString] sizeWithAttributes:@{NSFontAttributeName : CFontSolidoMedium(12)}];
        
        iSize.height = 23;
        iSize.width += CTextPadding*2;
        
        if (iSize.width > CViewWidth(_collVTopics))
            iSize.width = CViewWidth(_collVTopics);
        
        CGRect itemRect = CGRectMake(x, y, iSize.width, iSize.height);
        if (x > 0 && (x + iSize.width + CMinimumInteritemSpacing > CViewWidth(_collVTopics)))
        {
            itemRect.origin.x = 0.0f;
            itemRect.origin.y = y + iSize.height + CMinimumLineSpacing;
        }
        
        x = itemRect.origin.x + iSize.width + CMinimumInteritemSpacing;
        y = itemRect.origin.y;
        
    }
    y += iSize.height + CMinimumLineSpacing;
    
    [_cnHeightCollVTopics setConstant:(arrTopics.count > 0)?y:0];
    [_collVTopics reloadData];
    

}



#pragma mark
#pragma mark - UICollectionView Delegate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrTopics.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *indentifier = @"MITopicCollectionViewCell";
    MITopicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifier forIndexPath:indexPath];
    
    if (indexPathSurvey.row % 3 == 0)
        [cell.contentView setBackgroundColor:(indexPath.row == 0)?ColorPink_5F185A:ColorPink_F09CE6];
    else if (indexPathSurvey.row % 3 == 1)
        [cell.contentView setBackgroundColor:(indexPath.row == 0)?ColorBlueSky_318DAF:ColorBlueSky_56CEE8];
    else
        [cell.contentView setBackgroundColor:(indexPath.row == 0)?ColorOrange_EF9138:ColorPinkTone_F9BBB6];
    
    [cell.lblTopic setText:[[arrTopics[indexPath.row] stringValueForJSON:@"category_name"] uppercaseString]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView itemSizeAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = [[arrTopics[indexPath.row] stringValueForJSON:@"category_name"] uppercaseString];
    CGSize size = [title sizeWithAttributes:@{NSFontAttributeName:CFontSolidoMedium(12)}];
    size.height = 23;
    size.width += CTextPadding*2;
    
    if (size.width > CViewWidth(collectionView))
        size.width = CViewWidth(collectionView);
    return size;
}

@end

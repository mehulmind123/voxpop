//
//  MISurveyQACollectionViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MISurveyQACollectionViewCell.h"
#import "MIAnswerTableViewCell.h"

@interface MISurveyQACollectionViewCell ()
{
    NSMutableArray *arrAnswers;
    BOOL isMultipleSelection;
}
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cnHeightTblAnswers;
@end

@implementation MISurveyQACollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    if (!CGSizeEqualToSize(self.bounds.size, [self intrinsicContentSize]))
        [self invalidateIntrinsicContentSize];
}

- (CGSize)intrinsicContentSize
{
    return [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
}

- (void)setQAWithData:(NSDictionary *)dictQA
{
    arrAnswers = [[NSMutableArray alloc] initWithArray:dictQA[@"answers"]];
    [_lblQuestion setText:dictQA[@"question"]];
    [_tblAnswers setAllowsMultipleSelection:[dictQA booleanForKey:@"isMultiple"]];
    
    if (_tblAnswers.allowsMultipleSelection)
        [_lblAnswerType setText:@"(Multiple Answers)"];
    else
        [_lblAnswerType setText:@"(Choose 1)"];
    
    [_tblAnswers reloadData];
    [_cnHeightTblAnswers setConstant:_tblAnswers.contentSize.height];
}

#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrAnswers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MIAnswerTableViewCell";
    MIAnswerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (_tblAnswers.allowsMultipleSelection) {
        [cell.btnRadiobox setHidden:YES];
        [cell.btnCheckbox setHidden:NO];
    } else {
        [cell.btnRadiobox setHidden:NO];
        [cell.btnCheckbox setHidden:YES];
    }
    
    [cell.lblAnswer setText:arrAnswers[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
@end

//
//  MITableViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 26/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MITableViewCell.h"

@implementation MITableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    [self.lblTitle setPreferredMaxLayoutWidth:CScreenWidth - 75];
    [self.lblSubtitle setPreferredMaxLayoutWidth:CScreenWidth - 99];
}
@end

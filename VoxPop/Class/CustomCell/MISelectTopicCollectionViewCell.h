//
//  MISelectTopicCollectionViewCell.h
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISelectTopicCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIImageView *imgVBack;
@property (nonatomic, weak) IBOutlet UIView *vContent;
@property (nonatomic, weak) IBOutlet UIButton *btnCheck;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIView *viewCount;
@property (nonatomic, weak) IBOutlet UILabel *lblCount;
@property (nonatomic, weak) IBOutlet UIImageView *imgVInterestedCount;
@property (nonatomic, weak) IBOutlet UIButton *btnNext;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cnBottomImgBack;

@end

//
//  MIBarCollectionViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 28/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIBarCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblPercentage;
@property (nonatomic, weak) IBOutlet UIView *viewBar;
@property (nonatomic, weak) IBOutlet UILabel *lblAnsNo;

- (void)setBarValue:(CGFloat)barValue withIndexPath:(NSIndexPath *)indexPath;
@end

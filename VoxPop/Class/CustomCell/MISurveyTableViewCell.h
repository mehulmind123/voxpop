//
//  MISurveyTableViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 22/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISurveyTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *viewContainer;
@property (nonatomic, weak) IBOutlet UIImageView *imgVSubscriber;
@property (nonatomic, weak) IBOutlet UILabel *lblSubscriber;
@property (nonatomic, weak) IBOutlet UILabel *lblContactName;
@property (nonatomic, weak) IBOutlet UIButton *btnBlockCustomer;
@property (nonatomic, weak) IBOutlet UILabel *lblSurveyTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblExpiration;
@property (nonatomic, weak) IBOutlet UILabel *lblResponded;
@property (nonatomic, weak) IBOutlet UICollectionView *collVTopics;
@property (nonatomic, weak) IBOutlet UIButton *btnSurvey;
@property (nonatomic, weak) IBOutlet UIButton *btnComments;
@property (nonatomic, weak) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIButton *btnPremise;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cnHeightCollVTopics;

- (void)configureSurveyWithData:(NSDictionary *)dictData andIndexPath:(NSIndexPath *)indexPath;
- (void)configureCollectionView:(NSArray *)topics;
- (void)configureSurveyThemeWithIndexPath:(NSIndexPath *)indexPath;

@end

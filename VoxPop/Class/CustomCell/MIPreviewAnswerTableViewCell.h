//
//  MIPreviewAnswerTableViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 25/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIPreviewAnswerTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblQuestion;
@property (nonatomic, weak) IBOutlet UILabel *lblAnswer;
@end

//
//  MISelectTopicCollectionViewCell.m
//  VoxPop
//
//  Created by mac-00014 on 8/25/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MISelectTopicCollectionViewCell.h"

@implementation MISelectTopicCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.imgVInterestedCount setImage:[[UIImage imageNamed:@"interested_count"] stretchableImageWithLeftCapWidth:10 topCapHeight:10]];
}

@end

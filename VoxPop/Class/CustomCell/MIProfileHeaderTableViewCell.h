//
//  MIProfileHeaderTableViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 26/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIProfileHeaderTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblKey;
@property (nonatomic, weak) IBOutlet UILabel *lblValue;
@property (nonatomic, weak) IBOutlet UIImageView *imgVArrow;
@end

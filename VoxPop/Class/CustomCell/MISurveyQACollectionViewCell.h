//
//  MISurveyQACollectionViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISurveyQACollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblQuestion;
@property (nonatomic, weak) IBOutlet UILabel *lblAnswerType;
@property (nonatomic, weak) IBOutlet UITableView *tblAnswers;
@end

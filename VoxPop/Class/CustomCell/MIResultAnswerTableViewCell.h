//
//  MIResultAnswerTableViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 05/09/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIResultAnswerTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIView *viewColor;
@property (nonatomic, weak) IBOutlet UILabel *lblAnsNo;
@property (nonatomic, weak) IBOutlet UILabel *lblAnswer;
@end

//
//  MITopicCollectionViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 23/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MITopicCollectionViewCell.h"

@implementation MITopicCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.layer setCornerRadius:CViewHeight(self)/2];
}

@end

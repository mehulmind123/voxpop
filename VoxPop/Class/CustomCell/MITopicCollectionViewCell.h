//
//  MITopicCollectionViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 23/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CTextPadding                20
#define CMinimumLineSpacing         6
#define CMinimumInteritemSpacing    6

@interface MITopicCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblTopic;
@end

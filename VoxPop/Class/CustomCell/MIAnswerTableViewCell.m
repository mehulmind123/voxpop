//
//  MIAnswerTableViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIAnswerTableViewCell.h"

@implementation MIAnswerTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    [_btnCheckbox setSelected:selected];
    [_btnRadiobox setSelected:selected];
    [_lblAnswer setTextColor:selected?ColorBlueSky_409DFE:ColorGray_908D8D];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.lblAnswer.preferredMaxLayoutWidth = CScreenWidth - 40 - 37 - 37;
}
@end

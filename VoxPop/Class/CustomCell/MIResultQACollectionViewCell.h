//
//  MIResultQACollectionViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 28/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIGenericTableView.h"
#import <ORRingChartView.h>

@interface MIResultQACollectionViewCell : UICollectionViewCell
//@property (weak, nonatomic) IBOutlet ORRingChartView *ringChart;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollQA;
@property (nonatomic, weak) IBOutlet UILabel *lblQueNo;
@property (nonatomic, weak) IBOutlet UILabel *lblQue;
@property (nonatomic, weak) IBOutlet UILabel *lblYourAns;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic, weak) IBOutlet MIGenericTableView *tblAnswers;
@property (nonatomic, weak) IBOutlet UIButton *btnQuestion;
- (void)configureCellWithInfo:(NSDictionary *)dictInfo indexPath:(NSIndexPath *)indexPath;
@end

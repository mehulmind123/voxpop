//
//  MISocialAccountTableViewCell.h
//  VoxPop
//
//  Created by Mac-00014 on 05/01/18.
//  Copyright © 2018 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISocialAccountTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIButton *btnSelected;
@property (nonatomic, weak) IBOutlet UIButton *btnIcon;
@end

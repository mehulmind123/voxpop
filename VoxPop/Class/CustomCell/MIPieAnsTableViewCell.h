//
//  MIPieAnsTableViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 01/09/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIPieAnsTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIView *viewSegment;
@property (nonatomic, weak) IBOutlet UILabel *lblAns;
@end

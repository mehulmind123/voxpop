//
//  MIBarCollectionViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 28/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIBarCollectionViewCell.h"

@interface MIBarCollectionViewCell ()
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cnHeightBarView;
@end

@implementation MIBarCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setBarValue:(CGFloat)barValue withIndexPath:(NSIndexPath *)indexPath
{
    if (barValue <= 100)
    {
        //....SetBarValue
        [self layoutIfNeeded];
        CGFloat maxBarHeight = CViewHeight(self) - CViewHeight(_lblPercentage) - CViewHeight(_lblAnsNo);
        CGFloat barHeightAccordingToBarValue = (barValue * maxBarHeight) / 100;
        _cnHeightBarView.constant = barHeightAccordingToBarValue;
        
        
        //....Percentage
        _lblPercentage.text = [NSString stringWithFormat:@"%d%%", (int)barValue];
        
        
        //....AnsNo
        _lblAnsNo.text = [NSString stringWithFormat:@"A%ld", (long)indexPath.row + 1];
    }
}

@end

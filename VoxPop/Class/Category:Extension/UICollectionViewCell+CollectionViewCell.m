//
//  UICollectionViewCell+CollectionViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 31/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "UICollectionViewCell+CollectionViewCell.h"

@implementation UICollectionViewCell (CollectionViewCell)

- (UICollectionView *)collectionView
{
    id view = [self superview];
    
    while (view && [view isKindOfClass:[UICollectionView class]] == NO)
        view = [view superview];
    
    return view;
}

- (NSIndexPath *)indexPath
{
    return [self.collectionView indexPathForCell:self];
}

@end

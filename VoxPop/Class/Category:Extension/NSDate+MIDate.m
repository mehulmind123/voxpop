//
//  NSDate+MIDate.m
//  Engage
//
//  Created by mac-0007 on 16/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "NSDate+MIDate.h"

@implementation NSDate (MIDate)


+ (NSDateFormatter *)gmtDateFormatter
{
    static dispatch_once_t onceToken;
    static NSDateFormatter *gmtDateFormatter;
    dispatch_once(&onceToken, ^{
        gmtDateFormatter = [NSDateFormatter new];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [gmtDateFormatter setLocale:locale];
        [gmtDateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        
    });
    
    return gmtDateFormatter;
}
+ (NSDateFormatter *)dateFormatter
{
    static dispatch_once_t onceToken;
    static NSDateFormatter *dateFormatter;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSDateFormatter new];
        dateFormatter.timeZone = [NSTimeZone defaultTimeZone];
//        dateFormatter.locale = [NSLocale currentLocale];
        
    });
    
    return dateFormatter;
}

+ (NSCalendar *)calendar
{
    static dispatch_once_t onceToken;
    static NSCalendar *calendar;
    dispatch_once(&onceToken, ^{
        calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        calendar.timeZone = [NSTimeZone localTimeZone];
        calendar.locale = [NSLocale currentLocale];
    });
    
    return calendar;
}



#pragma mark -
#pragma mark - Helper

+ (NSDate *)dateByAddingValue:(NSInteger)value toDate:(NSDate *)date
{
    NSDate *newDate = [[NSDate calendar] dateByAddingUnit:NSCalendarUnitDay
                                                    value:value
                                                   toDate:date
                                                  options:0];
    
    return newDate;
}

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format
{
    [[NSDate dateFormatter] setDateFormat:format];
    
    NSDate *date = [[NSDate dateFormatter] dateFromString:string];
    return date;
}

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format
{
    [[NSDate dateFormatter] setDateFormat:format];
    
    NSString *string = [[NSDate dateFormatter] stringFromDate:date];
    return string;
}

+ (NSString *)stringFromTimeStamp:(double)timeStamp withFormat:(NSString *)format
{
    NSDate *gmtDate = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    NSTimeInterval timeZoneSeconds = [[NSTimeZone systemTimeZone] secondsFromGMT];
    NSDate *localDate = [gmtDate dateByAddingTimeInterval:timeZoneSeconds];
    
    NSString *strDate = [NSDate stringFromDate:localDate withFormat:format];
    return strDate.length?strDate:@"";
    
}

+ (NSString *)hmsSegmentFromTimeInterval:(NSInteger)interval
{
    NSInteger ti = interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    if (hours > 0)
        return [NSString stringWithFormat:@"%2ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    else
        return [NSString stringWithFormat:@"%2ld:%02ld", (long)minutes, (long)seconds];
}

+ (NSString *)hoursFromInterval:(NSInteger)interval
{
    NSString *sHours = [NSString stringWithFormat:@"%.1f", ((float)interval / 3600)];
    NSArray *arrComp = [sHours componentsSeparatedByString:@"."];
    
    return [NSString stringWithFormat:@"%@ %@", ([[arrComp lastObject] integerValue] == 0)?[arrComp firstObject]:sHours, ([[arrComp firstObject] integerValue] == 1)?@"hour":@"hours"];
}
/*
+ (NSString *)timeAgoFromTimestamp:(NSString *)timestamp
{
    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *dateComponents = [[NSDate calendar] components:unitFlags fromDate:[NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]] toDate:[NSDate date] options:0];
    
    NSInteger years     = [dateComponents year];
    NSInteger weeks     = [dateComponents weekOfYear];
    NSInteger days      = [dateComponents day];
    NSInteger hours     = [dateComponents hour];
    NSInteger minutes   = [dateComponents minute];
    NSInteger seconds   = [dateComponents second];
    
    NSString *duration;
    
    if (years > 0)
        duration = [NSString stringWithFormat:@"%ld %@", (long)years, (years > 1)?@"years":@"year"];
    else if (weeks > 0)
        duration = [NSString stringWithFormat:@"%ld %@", (long)weeks, (weeks > 1)?@"weeks":@"week"];
    else if (days > 0)
        duration = [NSString stringWithFormat:@"%ld %@", (long)days, (days > 1)?@"days":@"day"];
    else if (hours > 0)
        duration = [NSString stringWithFormat:@"%ld %@", (long)hours, (hours > 1)?@"hours":@"hour"];
    else if (minutes > 0)
        duration = [NSString stringWithFormat:@"%ld %@", (long)minutes, (minutes > 1)?@"mins":@"min"];
    else
    {
        if (seconds <= 0)
            return @"Just now";
        else
            duration = [NSString stringWithFormat:@"%ld sec", (long)seconds];
    }
    
    return [NSString stringWithFormat:@"%@ ago", duration];
}*/
+ (NSString *)timeAgoFromTimestamp:(NSString *)timestamp
{
    NSUInteger unitFlags =  NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute  ;
    
    NSDateComponents *dateComponents = [[NSDate calendar] components:unitFlags fromDate:[NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]] toDate:[NSDate date] options:0];
    
    NSInteger weeks     = [dateComponents weekOfYear];
    NSInteger days      = [dateComponents day];
    NSInteger hours     = [dateComponents hour];
    NSInteger minutes   = [dateComponents minute];

    
    NSString *duration;
    
    if (weeks > 4)
        return  [self stringFromTimeStamp:[timestamp doubleValue] withFormat:@"MM/dd/yyyy"];
    else if (weeks > 0)
        duration = [NSString stringWithFormat:@"%ldw", (long)weeks];
    else if (days > 0)
        duration = [NSString stringWithFormat:@"%ldd", (long)days];
    else if (hours > 0)
        duration = [NSString stringWithFormat:@"%ldh", (long)hours];
    else if (minutes > 0)
        duration = [NSString stringWithFormat:@"%ldm", (long)minutes];
    else
    {
            return @"Just now";
        
    }
    
    return [NSString stringWithFormat:@"%@ ago", duration];
}


+ (BOOL)time7daysAgoFromTimestamp:(NSString *)timestamp
{
    NSUInteger unitFlags =  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *dateComponents = [[NSDate calendar] components:unitFlags fromDate:[NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]] toDate:[NSDate date] options:0];
    
    
    NSInteger days      = [dateComponents day];
    
   
    if (days >= 7 )
        return YES;
    else
        return NO;
    
     return YES;// Temp
    
}

+ (NSString *)dayOrDateFromTimestamp:(NSString *)timestamp
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]];
    
    NSDateComponents *components = [[NSDate calendar] components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[NSDate date]];
    
    [components setHour:-[components hour]];
    [components setMinute:-[components minute]];
    [components setSecond:-[components second]];
    
    //This variable should now be pointing at a date object that is the start of today (midnight);
    NSDate *today = [[NSDate calendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
    
    NSString *duration;
    if ([[NSDate date] date:today isTheSameDayThan:date])
        duration = [NSString stringWithFormat:@"Today %@", [NSDate stringFromTimeStamp:[timestamp doubleValue] withFormat:@"hh:mm a"]];
    else
        duration =  [NSDate stringFromTimeStamp:[timestamp doubleValue] withFormat:@"dd MMM ,yyyy hh:mm a"];
    
    return  duration;
}


#pragma mark -
#pragma mark - ComprareDate

- (BOOL)date:(NSDate *)dateA isTheSameDayThan:(NSDate *)dateB
{
    NSDateComponents *componentsA = [[NSDate calendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:dateA];
    NSDateComponents *componentsB = [[NSDate calendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:dateB];
    
    return (componentsA.year == componentsB.year) && (componentsA.month == componentsB.month) && (componentsA.day == componentsB.day);
}

- (BOOL)date:(NSDate *)dateA isEqualOrBefore:(NSDate *)dateB
{
    if ([dateA compare:dateB] == NSOrderedAscending || [self date:dateA isTheSameDayThan:dateB])
        return YES;
    
    return NO;
}

- (BOOL)date:(NSDate *)dateA isEqualOrAfter:(NSDate *)dateB
{
    if ([dateA compare:dateB] == NSOrderedDescending || [self date:dateA isTheSameDayThan:dateB])
        return YES;
    
    return NO;
}

- (BOOL)date:(NSDate *)date isEqualOrAfter:(NSDate *)startDate andEqualOrBefore:(NSDate *)endDate
{
    if ([self date:date isEqualOrAfter:startDate] && [self date:date isEqualOrBefore:endDate])
        return YES;
    
    return NO;
}

@end

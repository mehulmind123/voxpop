//
//  UITableViewCell+TableViewCell.m
//  VoxPop
//
//  Created by mac-0007 on 31/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "UITableViewCell+TableViewCell.h"

@implementation UITableViewCell (TableViewCell)

- (UITableView *)tableView
{
    id view = [self superview];
    
    while (view && [view isKindOfClass:[UITableView class]] == NO)
        view = [view superview];
    
    return view;
}

- (NSIndexPath *)indexPath
{
    return [self.tableView indexPathForCell:self];
}

@end

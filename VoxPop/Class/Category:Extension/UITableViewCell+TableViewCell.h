//
//  UITableViewCell+TableViewCell.h
//  VoxPop
//
//  Created by mac-0007 on 31/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (TableViewCell)
- (UITableView *)tableView;
- (NSIndexPath *)indexPath;
@end

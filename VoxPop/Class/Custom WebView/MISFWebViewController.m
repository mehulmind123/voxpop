//
//  MIWebViewController.m
//  VoxPop
//
//  Created by mac-00014 on 19/01/18.
//  Copyright © 2017 Jignesh-0007. All rights reserved.

#import "MISFWebViewController.h"
#import <WebKit/WebKit.h>

@interface MISFWebViewController ()<WKUIDelegate,WKNavigationDelegate>
{
    WKWebView *webView;
}
@end

@implementation MISFWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initliaze];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark - General Method

-(void)initliaze
{
    //self.title = @"linkedin.com";
    [self.navigationItem setTitleView:vWTitle];
    NSURLRequest *request = [NSURLRequest requestWithURL:self.iObject];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:CFontRobotoMedium(16), NSForegroundColorAttributeName:CRGB(0, 0, 0)}];
    self.navigationController.navigationBar.tintColor = CRGB(62, 122, 255);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(barBtnRefreshClicked:)];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(barBtnDoneClicked:)];
    
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, CScreenHeight) configuration:theConfiguration];
    webView.allowsBackForwardNavigationGestures = true;
   

    webView.navigationDelegate = self;

    [webView loadRequest:request];
    [self.view addSubview:webView];
}

#pragma mark -
#pragma mark - Action Event

-(IBAction)barBtnDoneClicked:(UIBarButtonItem*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)barBtnRefreshClicked:(UIBarButtonItem*)sender
{
    [webView reload];
}

#pragma mark -
#pragma mark - WKWebViewDelegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    if (([[navigationAction.request.URL absoluteString] rangeOfString:[NSString stringWithFormat:@"%@://oauth-response?code=",CBundleId]].location != NSNotFound))
    {
        if (self.block)
            self.block(navigationAction.request.URL,nil);
        
        [self barBtnDoneClicked:nil];
    }
    else
    {
        if (!navigationAction.targetFrame.isMainFrame)
        {
            [webView loadRequest:navigationAction.request];
            
        }
    }
    

    if (navigationAction.navigationType == WKNavigationTypeLinkActivated)
        decisionHandler(WKNavigationActionPolicyCancel);
    else
        decisionHandler(WKNavigationActionPolicyAllow);
}



@end

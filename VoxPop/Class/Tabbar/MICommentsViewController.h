//
//  MICommentsViewController.h
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"
#import "HPGrowingTextView.h"

@interface MICommentsViewController : MIParentViewController <HPGrowingTextViewDelegate>
{
    IBOutlet UIScrollView *scrollMain;
    IBOutlet UITableView *tblComments;
    IBOutlet UIView *viewComment;
    IBOutlet HPGrowingTextView *txtVComment;
    IBOutlet UIButton *btnPost;
    
    IBOutlet UILabel *lblNotAllowed;
    
    IBOutlet NSLayoutConstraint *cnHeightCommentTextView;
    IBOutlet NSLayoutConstraint *cnBottomCommentView;
}
@end

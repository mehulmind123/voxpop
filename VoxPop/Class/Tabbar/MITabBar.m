//
//  MITabBar.m
//  VoxPop
//
//  Created by mac-0007 on 30/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "MITabBar.h"

@implementation MITabBar

+ (id)customTabBar
{
    MITabBar *tabBar = [[[NSBundle mainBundle] loadNibNamed:@"MITabBar" owner:nil options:nil] lastObject];
    tabBar.frame = CGRectMake(0, CScreenHeight - (65 + (CStatusBarHeight - 20)), CScreenWidth, 65);
    
    if (Is_iPhone_4 || Is_iPhone_5)
    {
        tabBar.lblTab1.font = [UIFont fontWithName:tabBar.lblTab1.font.fontName size:(tabBar.lblTab1.font.pointSize - 2)];
        tabBar.lblTab2.font = [UIFont fontWithName:tabBar.lblTab2.font.fontName size:(tabBar.lblTab2.font.pointSize - 2)];
        tabBar.lblTab3.font = [UIFont fontWithName:tabBar.lblTab3.font.fontName size:(tabBar.lblTab3.font.pointSize - 2)];
        tabBar.lblTab4.font = [UIFont fontWithName:tabBar.lblTab4.font.fontName size:(tabBar.lblTab4.font.pointSize - 2)];
    }
    
    
    [tabBar.viewDot.layer setCornerRadius:CViewWidth(tabBar.viewDot)/2];
    
    
    [tabBar.viewBack.layer setShadowColor:[UIColor grayColor].CGColor];
    [tabBar.viewBack.layer setShadowOffset:CGSizeZero];
    [tabBar.viewBack.layer setShadowOpacity:0.8f];
    [tabBar.viewBack.layer setShadowRadius:8];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:tabBar.viewSelect.bounds];
    [gradient setColors:@[(id)ColorBlue_0064ED.CGColor,
                          (id)ColorBlue_1190E7.CGColor,
                          (id)ColorBlue_0090F3.CGColor,
                          (id)ColorBlue_00AAED.CGColor]];
    [tabBar.viewSelect.layer insertSublayer:gradient atIndex:0];
    [tabBar.viewSelect.layer setCornerRadius:10.0f];
    
    return tabBar;
}

- (IBAction)btnTabBarClicked:(UIButton *)tab
{
    if (appDelegate.tabBarViewController.selectedIndex == tab.tag)
        return;
    
    [appDelegate.tabBarViewController setSelectedIndex:tab.tag];
}
@end

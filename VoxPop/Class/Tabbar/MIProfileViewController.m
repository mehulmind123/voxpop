//
//  MIProfileViewController.m
//  VoxPop
//
//  Created by mac-0007 on 22/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIProfileViewController.h"
#import "MIEditProfileViewController.h"
#import "MIAccessCodeViewController.h"
#import "MIBlockedListViewController.h"
#import "MISelectTopicViewController.h"
#import "MIChangePasswordViewController.h"
#import "MIWebViewController.h"
#import "MILinkedAccountsViewController.h"

#import "MIProfileHeaderTableViewCell.h"
#import "MITableViewCell.h"

@interface MIProfileViewController ()
{
    NSArray *arrProfile;
    
    NSDictionary *dictCMS;
    
    BOOL needToRefreshOnCurrentViewAppear;
    
    UIButton *btnEdit;
}
@property (nonatomic, strong) MIProfileHeaderTableViewCell *prototypeCell1;
@property (nonatomic, strong) MITableViewCell *prototypeCell2;
@end

@implementation MIProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (needToRefreshOnCurrentViewAppear) [self loadDataFromServer];
    else needToRefreshOnCurrentViewAppear = YES;
}
- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    btnEdit.layer.cornerRadius = 15;
    btnEdit.layer.borderWidth = 1;
    btnEdit.layer.borderColor = [UIColor whiteColor].CGColor;
    btnEdit.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"PROFILE";
    
    needToRefreshOnCurrentViewAppear = YES;
    
    [self refreshUserInfo];
    dictCMS =  [CUserDefaults valueForKey:UserDefaultCMS];
    
    //....UITableView
    [tblProfile registerNib:[UINib nibWithNibName:@"MIProfileHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIProfileHeaderTableViewCell"];
    [tblProfile registerNib:[UINib nibWithNibName:@"MITableViewCell" bundle:nil] forCellReuseIdentifier:@"MITableViewCell"];
    [tblProfile reloadData];
    
    [cnHeightTblProfile setConstant:tblProfile.contentSize.height - 1];
    [tblProfile.layer setCornerRadius:10];
    
    [btnContactUs.layer setCornerRadius:5.0];
    
    //....Button Edit.
    [btnEdit setHidden:btnPersonal.isSelected ? false : true];
//    if (btnPersonal.isSelected)
//    {
//        btnEdit = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"edit"] style:UIBarButtonItemStylePlain target:self action:@selector(btnEditClicked:)];
//        [self.navigationItem setRightBarButtonItem:btnEdit];
//    }
}

- (void)refreshUserInfo
{
    arrProfile = @[@{@"key":@"Username", @"value":appDelegate.loginUser.user_name},
                   @{@"key":@"Email", @"value":appDelegate.loginUser.email},
                   @{@"key":@"Change Password", @"value":@""},
                   @{@"key":@"Date of birth", @"value":appDelegate.loginUser.dob.isBlankValidationPassed?appDelegate.loginUser.dob:@"N/A"},
                   @{@"key":@"Marriage status", @"value":appDelegate.loginUser.marital_status.name.isBlankValidationPassed?  appDelegate.loginUser.marital_status.name : @"N/A"},
                   @{@"key":@"gender", @"value":appDelegate.loginUser.gender.isBlankValidationPassed?  appDelegate.loginUser.gender :@"N/A"},
                   @{@"key":@"zip code", @"value":appDelegate.loginUser.zip_code.isBlankValidationPassed?  appDelegate.loginUser.zip_code :@"N/A"}];
    
//    arrProfile = @[@{@"key":@"Username", @"value":appDelegate.loginUser.user_name},
//                   @{@"key":@"Email", @"value":appDelegate.loginUser.email},
//                   @{@"key":@"Change Password", @"value":@""},
//                   @{@"key":@"Date of birth", @"value":appDelegate.loginUser.dob.isBlankValidationPassed?appDelegate.loginUser.dob:@"N/A"},
//                   @{@"key":@"Marriage status", @"value":appDelegate.loginUser.marital_status.name.isBlankValidationPassed?  appDelegate.loginUser.marital_status.name : @"N/A"}];
}


#pragma mark -
#pragma mark - Load Data

- (void)loadDataFromServer
{
    [[APIRequest request] userProfile_completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            if(self->btnPersonal.isSelected)
            {
                [self refreshUserInfo];
                [self->tblProfile reloadData];
            }
        }
    }];
}


#pragma mark -
#pragma mark - Action Event

- (void)btnEditClicked:(UIButton *)sender
{
    needToRefreshOnCurrentViewAppear = NO;
    MIEditProfileViewController *editProfileVC = [MIEditProfileViewController initWithXib];
    [editProfileVC setBlock:^(NSNumber *isUpdate, NSError *error)
     {
         if (isUpdate.boolValue)
         {
             [self refreshUserInfo];
             [self->tblProfile reloadData];
         }
     }];
    [self.navigationController pushViewController:editProfileVC animated:YES];
}

- (void)notificationSwitchChanged:(UISwitch *)sender
{
    [[APIRequest request] changeAllowedNotification:[NSNumber numberWithBool:sender.on] completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            appDelegate.loginUser.iAllowedNotification = sender.on;
            [[[Store sharedInstance]mainManagedObjectContext] save];
        }
        else
            [sender setOn:!sender.on];
    }];
}

- (IBAction)btnHeaderVClicked:(UIButton *)sender
{
    if (sender.isSelected)
        return;
    
    btnPersonal.selected = btnSitePreferances.selected = btnAbout.selected = NO;
    sender.selected = YES;
    
    [btnEdit setHidden:sender.tag == 1 ? false : true];
    //[self.navigationItem setRightBarButtonItem:(sender.tag == 1 ? btnEdit : nil)];
    
    [btnPersonal setTitleColor:CRGB(148, 198, 248) forState:UIControlStateNormal];
    [btnSitePreferances setTitleColor:CRGB(148, 198, 248) forState:UIControlStateNormal];
    [btnAbout setTitleColor:CRGB(148, 198, 248) forState:UIControlStateNormal];
    [sender setTitleColor:ColorWhite_FFFFFF forState:UIControlStateNormal];
    
    switch (sender.tag) {
        case 1:
        {
            [self refreshUserInfo];
            break;
        }
        case 2:
        {
            arrProfile = @[@"topics of interest",
                           @"access codes",
                           @"linked social accounts",
                           @"blocked customers",
                           @"push notification"];
            break;
        }
        case 3:
        {
            arrProfile = @[@"privacy policy",
                           @"terms",
                           @"about us",
                           @"logout"];
            
            break;
        }
    }
    
    [tblProfile reloadData];
    [cnHeightTblProfile setConstant:tblProfile.contentSize.height - 1];
}

- (IBAction)btnContactUsClicked:(UIButton *)sender
{
    //...Contact Us
    [appDelegate openMailComposerWithEmailId:@"support@voxpopapp.com" subject:@"Contact us" content:@"" viewController:self];
}


#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrProfile.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnPersonal.isSelected)
    {
        [self configureHeaderCell:self.prototypeCell1 forRowAtIndexPath:indexPath];
        
        [self.prototypeCell1 updateConstraintsIfNeeded];
        [self.prototypeCell1 layoutIfNeeded];
        
        return [self.prototypeCell1.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    }
    else
    {
        [self configureFooterCell:self.prototypeCell2 forRowAtIndexPath:indexPath];
        
        [self.prototypeCell2 updateConstraintsIfNeeded];
        [self.prototypeCell2 layoutIfNeeded];
        
        return [self.prototypeCell2.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnPersonal.isSelected)
    {
        NSString *identifier = @"MIProfileHeaderTableViewCell";
        MIProfileHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        [self configureHeaderCell:cell forRowAtIndexPath:indexPath];
        
        return cell;
    }
    else
    {
        NSString *identifier = @"MITableViewCell";
        MITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        [self configureFooterCell:cell forRowAtIndexPath:indexPath];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnPersonal.isSelected)
    {
        switch (indexPath.row)
        {
            case 2:
            {
                //...Change Password
                MIChangePasswordViewController *changePwdVC = [MIChangePasswordViewController initWithXib];
                [self.navigationController pushViewController:changePwdVC animated:YES];
                break;
            }
        }
    }
    else if (btnSitePreferances.isSelected)
    {
        switch (indexPath.row)
        {
            case 0:
            {
                //...Edit Interest Topics
                MISelectTopicViewController *editTopicVC = [MISelectTopicViewController initWithXib];
                
                [self.navigationController pushViewController:editTopicVC animated:YES];
                break;
            }
            case 1:
            {
                //...Manage Access Codes
                MIAccessCodeViewController *accessCodeVC = [MIAccessCodeViewController initWithXib];
                [self.navigationController pushViewController:accessCodeVC animated:YES];
                break;
            }
            case 2:
            {
                //...linked accounts
                MILinkedAccountsViewController *accountVC = [MILinkedAccountsViewController initWithXib];
                [self.navigationController pushViewController:accountVC animated:YES];
                break;
            }
            case 3:
            {
                //...Blocked Customers
                MIBlockedListViewController *blockedVC = [MIBlockedListViewController initWithXib];
                [self.navigationController pushViewController:blockedVC animated:YES];
                break;
            }
            case 4:
            {
                needToRefreshOnCurrentViewAppear = YES;
                //...Push Notification
                break;
            }
                
        }
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
            {
                //...Privacy Policy
                MIWebViewController *privacyVC = [MIWebViewController initWithXib];
                [privacyVC setDataType:DataTypePrivacyPolicy];
                [privacyVC setIObject: dictCMS ? [dictCMS valueForKey:@"privacy-policy"] : nil];
                [self.navigationController pushViewController:privacyVC animated:YES];
                break;
            }
            case 1:
            {
                //...Terms & Condition
                MIWebViewController *termsVC = [MIWebViewController initWithXib];
                [termsVC setDataType:DataTypeTermsCondition];
                [termsVC setIObject:dictCMS ? [dictCMS valueForKey:@"terms-and-conditions"] : nil];
                [self.navigationController pushViewController:termsVC animated:YES];
                break;
            }
            case 2:
            {
                //...About Us
                MIWebViewController *aboutVC = [MIWebViewController initWithXib];
                [aboutVC setDataType:DataTypeAboutUs];
                [aboutVC setIObject: dictCMS ? [dictCMS valueForKey:@"about-us"] : nil];
                [self.navigationController pushViewController:aboutVC animated:YES];
                break;
            }
            case 3:
            {
                needToRefreshOnCurrentViewAppear = YES;
                //...Logout
                dispatch_async(GCDMainThread, ^{
                    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageLogout firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
                     {
                         [appDelegate logoutUser];
                     } secondButton:@"No" secondHandler:nil inView:self];
                });
                
                break;
            }
        }
    }
}

- (MIProfileHeaderTableViewCell *)prototypeCell1
{
    static NSString *identifier = @"MIProfileHeaderTableViewCell";
    
    if (!_prototypeCell1)
        _prototypeCell1 = [tblProfile dequeueReusableCellWithIdentifier:identifier];
    
    return _prototypeCell1;
}

- (MITableViewCell *)prototypeCell2
{
    static NSString *identifier = @"MITableViewCell";
    
    if (!_prototypeCell2)
        _prototypeCell2 = [tblProfile dequeueReusableCellWithIdentifier:identifier];
    
    return _prototypeCell2;
}

- (void)configureHeaderCell:(MIProfileHeaderTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictionary = arrProfile[indexPath.row];
    [cell.lblKey setText:dictionary[@"key"]];
    [cell.lblValue setText:dictionary[@"value"]];
    [cell.imgVArrow setHidden:(indexPath.row != 2)];
}

- (void)configureFooterCell:(MITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell.lblTitle setText:arrProfile[indexPath.row]];
    [cell.lblSubtitle setText:@""];
    [cell.btnLockUnlock setHidden:YES];
    [cell.btnNextRemove setHidden:NO];
    [cell.btnNextRemove setSelected:YES];
    [cell.switchNotification setHidden:YES];
    [cell.btnNextRemove setUserInteractionEnabled:NO];
    [cell.lblTitle setConstraintConstant:10 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    [cell.lblSubtitle setConstraintConstant:10 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
    
    if (btnSitePreferances.isSelected)
    {
        if (indexPath.row == 4)
        {
            [cell.btnNextRemove setHidden:YES];
            [cell.switchNotification setHidden:NO];
            [cell.switchNotification setOn:appDelegate.loginUser.iAllowedNotification];
            [cell.switchNotification addTarget:self action:@selector(notificationSwitchChanged:) forControlEvents:UIControlEventValueChanged];
            [cell.lblSubtitle setText:@"(If turned off, you will not receive alert when survey goes live)"];
            [cell.lblTitle setConstraintConstant:4 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
            [cell.lblSubtitle setConstraintConstant:4 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
        }
    }
    else if (btnAbout.isSelected)
    {
        if (indexPath.row == arrProfile.count - 1)
        {
            [cell.btnNextRemove setHidden:YES];
            [cell.switchNotification setHidden:YES];
            [cell.lblSubtitle setText:@"If you logout, you will not receive push notifications for any survey."];
            [cell.lblTitle setConstraintConstant:4 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
            [cell.lblSubtitle setConstraintConstant:4 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
        }
    }
}

@end

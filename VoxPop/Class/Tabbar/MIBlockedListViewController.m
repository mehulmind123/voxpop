//
//  MIBlockedListViewController.m
//  VoxPop
//
//  Created by mac-0007 on 26/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIBlockedListViewController.h"
#import "MITableViewCell.h"

@interface MIBlockedListViewController ()
{
    NSMutableArray *arrBlockedList;
}
@property (nonatomic, strong) MITableViewCell *prototypeCell;
@end

@implementation MIBlockedListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"BLOCKED CUSTOMERS";
   
   
   arrBlockedList = [NSMutableArray new];
    //....UITableView
    [tblBlockedList registerNib:[UINib nibWithNibName:@"MITableViewCell" bundle:nil] forCellReuseIdentifier:@"MITableViewCell"];
    
   [self reloadBlockCustomerList];
   
   [self loadBlockCustomerFromServer];

}

#pragma mark -
#pragma mark - Load Data

- (void)reloadBlockCustomerList
{
   [tblBlockedList reloadData];
   [cnHeightTblBlock setConstant:tblBlockedList.contentSize.height - 1];
   [tblBlockedList.layer setCornerRadius:10];
}

- (void)loadBlockCustomerFromServer
{
   [self startLoadingAnimationInView:viewBackground];
   
   [[APIRequest request] loadBlockCustomer_completed:^(id responseObject, NSError *error)
    {
       
       if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
       {
          [arrBlockedList addObjectsFromArray:[responseObject valueForKey:CJsonData]];
          [self reloadBlockCustomerList];
          
       }
       
       [self stopLoadingAnimationInView:viewBackground type:error ? StopAnimationTypeErrorTapToRetry : !arrBlockedList.count ? StopAnimationTypeDataNotFound : StopAnimationTypeRemove  touchUpInsideClickedEvent:^{
          
          [self loadBlockCustomerFromServer];
       }];
       
    }];
   
}

#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrBlockedList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    
    [self.prototypeCell updateConstraintsIfNeeded];
    [self.prototypeCell layoutIfNeeded];
    
    return [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MITableViewCell";
    MITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (MITableViewCell *)prototypeCell
{
    static NSString *identifier = @"MITableViewCell";
    
    if (!_prototypeCell)
        _prototypeCell = [tblBlockedList dequeueReusableCellWithIdentifier:identifier];
    
    return _prototypeCell;
}

- (void)configureCell:(MITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = arrBlockedList[indexPath.row];
    [cell.lblTitle setText:dict[@"name"]];
    [cell.btnNextRemove setHidden:YES];
    [cell.btnLockUnlock setSelected:![dict booleanForKey:@"iblock"]];
    
    [cell.btnLockUnlock touchUpInsideClicked:^{
        if (cell.btnLockUnlock.selected)
        {
            [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageBlockCustomerMessage firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
             {
                [[APIRequest request] blockUnblockUser:[dict numberForJson:@"id"] status:YES completed:^(id responseObject, NSError *error)
                {
                   if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                   {
                      
                      NSMutableDictionary *updateDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
                      [updateDict setObject:@1 forKey:@"iblock"];
                      [arrBlockedList replaceObjectAtIndex:indexPath.row withObject:updateDict];
                      [tblBlockedList reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                   }

                }];
                
                
             } secondButton:@"No" secondHandler:nil inView:self];
        }
        else
        {
            [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageUnblockCustomer firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
             {
                [[APIRequest request] blockUnblockUser:[dict numberForJson:@"id"] status:NO completed:^(id responseObject, NSError *error)
                 {
                    if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                    {
                       NSMutableDictionary *updateDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
                       [updateDict setObject:@0 forKey:@"iblock"];
                       [arrBlockedList replaceObjectAtIndex:indexPath.row withObject:updateDict];
                       
                       [tblBlockedList reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                    
                 }];

                
             } secondButton:@"No" secondHandler:nil inView:self];
        }
    }];
}

@end

//
//  MISurveyResultViewController.m
//  VoxPop
//
//  Created by mac-0007 on 28/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MISurveyResultViewController.h"
#import "MICommentsViewController.h"

#import "MIResultQACollectionViewCell.h"

@interface MISurveyResultViewController ()
{
    NSArray *arrQA;
    NSDictionary *dictSurveyResults;
}
@end

@implementation MISurveyResultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"RESULTS";

    
    //....NavigationBarItem
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"navBackWhite"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonItemClicked:)]];
    
    [btnComments.layer setShadowColor:ColorBlue_3490C2.CGColor];
    [btnComments.layer setShadowOffset:CGSizeMake(2, 2)];
    [btnComments.layer setShadowOpacity:1.0];
    [btnComments.layer setShadowRadius:0];
    
    //....UICollectionView
    [collVQA registerNib:[UINib nibWithNibName:@"MIResultQACollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIResultQACollectionViewCell"];
    
    [self loadDataFromServer];
}



#pragma mark -
#pragma mark - Load Data

- (void)loadDataFromServer
{
    [self startLoadingAnimationInView:collVQA];
    
    [[APIRequest request] getSurveyResults:self.iObject completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            self->dictSurveyResults = [responseObject valueForKey:CJsonData];
            self->arrQA = [self->dictSurveyResults valueForKey:@"survey_QA"];
            [self->pageControl setNumberOfPages:self->arrQA.count];
            [self->collVQA reloadData];
        }
        
        if (!self->dictSurveyResults.count)
        {
            //... (Data not available Or error) AND (Stop loader animation)
            [self stopLoadingAnimationInView:self->collVQA type:error ?StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound touchUpInsideClickedEvent:^
            {
                [self loadDataFromServer];
            }];
        }
        else //... Stop loader animation
            [self stopLoadingAnimationInView:self->collVQA type:StopAnimationTypeRemove touchUpInsideClickedEvent:nil];
    }];
}



#pragma mark -
#pragma mark - Action Event

- (void)backBarButtonItemClicked:(UIBarButtonItem *)barButtonItem
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)btnCommentClicked:(UIButton *)sender
{
    MICommentsViewController *commentsVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
    [commentsVC setIObject:@1];
    [self.navigationController pushViewController:commentsVC animated:YES];
}




#pragma mark
#pragma mark - UICollectionView Delegate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrQA.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *indentifier = @"MIResultQACollectionViewCell";
    MIResultQACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifier forIndexPath:indexPath];
    [cell configureCellWithInfo:arrQA[indexPath.row] indexPath:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CScreenWidth, CViewHeight(collectionView));
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    pageControl.currentPage = round(scrollView.contentOffset.x/CViewWidth(scrollView));
}
@end

//
//  MIProfileViewController.h
//  VoxPop
//
//  Created by mac-0007 on 22/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIProfileViewController : MIParentViewController
{
//    IBOutlet UIButton *btnEdit;
    
    IBOutlet UITableView *tblProfile;
    IBOutlet NSLayoutConstraint *cnHeightTblProfile;
    
    IBOutlet UIButton *btnPersonal;
    IBOutlet UIButton *btnSitePreferances;
    IBOutlet UIButton *btnAbout;
    IBOutlet MIGenericButton *btnContactUs;
    IBOutlet MIGenericView *headerV;
}
@end

//
//  MIWebViewController.m
//  VoxPop
//
//  Created by mac-0007 on 26/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIWebViewController.h"

@interface MIWebViewController ()
{
    BOOL isAboutBlank;
}
@end

@implementation MIWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    
    UIImage *image = [[UIImage imageNamed:@"navBackBlack"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked)]];

    switch (self.dataType)
    {
        case DataTypeAboutUs:
        {
            self.title = @"ABOUT US";
            break;
        }
        case DataTypeTermsCondition:
        {
            self.title = @"TERMS & CONDITIONS";
            break;
        }
        case DataTypePrivacyPolicy:
        {
            self.title = @"PRIVACY POLICY   ";
            break;
        }
    }
    

    isAboutBlank = YES;
    [webVw loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    [self loadCMSDataFromServerWithType:self.iObject ? self.iObject[@"cms_desc"] : @""];
    [self loadDataFromServer];
}

- (void)loadCMSDataFromServerWithType:(NSString *)type
{
    if(type.isBlankValidationPassed)
    {
        
        [webVw loadHTMLString:type baseURL:nil];
    }
    else
        [webVw loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://generator.lorem-ipsum.info/terms-and-conditions"]]];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
   
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([webView.request.URL.absoluteString isEqualToString:@"about:blank"] && ![webView canGoBack] && !isAboutBlank)
    {
        isAboutBlank = YES;
        [self loadCMSDataFromServerWithType:self.iObject ? self.iObject[@"cms_desc"] : @""];
    }
}

#pragma mark -
#pragma mark - Load Data

- (void)loadDataFromServer
{
    [[APIRequest request] loadCMS_completed:^(id responseObject, NSError *error)
    {
        if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
           NSDictionary *dictCMS =  [CUserDefaults valueForKey:UserDefaultCMS];

            
            switch (self.dataType)
            {
                case DataTypeAboutUs:
                {
                    [self setIObject: dictCMS ? [dictCMS valueForKey:@"about-us"] : nil];
                    break;
                }
                case DataTypeTermsCondition:
                {
                    [self setIObject:dictCMS ? [dictCMS valueForKey:@"terms-and-conditions"] : nil];
                    break;
                }
                case DataTypePrivacyPolicy:
                {
                    
                    [self setIObject: dictCMS ? [dictCMS valueForKey:@"privacy-policy"] : nil];
                    break;
                }
            }
            [self loadCMSDataFromServerWithType:self.iObject ? self.iObject[@"cms_desc"] : @""];
        }
        
    }];
}

#pragma mark -
#pragma mark - Action event

- (void)backButtonClicked
{
    if([webVw canGoBack])
    {
        isAboutBlank = NO;
        //Go back in webview history
        [webVw goBack];
    } else
    {
        //Pop view controller to preview view controller
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end

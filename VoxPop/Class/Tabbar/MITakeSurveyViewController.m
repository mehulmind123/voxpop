//
//  MITakeSurveyViewController.m
//  VoxPop
//
//  Created by mac-0007 on 23/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MITakeSurveyViewController.h"
#import "MICommentsViewController.h"
#import "MIStartSurveyViewController.h"
#import "MISurveyResultViewController.h"

#import "MITopicCollectionViewCell.h"

@interface MITakeSurveyViewController () <MIBubbleViewLayoutDelegate>
{
    NSMutableArray *arrTopics;
}
@end

@implementation MITakeSurveyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"TAKE SURVEY";
    
    arrTopics = @[@"EDUCATION",
                  @"HIGH SCHOOL",
                  @"STUDENT",
                  @"TEACHER",
                  @"PROFESSOR",
                  @"STUDENT",
                  @"TEACHER"].mutableCopy;
    
    [viewYoutube hideByHeight:YES];
    
    //...Shadow/CornerRadius
    [btnBlockCustomer.layer setCornerRadius:CViewHeight(btnBlockCustomer)/2];
    [viewYoutube.layer setCornerRadius:10];
    
    [btnComments.layer setShadowColor:ColorBlue_3B8DF2.CGColor];
    [btnComments.layer setShadowOffset:CGSizeMake(2, 2)];
    [btnComments.layer setShadowOpacity:1.0];
    [btnComments.layer setShadowRadius:0];
    
    
    //....Attributed String
    [lblExpiration setText:@"Expires on: 27/08/2017"];
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?11:(Is_iPhone_6_PLUS)?15:13;
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:lblExpiration.text];
    [attString addAttribute:NSFontAttributeName value:CFontRobotoLight(fontSize) range:NSMakeRange(0, lblExpiration.text.length)];
    [attString addAttribute:NSFontAttributeName value:CFontRobotoMedium(fontSize) range:NSMakeRange(0, [lblExpiration.text rangeOfString:@":"].location + 1)];
    [lblExpiration setAttributedText:attString];
    
    
    //...UICollectionView
    MIBubbleViewLayout *layout = [[MIBubbleViewLayout alloc] initWithDelegate:self];
    [layout setMinimumLineSpacing:6.0f];
    [layout setMinimumInteritemSpacing:6.0f];
    
    [collVTopics setCollectionViewLayout:layout];
    [collVTopics setAllowsMultipleSelection:YES];
    [collVTopics registerNib:[UINib nibWithNibName:@"MITopicCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MITopicCollectionViewCell"];
    
    
    
}


#pragma mark
#pragma mark - Action Event

- (IBAction)btnBlockCustomerClicked:(UIButton *)sender
{
    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:CMessageBlockCustomerTitle message:CMessageBlockCustomerMessage firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
     {
         [self.navigationController popViewControllerAnimated:YES];
     } secondButton:@"No" secondHandler:^(UIAlertAction *action)
     {
         
     } inView:self];
}

- (IBAction)btnCommentsClicked:(id)sender
{
    MICommentsViewController *commentsVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
    [commentsVC setIObject:@0];
    [self.navigationController pushViewController:commentsVC animated:YES];
}

- (IBAction)btnYoutubeClicked:(id)sender
{
    
}

- (IBAction)btnStartNowClicked:(id)sender
{
    MIStartSurveyViewController *startSurveyVC = [[MIStartSurveyViewController alloc] initWithNibName:@"MIStartSurveyViewController" bundle:nil];
    
    [startSurveyVC setBlock:^(id object, NSError *error) {
        MISurveyResultViewController *resultVC = [[MISurveyResultViewController alloc] initWithNibName:@"MISurveyResultViewController" bundle:nil];
        [self.navigationController pushViewController:resultVC animated:YES];
    }];
    
    [self presentViewController:[UINavigationController navigationControllerWithRootViewController:startSurveyVC] animated:YES completion:nil];
}




#pragma mark
#pragma mark - UICollectionView Delegate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrTopics.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *indentifier = @"MITopicCollectionViewCell";
    MITopicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifier forIndexPath:indexPath];
    [cell.contentView setBackgroundColor:(indexPath.row == 0)?ColorBlue_2F7AC1:ColorBlueSky_4DC0FA];
    [cell.lblTopic setText:[arrTopics[indexPath.row] uppercaseString]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView itemSizeAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = arrTopics[indexPath.row];
    CGSize size = [title sizeWithAttributes:@{NSFontAttributeName:CFontSolidoMedium(12)}];
    size.height = 23;
    size.width += CTextPadding*2;
    
    if (size.width > CViewWidth(collectionView))
        size.width = CViewWidth(collectionView);
    return size;
}

@end

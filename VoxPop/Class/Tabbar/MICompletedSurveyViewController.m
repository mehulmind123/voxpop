//
//  MICompletedSurveyViewController.m
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MICompletedSurveyViewController.h"
#import "MIPreviewAnswerTableViewCell.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>

@interface MICompletedSurveyViewController ()
{
    NSArray *arrSurveyQA;
}
@property (nonatomic, strong) MIPreviewAnswerTableViewCell *prototypeCell;
@end

@implementation MICompletedSurveyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"SURVEY COMPLETED";
    self.navigationItem.hidesBackButton = YES;
    
    
    //....NavigationBarItem
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"quitSurvey"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnBarItemQuitClicked:)];
    
    
    //....UITableView
    [tblPreview registerNib:[UINib nibWithNibName:@"MIPreviewAnswerTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIPreviewAnswerTableViewCell"];
    
    [self loadSurveyQADetail];
}


#pragma mark -
#pragma mark - Load Data

- (void)loadSurveyQADetail
{
    [lblSurveyTitle setText:[NSString stringWithFormat:@"You have answered all questions of the survey \"%@.\"", self.surveyInfo.title]];
    
    arrSurveyQA = self.surveyInfo.surveyQA;
    [tblPreview reloadData];
    [cnHeightTblPreview setConstant:tblPreview.contentSize.height];
}



#pragma mark -
#pragma mark - Action Event

- (void)btnBarItemQuitClicked:(UIBarButtonItem *)barButtonItem
{
    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:CMessageQuitSurveyTitle message:CMessageQuitSurveyMessage firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
     {
         [self.navigationController dismissViewControllerAnimated:YES completion:nil];
     } secondButton:@"No" secondHandler:nil inView:self];
}

- (IBAction)btnEditAnswerClicked:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSubmitClicked:(UIButton *)sender
{
    __block BOOL isAllQuestionAnswered = YES;
    __block NSMutableArray *arrSelectedQA = [NSMutableArray new];
    
    [arrSurveyQA enumerateObjectsUsingBlock:^(SurveyQA *surveyQA, NSUInteger idx, BOOL *stop)
    {
        NSArray *arrSelectedAnswers = [surveyQA.answers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isSelected == 1"]];
        
        if (arrSelectedAnswers.count == 0)
        {
            isAllQuestionAnswered = NO;
            *stop = YES;
        }
        else
        {
            NSArray *selectedIds = [arrSelectedAnswers valueForKeyPath:@"answersIdentifier"];
            [arrSelectedQA addObject:@{@"id":[NSNumber numberWithDouble:surveyQA.surveyQAIdentifier],
                                       @"answer_ids":[selectedIds componentsJoinedByString:@", "]}];
        }
    }];
    
    if (isAllQuestionAnswered)
    {
        [[APIRequest request] submitSurveyWithSurveyId:[NSNumber numberWithDouble:self.surveyInfo.surveyIdentifier] surveyQA:arrSelectedQA completed:^(id responseObject, NSError *error)
        {
            if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                if ([[responseObject valueForKey:CJsonData] booleanForKey:@"needToAddAsNewInterest"])
                {
                    
                    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:CMessageSurveyAcknowledge2Title message:CMessageSurveyAcknowledge2Message firstButton:@"Add now" firstHandler:^(UIAlertAction *action)
                     {
                        [[APIRequest request] addTopicFromSurveyWithSurveyId:[NSNumber numberWithDouble:self.surveyInfo.surveyIdentifier] completed:^(id responseObject, NSError *error)
                        {
                            [self.navigationController dismissViewControllerAnimated:YES completion:^{
                                if (self.block)
                                    self.block(@1, nil);
                            }];
                        }];
                         
                    } secondButton:@"View results for now" secondHandler:^(UIAlertAction *action)
                    {
                        [self.navigationController dismissViewControllerAnimated:YES completion:^{
                            if (self.block)
                                self.block(@1, nil);
                        }];
                    } inView:self];
                }
                else
                {
                    [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:CMessageSurveyAcknowledge1Title message:CMessageSurveyAcknowledge1Message buttonTitle:@"Ok" handler:^(UIAlertAction *action)
                     {
                        // dissmis survey premise
//                        MITabBarViewController *tb = [UIApplication sharedApplication].keyWindow.rootViewController;
//
//                        UINavigationController *vc = [tb selectedViewController];
//                        [vc popToRootViewControllerAnimated:true];
                        
                         [self.navigationController dismissViewControllerAnimated:YES completion:^{
                             if (self.block)
                                 self.block(@1, nil);
                         }];
                        
                     } inView:self];
                }
            }
            


        }];
    }
    else
    {
        [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:CMessageSurveyMissedAnsTitle message:CMessageSurveyMissedAnsMessage buttonTitle:@"Ok" handler:^(UIAlertAction *action)
         {
             
         } inView:self];
    }
}



#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSurveyQA.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    
    [self.prototypeCell updateConstraintsIfNeeded];
    [self.prototypeCell layoutIfNeeded];
    
    return [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MIPreviewAnswerTableViewCell";
    MIPreviewAnswerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SurveyQA *surveyQA = arrSurveyQA[indexPath.row];
    if ((int)surveyQA.questionType != 1)
    {
        NSString *youtubeIdentifier = [[surveyQA.question componentsSeparatedByString:@"v="] lastObject];
        
        XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:youtubeIdentifier];
        videoPlayerViewController.preferredVideoQualities = @[@(XCDYouTubeVideoQualitySmall240), @(XCDYouTubeVideoQualityMedium360)];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewController.moviePlayer];
        [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
    }
}

- (MIPreviewAnswerTableViewCell *)prototypeCell
{
    static NSString *identifier = @"MIPreviewAnswerTableViewCell";
    
    if (!_prototypeCell)
        _prototypeCell = [tblPreview dequeueReusableCellWithIdentifier:identifier];
    
    return _prototypeCell;
}

- (void)configureCell:(MIPreviewAnswerTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    SurveyQA *surveyQA = arrSurveyQA[indexPath.row];
    NSArray *arrSelectedAnswers = [surveyQA.answers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isSelected == 1"]];
    NSArray *arrAnswersText = [arrSelectedAnswers valueForKeyPath:@"answerText"];
    
    if ((int)surveyQA.questionType == 1)
        [cell.lblQuestion setText:surveyQA.question];
    
    else
       [cell.lblQuestion setText:@"Click here to watch video for the question."];
    
    
    [cell.lblAnswer setText:[arrAnswersText componentsJoinedByString:@", "]];
}



#pragma mark -
#pragma mark - XCDYouTubeVideoPlayer Observer

- (void) moviePlayerPlaybackDidFinish:(NSNotification *)notification
{
    MPMovieFinishReason finishReason = [notification.userInfo[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] integerValue];
    NSString *reason = @"Unknown";
    
    switch (finishReason)
    {
        case MPMovieFinishReasonPlaybackEnded:
            reason = @"Playback Ended";
            break;
        case MPMovieFinishReasonPlaybackError:
            reason = @"Playback Error";
            break;
        case MPMovieFinishReasonUserExited:
            reason = @"User Exited";
            break;
    }
    
    NSLog(@"Finish Reason: %@", reason);
}

@end

//
//  MIHomeViewController.h
//  VoxPop
//
//  Created by mac-0007 on 22/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIHomeViewController : MIParentViewController
{
    IBOutlet UITableView *tblSurvey;
}
@end

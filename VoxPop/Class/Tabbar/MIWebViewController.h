//
//  MIWebViewController.h
//  VoxPop
//
//  Created by mac-0007 on 26/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

typedef enum
{
    DataTypeAboutUs,
    DataTypeTermsCondition,
    DataTypePrivacyPolicy
} DataType;

@interface MIWebViewController : MIParentViewController
{
    IBOutlet UIWebView *webVw;
}
@property (assign, nonatomic) DataType dataType;
@end

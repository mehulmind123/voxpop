//
//  MIStartSurveyViewController.h
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIStartSurveyViewController : MIParentViewController
{
    IBOutlet UIView *viewQAContainer;
    IBOutlet UILabel *lblQuestion;
    IBOutlet UIButton *btnQuestion;
    IBOutlet UILabel *lblAnswerType;
    IBOutlet UITableView *tblAnswers;
    
    IBOutlet UIButton *btnPrevious;
    IBOutlet UIButton *btnNext;
    IBOutlet UILabel *lblNextTitle;
    
    IBOutlet NSLayoutConstraint *cnHeightTblAnswers;
}

@property (nonatomic, strong) MISurvey *surveyInfo;
@end

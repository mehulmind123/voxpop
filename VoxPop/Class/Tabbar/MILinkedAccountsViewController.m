//
//  MILinkedAccountsViewController.m
//  VoxPop
//
//  Created by Mac-00014 on 05/01/18.
//  Copyright © 2018 Jignesh-0007. All rights reserved.
//

#import "MILinkedAccountsViewController.h"
#import "MISocialAccountTableViewCell.h"
@interface MILinkedAccountsViewController ()
{
    NSMutableArray *arrAccount;
}
@end

@implementation MILinkedAccountsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"linked accounts";
    
    arrAccount = [[NSMutableArray alloc] initWithObjects:
                  @{@"title":@"Facebook",
                    @"icon_normal":@"account_fb",
                    @"icon_selected":@"account_fb_selected",
                    @"type":@"Facebook"},
                  @{@"title":@"LinkedIn", @"icon_normal":@"account_Linkedin",
                    @"icon_selected":@"account_Linkedin_selected",
                    @"type":@"LinkedIn"},
                  @{@"title":@"Google",
                    @"icon_normal":@"account_google",
                    @"icon_selected":@"account_google_selected",
                    @"type":@"Google"}, nil];
    
    
    //....UITableView
    [tblAccounts registerNib:[UINib nibWithNibName:@"MISocialAccountTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISocialAccountTableViewCell"];

    
    [tblAccounts reloadData];
    [cnHeightTblAccounts setConstant:tblAccounts.contentSize.height];
    [tblAccounts.layer setCornerRadius:10];
}

#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrAccount.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MISocialAccountTableViewCell";
    MISocialAccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSDictionary *dict  = arrAccount[indexPath.row];
    
    cell.lblTitle.text = [dict stringValueForJSON:@"title"];
    [cell.btnIcon setImage:[UIImage imageNamed:[dict stringValueForJSON:@"icon_normal"]] forState:UIControlStateNormal];
    [cell.btnIcon setImage:[UIImage imageNamed:[dict stringValueForJSON:@"icon_selected"]] forState:UIControlStateSelected];
    cell.btnIcon.selected = cell.btnSelected.selected = false;
    cell.lblTitle.textColor = CRGB(144, 141, 141);
    switch (indexPath.row)
    {
        case 0 : // FACEBOOK
        {
            if (appDelegate.loginUser.is_facebook_linked)
            {
                cell.btnIcon.selected = cell.btnSelected.selected = true;
                cell.lblTitle.textColor = CRGB(71, 89, 147);
            }
            
            break;
        }
        case 1 : // LINKEDIN
        {
            if (appDelegate.loginUser.is_linkedIn_linked)
            {
                cell.btnIcon.selected = cell.btnSelected.selected = true;
                cell.lblTitle.textColor = CRGB(45, 122, 181);
            }
            break;
        }
        case 2 : // GOOGLE
        {
            if (appDelegate.loginUser.is_google_linked)
            {
                cell.btnIcon.selected = cell.btnSelected.selected = true;
                cell.lblTitle.textColor = CRGB(243, 74, 56);
            }
            break;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row)
    {
        case 0 : // FACEBOOK
        {
            if (!appDelegate.loginUser.is_facebook_linked)
            {
                [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                [[APIRequest request] signUpWithFacebookFromViewController:self withCompletion:^(id userInfo, NSError *error)
                 {
                     [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                     if (error)
                     {
                         NSString *title = [NSString stringWithFormat:@"Facebook(%ld)", (long)error.code];
                         [CustomAlertView iOSAlert:title withMessage:error.localizedDescription onView:self];
                     }
                     else if (userInfo)
                     {
                         NSLog(@"%@",userInfo);
                         
                         NSString *fbId = [userInfo[@"id"] isBlankValidationPassed]?userInfo[@"id"]:@"";
                         
                         [[APIRequest request] linkSocialAccount:fbId socialType:@1 completed:^(id responseObject, NSError *error)
                          {
                              if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                              {
                                  appDelegate.loginUser.is_facebook_linked = 1;
                                  [[Store sharedInstance].mainManagedObjectContext save];
                                  
                                  [self->tblAccounts reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                              }
                              
                          }];
                         
                         
                     }
                 }];
                
            }
            
            break;
        }
        case 1 : // LINKEDIN
        {
            if (!appDelegate.loginUser.is_linkedIn_linked)
            {
                [appDelegate loginWithLinkedIn:^(id user, NSError *error)
                {
                    if (user && !error)
                    {
                        NSLog(@"linkedInCallback - %@",user);
                        
                        NSString *linkedInId = [user objectForKey:@"id"]?[user objectForKey:@"id"]:@"";
                        
                        [[APIRequest request] linkSocialAccount:linkedInId socialType:@2 completed:^(id responseObject, NSError *error)
                         {
                             if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                             {
                                 appDelegate.loginUser.is_linkedIn_linked = 1;
                                 [[Store sharedInstance].mainManagedObjectContext save];
                                 
                                 [self->tblAccounts reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                             }
                             
                         }];
                    }
                    else
                    {
                        
                    }
                }];

                
            }
            break;
        }
        case 2 : // GOOGLE
        {
            if (!appDelegate.loginUser.is_google_linked)
            {
                [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                [[APIRequest request] signUpWithGoogleFromViewController:self withCompletion:^(GIDGoogleUser *userInfo, NSError *error)
                 {
                     [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                     if (error)
                     {
                         /*
                          Do not prompt error message for following error.
                          
                          Error Domain=com.google.GIDSignIn Code=-5 "The user canceled the sign-in flow." UserInfo={NSLocalizedDescription=The user canceled the sign-in flow.}
                          */
                         if (error.code == -5) return;
                         
                         NSString *title = [NSString stringWithFormat:@"Google(%ld)", (long)error.code];
                         [CustomAlertView iOSAlert:title withMessage:error.localizedDescription onView:self];
                     }
                     else if (userInfo)
                     {
                        
                         NSString *googleId = [userInfo.userID isBlankValidationPassed]?userInfo.userID:@"";
                         
                         [[APIRequest request] linkSocialAccount:googleId socialType:@3 completed:^(id responseObject, NSError *error)
                          {
                              if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                              {
                                  appDelegate.loginUser.is_google_linked = 1;
                                  [[Store sharedInstance].mainManagedObjectContext save];
                                  [self->tblAccounts reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                              }
                              
                          }];
                         
                     }
                 }];
            }
            break;
        }
    }
    
}

@end

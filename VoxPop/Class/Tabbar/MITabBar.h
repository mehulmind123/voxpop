//
//  MITabBar.h
//  VoxPop
//
//  Created by mac-0007 on 30/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MITabBar : UIView

@property (nonatomic, strong) IBOutlet UIView *viewBack;
@property (nonatomic, strong) IBOutlet UIView *viewSelect;

@property (nonatomic, strong) IBOutlet UIButton *btnTab1;
@property (nonatomic, strong) IBOutlet UIButton *btnTab2;
@property (nonatomic, strong) IBOutlet UIButton *btnTab3;
@property (nonatomic, strong) IBOutlet UIButton *btnTab4;

@property (nonatomic, strong) IBOutlet UILabel *lblTab1;
@property (nonatomic, strong) IBOutlet UILabel *lblTab2;
@property (nonatomic, strong) IBOutlet UILabel *lblTab3;
@property (nonatomic, strong) IBOutlet UILabel *lblTab4;

@property (nonatomic, strong) IBOutlet UIView *viewDot;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *cnCenterViewSelect;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *cnTopBtn1;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *cnTopBtn2;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *cnTopBtn3;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *cnTopBtn4;

+ (id)customTabBar;

@end

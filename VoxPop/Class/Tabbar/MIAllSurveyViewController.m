//
//  MIAllSurveyViewController.m
//  VoxPop
//
//  Created by mac-0007 on 22/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIAllSurveyViewController.h"
#import "MICommentsViewController.h"
#import "MISurveyResultViewController.h"
#import "MIStartSurveyViewController.h"

#import "MISurveyTableViewCell.h"
#import "SurveyPremiseVC.h"

@interface MIAllSurveyViewController ()
{
    NSMutableArray *arrLiveSurveys;
    NSString *timeStampLiveSurveys;
    
    NSMutableArray *arrCompletedSurveys;
    NSString *timeStampCompletedSurveys;
    NSString *timeStamp;
    NSURLSessionDataTask *apiDataTask;
    
    UIRefreshControl *refreshControl;
    
    BOOL needToRefreshOnCurrentViewAppear;
}
@end

@implementation MIAllSurveyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (needToRefreshOnCurrentViewAppear) [self pullToRefresh];
    else needToRefreshOnCurrentViewAppear = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"ALL SURVEYS";
    
    needToRefreshOnCurrentViewAppear = YES;
    timeStampCompletedSurveys =
    timeStampLiveSurveys = @"0";
    
    arrLiveSurveys = [NSMutableArray new];
    arrCompletedSurveys = [NSMutableArray new];
    
    
    //...UISegmentedControl
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    [gradient setFrame:segmentedControl.bounds];
//    [gradient setColors:@[(id)ColorBlue_0064ED.CGColor,
//                          (id)ColorBlue_1190E7.CGColor,
//                          (id)ColorBlue_0090F3.CGColor,
//                          (id)ColorBlue_00AAED.CGColor]];
//    [segmentedControl.layer insertSublayer:gradient atIndex:0];
    [segmentedControl.layer setBorderColor:ColorBlue_3576E9.CGColor];
    [segmentedControl.layer setBorderWidth:1.0f];
    [segmentedControl.layer setCornerRadius:15.0f];
    [segmentedControl.layer setMasksToBounds:YES];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSFontAttributeName:CFontSolidoBold(15), NSForegroundColorAttributeName:ColorBlack_000000} forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSFontAttributeName:CFontSolidoBold(15), NSForegroundColorAttributeName:ColorWhite_FFFFFF} forState:UIControlStateSelected];
    
    timeStamp = @"0";
    [self loadNewSurveyDataFromServer];
    
    //...UITableView
    [tblSurvey registerNib:[UINib nibWithNibName:@"MISurveyTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISurveyTableViewCell"];
    [tblSurvey setRowHeight:UITableViewAutomaticDimension];
    [tblSurvey setEstimatedRowHeight:500];
    [tblSurvey setContentInset:UIEdgeInsetsMake(8, 0, 30, 0)];
    
    //...UIRefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblSurvey addSubview:refreshControl];
    
    
    if(![CUserDefaults booleanForKey:UserDefaultLiveSurvey])
    {
        [CUserDefaults setValue:@1 forKey:UserDefaultLiveSurvey];
        [CustomAlertView iOSAlert:@"" withMessage:CMessageLiveSurvey onView:self];
    }

}

#pragma mark -
#pragma mark - Load Data

- (void)pullToRefresh
{
    if(segmentedControl.selectedSegmentIndex)
        timeStampCompletedSurveys = @"0";
    else
        timeStampLiveSurveys = @"0";
    
    [self loadAllSurveyDataFromServer];
}
- (void)loadAllSurveyDataFromServer
{
    if(!segmentedControl.selectedSegmentIndex &&[timeStampLiveSurveys isEqualToString:@"0"] && !arrLiveSurveys.count && !refreshControl.refreshing)
    {
        [self startLoadingAnimationInView:tblSurvey];
    }
    
    else if([timeStampCompletedSurveys isEqualToString:@"0"] && !arrCompletedSurveys.count && !refreshControl.refreshing)
    {
        [self startLoadingAnimationInView:tblSurvey];
    }
    
    
    if (apiDataTask && apiDataTask.state == NSURLSessionTaskStateRunning)
        [apiDataTask cancel];
    
    
    apiDataTask =  [[APIRequest request] getAllSurveyWithType:segmentedControl.selectedSegmentIndex timestamp:segmentedControl.selectedSegmentIndex ? timeStampCompletedSurveys : timeStampLiveSurveys completed:^(id responseObject, NSError *error)
    {
        [self->refreshControl endRefreshing];
        
        
        if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            [self manageResponse:responseObject];
            
        }
        else if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject] )
        {
            if([self->timeStampLiveSurveys isEqualToString:@"0"] && self->segmentedControl.selectedSegmentIndex == 0)
            {
                [self->arrLiveSurveys removeAllObjects];
                [self->tblSurvey reloadData];
                [self showNoDataFoundInView:self->tblSurvey];
            }
            else if([self->timeStampCompletedSurveys isEqualToString:@"0"] && self->segmentedControl.selectedSegmentIndex == 1)
            {
                [self->arrCompletedSurveys removeAllObjects];
                [self->tblSurvey reloadData];
                [self showNoDataFoundInView:self->tblSurvey];
            }
            
            if (!self->segmentedControl.selectedSegmentIndex)
                self->timeStampLiveSurveys = @"0";
            else
                self->timeStampCompletedSurveys = @"0";

        }
        
        
        if(!self->segmentedControl.selectedSegmentIndex && !self->arrLiveSurveys.count)
        {
            // ... For Live Survey
            [self stopLoadingAnimationInView:self->tblSurvey type:error ?StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound touchUpInsideClickedEvent:^{
                
                [self loadAllSurveyDataFromServer];
            }];
            
        }
        else if (self->segmentedControl.selectedSegmentIndex && !self->arrCompletedSurveys.count)
        {
            // ... For Completed Survey
            [self stopLoadingAnimationInView:self->tblSurvey type:error ?StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound  touchUpInsideClickedEvent:^{
                
                [self loadAllSurveyDataFromServer];
            }];
            
        }
        else
        {
            // ... For Both
            [self removeNodataFoundInView:self->tblSurvey];
            [self stopLoadingAnimationInView:self->tblSurvey type:StopAnimationTypeRemove touchUpInsideClickedEvent:nil];
        }
        
        
        
    }];
    
}

- (void)manageResponse:(id)responseObject
{
    if(!segmentedControl.selectedSegmentIndex)
    {
        // ... For Live Survey
        
        if([timeStampLiveSurveys isEqualToString:@"0"])
            [arrLiveSurveys removeAllObjects];
        
        [arrLiveSurveys addObjectsFromArray:[responseObject valueForKey:CJsonData]];
        timeStampLiveSurveys = [[arrLiveSurveys lastObject] stringValueForJSON:@"created"];
        
    }
    else
    {
        // ... For Completed Survey
        if([timeStampCompletedSurveys isEqualToString:@"0"])
            [arrCompletedSurveys removeAllObjects];
        
        [arrCompletedSurveys addObjectsFromArray:[responseObject valueForKey:CJsonData]];
        timeStampCompletedSurveys = [[arrCompletedSurveys lastObject] stringValueForJSON:@"created"];

        
    }
    [tblSurvey reloadData];
}

#pragma mark -
#pragma mark - Action Event

- (IBAction)segmentedControlChanged:(UISegmentedControl *)sender
{
    
    if(![CUserDefaults booleanForKey:UserDefaultCompletedSurvey])
    {
        [CUserDefaults setValue:@1 forKey:UserDefaultCompletedSurvey];
        [CustomAlertView iOSAlert:@"" withMessage:CMessageCompletedSurvey onView:self];
    }
    
    [self loadAllSurveyDataFromServer];
    [tblSurvey reloadData];
    
}



#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return segmentedControl.selectedSegmentIndex == 0 ? arrLiveSurveys.count : arrCompletedSurveys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MISurveyTableViewCell";
    MISurveyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSDictionary *dict = segmentedControl.selectedSegmentIndex == 0 ? arrLiveSurveys[indexPath.row] : arrCompletedSurveys[indexPath.row];
    
    cell.btnPremise.hidden = YES;
    [cell.lblExpiration setText:[NSString stringWithFormat:@"%@: %@",segmentedControl.selectedSegmentIndex == 0 ? @"Expires on" : @"Expired on",[NSDate stringFromTimeStamp:[dict doubleForKey:@"expired_on"] withFormat:@"MM/dd/yyyy hh:mm a"]]];
    
    [cell configureSurveyWithData:dict andIndexPath:indexPath];
    [cell.btnRefresh setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    
    if (segmentedControl.selectedSegmentIndex == 0)
    {
        [cell.btnRefresh setHidden:YES];
        [cell.lblResponded setHidden:YES];
        [cell.btnSurvey setTitle:@"OPEN SURVEY" forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnRefresh setHidden:NO];
        [cell.lblResponded setHidden:NO];
        [cell.btnSurvey setTitle:@"VIEW RESULT" forState:UIControlStateNormal];
    }
    
    [cell.btnRefresh touchUpInsideClicked:^{
        
        CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        animation.fromValue = [NSNumber numberWithFloat:0.0f];
        animation.toValue = [NSNumber numberWithFloat: 2*M_PI];
        animation.duration = 1.0f;
        animation.repeatCount = INFINITY;
        if([cell.btnRefresh.imageView.layer animationForKey:@"SpinAnimation"])
            [cell.btnRefresh.imageView.layer removeAnimationForKey:@"SpinAnimation"];
        [cell.btnRefresh.imageView.layer addAnimation:animation forKey:@"SpinAnimation"];

        [cell.btnRefresh setUserInteractionEnabled:NO];
        
        [[APIRequest request] getUserRespondedCount:[dict numberForJson:@"id"] completed:^(id responseObject, NSError *error)
         {
             if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
             {
                 NSMutableDictionary *dictMut = [[NSMutableDictionary alloc] initWithDictionary:dict];
                 [dictMut setValue:[[responseObject valueForKey:CJsonData] stringValueForJSON:@"count"] forKey:@"total_responded_users"];
                 [self->arrCompletedSurveys replaceObjectAtIndex:indexPath.row withObject:dictMut];
                 
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [cell.btnRefresh setUserInteractionEnabled:YES];
                     [self->tblSurvey reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                 });
                 
             }
             else
                 [cell.btnRefresh setUserInteractionEnabled:YES];
             
         }];
    }];
    
    if (![[dict stringValueForJSON:@"survey_premise"] isEqualToString:@""]) {
        cell.btnPremise.hidden = NO;
        // ...Clicked event of button Premise
        [cell.btnPremise touchUpInsideClicked:^{
            
            SurveyPremiseVC *surveyPremiseVC = [SurveyPremiseVC initWithXib];
            surveyPremiseVC.strSurveyPremise = [dict stringValueForJSON:@"survey_premise"];
            
            if (self->segmentedControl.selectedSegmentIndex == 1){
                surveyPremiseVC.isFromMySurveyOrCompleted = YES;
            }
            surveyPremiseVC.dictOpenSurveyViewResult = dict;
            [self.navigationController pushViewController:surveyPremiseVC animated:YES];
            //        [self presentViewController:surveyPremiseVC animated:YES completion:nil];
        }];
    }
    
    //...Clicked Event
    [cell.btnBlockCustomer touchUpInsideClicked:^{
        [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:CMessageBlockCustomerTitle message:CMessageBlockCustomerMessage firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
         {
             
             [[APIRequest request] blockUnblockUser:[dict[@"subscriber"] numberForJson:@"id"] status:YES completed:^(id responseObject, NSError *error)
              {
                  if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                  {
                      if (self->segmentedControl.selectedSegmentIndex == 0)
                      {
                          //...
                          NSIndexSet *indexSet = [self->arrLiveSurveys indexesOfObjectsPassingTest:^(id obj, NSUInteger idx, BOOL *stop)
                                                  {
                                                      return [obj[@"subscriber"][@"id"] isEqual:dict[@"subscriber"][@"id"]];
                                                  }];
                          
                          [self->arrLiveSurveys removeObjectsAtIndexes:indexSet];
                          
                          //...
                          NSIndexSet *indexSet1 = [self->arrCompletedSurveys indexesOfObjectsPassingTest:^(id obj, NSUInteger idx, BOOL *stop)
                                                  {
                                                      return [obj[@"subscriber"][@"id"] isEqual:dict[@"subscriber"][@"id"]];
                                                  }];
                          
                          [self->arrCompletedSurveys removeObjectsAtIndexes:indexSet1];
                          
                          if (self->arrCompletedSurveys.count == 0)
                          {
                              self->timeStampCompletedSurveys = @"0";
                          }
                          
                          //...
                          NSMutableArray *allIndexPaths = [[NSMutableArray alloc] init];
                          
                          [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop)
                           {
                               NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx inSection:0];
                               [allIndexPaths addObject:indexPath];
                           }];
                          
                          
                          //...
                          [self->tblSurvey deleteRowsAtIndexPaths:allIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                          [self->tblSurvey performSelector:@selector(reloadData) withObject:nil afterDelay:0.3];
                          
                          if (self->arrLiveSurveys.count == 0)
                          {
                              [self showNoDataFoundInView:self->tblSurvey];
                          }

                      }
                      else
                      {
                          //...
                          NSIndexSet *indexSet = [self->arrCompletedSurveys indexesOfObjectsPassingTest:^(id obj, NSUInteger idx, BOOL *stop)
                                                  {
                                                      return [obj[@"subscriber"][@"id"] isEqual:dict[@"subscriber"][@"id"]];
                                                  }];
                          
                          [self->arrCompletedSurveys removeObjectsAtIndexes:indexSet];
                          
                          //...
                          NSIndexSet *indexSet1 = [self->arrLiveSurveys indexesOfObjectsPassingTest:^(id obj, NSUInteger idx, BOOL *stop)
                                                  {
                                                      return [obj[@"subscriber"][@"id"] isEqual:dict[@"subscriber"][@"id"]];
                                                  }];
                          
                          [self->arrLiveSurveys removeObjectsAtIndexes:indexSet1];
                          if (self->arrLiveSurveys.count == 0)
                          {
                              self->timeStampLiveSurveys = @"0";
                          }
                          
                          //...
                          NSMutableArray *allIndexPaths = [[NSMutableArray alloc] init];
                          
                          [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop)
                           {
                               NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx inSection:0];
                               [allIndexPaths addObject:indexPath];
                           }];
                          
                          
                          [self->tblSurvey deleteRowsAtIndexPaths:allIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                          [self->tblSurvey performSelector:@selector(reloadData) withObject:nil afterDelay:0.3];
                          
                          if (self->arrCompletedSurveys.count == 0)
                          {
                              [self showNoDataFoundInView:self->tblSurvey];
                          }


                      }
                      
                      
                      
                  }
              }];
             
         } secondButton:@"No" secondHandler:^(UIAlertAction *action)
         {
             
         } inView:self];
    }];
    
    [cell.btnSurvey touchUpInsideClicked:^{
        
        self->needToRefreshOnCurrentViewAppear = NO;
        
        if ([cell.btnSurvey.titleLabel.text isEqualToString:@"OPEN SURVEY"])
        {
            
            MIStartSurveyViewController *startSurveyVC = [[MIStartSurveyViewController alloc] initWithNibName:@"MIStartSurveyViewController" bundle:nil];
            [startSurveyVC setIObject:[dict numberForJson:@"id"]];
            [startSurveyVC setBlock:^(id object, NSError *error)
             {
                 [appDelegate.tabBarViewController.tabBarView.btnTab2 sendActionsForControlEvents:UIControlEventTouchUpInside];
                 
                 UINavigationController *nav = [appDelegate.tabBarViewController.viewControllers objectAtIndex:1];
                
                 
                 MISurveyResultViewController *resultVC = [[MISurveyResultViewController alloc] initWithNibName:@"MISurveyResultViewController" bundle:nil];
                 [resultVC setIObject:[dict numberForJson:@"id"]];
                 [nav pushViewController:resultVC animated:YES];
                 self->needToRefreshOnCurrentViewAppear = YES;
                 
             }];
            
            [self presentViewController:[UINavigationController navigationControllerWithRootViewController:startSurveyVC] animated:YES completion:nil];

        }
        else
        {
            
            MISurveyResultViewController *resultVC = [[MISurveyResultViewController alloc] initWithNibName:@"MISurveyResultViewController" bundle:nil];
            [resultVC setIObject:[dict numberForJson:@"id"]];
            [self.navigationController pushViewController:resultVC animated:YES];
        }
    }];
    
    [cell.btnComments touchUpInsideClicked:^{
        
        if(self->segmentedControl.selectedSegmentIndex == 0)
        {
            [CustomAlertView iOSAlert:@"" withMessage:CMessageUnableToPostComment onView:self];
        }
        else
        {
            self->needToRefreshOnCurrentViewAppear = NO;
            
            MICommentsViewController *commentsVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
            [commentsVC setIObject:@{@"id":[dict numberForJson:@"id"],
                                     @"can_comment":[dict numberForJson:@"can_comment"]}];
            [self.navigationController pushViewController:commentsVC animated:YES];
        }
    }];
    
    
    if(segmentedControl.selectedSegmentIndex == 0 && indexPath.row == arrLiveSurveys.count - 1 && ![timeStampLiveSurveys isEqualToString:@"0"])
        [self loadAllSurveyDataFromServer];
    
    else if(segmentedControl.selectedSegmentIndex == 1 && indexPath.row == arrCompletedSurveys.count - 1 && ![timeStampCompletedSurveys isEqualToString:@"0"])
        [self loadAllSurveyDataFromServer];
    


    return cell;
}

- (void)loadNewSurveyDataFromServer
{
    
    [[APIRequest request] getNewSurveyWithTimestamp:timeStamp completed:^(id responseObject, NSError *error)
     {
         NSMutableArray *arrSurveys = [[NSMutableArray alloc] init];
         if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
         {
             [arrSurveys addObjectsFromArray:[responseObject valueForKey:CJsonData]];
             self->timeStamp =  [[arrSurveys lastObject] stringValueForJSON:@"created"];
             [appDelegate.tabBarViewController.tabBarView.viewDot setHidden:!arrSurveys.count];
         }
     }];
}

@end

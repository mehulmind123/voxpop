//
//  MITabBarViewController.m
//  VoxPop
//
//  Created by mac-0007 on 18/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "MITabBarViewController.h"

#import "MIHomeViewController.h"
#import "MIMySurveyViewController.h"
#import "MIAllSurveyViewController.h"
#import "MIProfileViewController.h"

#define CTabAnimationDuration   0.2

@interface MITabBarViewController ()

@end

@implementation MITabBarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.tabBarView = [MITabBar customTabBar];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tabBarView];
    [self.tabBar setHidden:YES];
    
    
    //....UIApplicationWillChangeStatusBarFrameNotification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameFrameWillChange) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    
    
    //.......Self ViewControllers........
    MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
    [homeVC setShowTabBar:YES];
    
    MIMySurveyViewController *mySurveyVC = [[MIMySurveyViewController alloc] initWithNibName:@"MIMySurveyViewController" bundle:nil];
    [mySurveyVC setShowTabBar:YES];
    
    
    MIAllSurveyViewController *allSurveyVC = [[MIAllSurveyViewController alloc] initWithNibName:@"MIAllSurveyViewController" bundle:nil];
    [allSurveyVC setShowTabBar:YES];
    
    
    MIProfileViewController *profileVC = [[MIProfileViewController alloc] initWithNibName:@"MIProfileViewController" bundle:nil];
    [profileVC setShowTabBar:YES];
    
    self.viewControllers = @[[[UINavigationController alloc] initWithRootViewController:homeVC],
                             [[UINavigationController alloc] initWithRootViewController:mySurveyVC],
                             [[UINavigationController alloc] initWithRootViewController:allSurveyVC],
                             [[UINavigationController alloc] initWithRootViewController:profileVC]];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    [super setSelectedIndex:selectedIndex];
    
//    self.tabBarView.btnTab1.selected =
//    self.tabBarView.btnTab2.selected =
//    self.tabBarView.btnTab3.selected =
//    self.tabBarView.btnTab4.selected = NO;
//    
//    self.tabBarView.lblTab1.textColor =
//    self.tabBarView.lblTab2.textColor =
//    self.tabBarView.lblTab3.textColor =
//    self.tabBarView.lblTab4.textColor = ColorGray_CCCCCC;
    
    switch (selectedIndex)
    {
        case 0:
        {
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [UIView animateWithDuration:CTabAnimationDuration animations:^{
                [self.tabBarView.cnTopBtn1 setConstant:0];
                [self.tabBarView.cnTopBtn2 setConstant:16];
                [self.tabBarView.cnTopBtn3 setConstant:16];
                [self.tabBarView.cnTopBtn4 setConstant:16];
                [self.tabBarView.cnCenterViewSelect setConstant:self.tabBarView.btnTab1.center.x];
                [self.tabBarView layoutIfNeeded];
            } completion:^(BOOL finished) {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.tabBarView.btnTab1 setSelected:YES];
                
                self.tabBarView.btnTab2.selected =
                self.tabBarView.btnTab3.selected =
                self.tabBarView.btnTab4.selected = NO;

                self.tabBarView.lblTab2.textColor =
                self.tabBarView.lblTab3.textColor =
                self.tabBarView.lblTab4.textColor = ColorBlack_000000;
                
                
                [self.tabBarView.lblTab1 setTextColor:ColorWhite_FFFFFF];
            }];
            
            break;
        }
        case 1:
        {
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [UIView animateWithDuration:CTabAnimationDuration animations:^{
                [self.tabBarView.cnTopBtn1 setConstant:16];
                [self.tabBarView.cnTopBtn2 setConstant:0];
                [self.tabBarView.cnTopBtn3 setConstant:16];
                [self.tabBarView.cnTopBtn4 setConstant:16];
                [self.tabBarView.cnCenterViewSelect setConstant:self.tabBarView.btnTab2.center.x];
                [self.tabBarView layoutIfNeeded];
            } completion:^(BOOL finished) {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.tabBarView.btnTab2 setSelected:YES];
                self.tabBarView.btnTab1.selected =
                self.tabBarView.btnTab3.selected =
                self.tabBarView.btnTab4.selected = NO;
                
                self.tabBarView.lblTab1.textColor =
                self.tabBarView.lblTab3.textColor =
                self.tabBarView.lblTab4.textColor = ColorBlack_000000;
                [self.tabBarView.lblTab2 setTextColor:ColorWhite_FFFFFF];
            }];
            
            break;
        }
        case 2:
        {
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [UIView animateWithDuration:CTabAnimationDuration animations:^{
                [self.tabBarView.cnTopBtn1 setConstant:16];
                [self.tabBarView.cnTopBtn2 setConstant:16];
                [self.tabBarView.cnTopBtn3 setConstant:0];
                [self.tabBarView.cnTopBtn4 setConstant:16];
                [self.tabBarView.cnCenterViewSelect setConstant:self.tabBarView.btnTab3.center.x];
                [self.tabBarView layoutIfNeeded];
            } completion:^(BOOL finished) {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.tabBarView.btnTab3 setSelected:YES];
                self.tabBarView.btnTab1.selected =
                self.tabBarView.btnTab2.selected =
                self.tabBarView.btnTab4.selected = NO;
                
                self.tabBarView.lblTab1.textColor =
                self.tabBarView.lblTab2.textColor =
                self.tabBarView.lblTab4.textColor = ColorBlack_000000;
                
                [self.tabBarView.lblTab3 setTextColor:ColorWhite_FFFFFF];
            }];
            
            break;
        }
        case 3:
        {
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [UIView animateWithDuration:CTabAnimationDuration animations:^{
                [self.tabBarView.cnTopBtn1 setConstant:16];
                [self.tabBarView.cnTopBtn2 setConstant:16];
                [self.tabBarView.cnTopBtn3 setConstant:16];
                [self.tabBarView.cnTopBtn4 setConstant:0];
                [self.tabBarView.cnCenterViewSelect setConstant:self.tabBarView.btnTab4.center.x];
                [self.tabBarView layoutIfNeeded];
            } completion:^(BOOL finished) {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.tabBarView.btnTab4 setSelected:YES];
                self.tabBarView.btnTab1.selected =
                self.tabBarView.btnTab2.selected =
                self.tabBarView.btnTab3.selected = NO;
                
                self.tabBarView.lblTab1.textColor =
                self.tabBarView.lblTab2.textColor =
                self.tabBarView.lblTab3.textColor = ColorBlack_000000;
                
                [self.tabBarView.lblTab4 setTextColor:ColorWhite_FFFFFF];
            }];
            
            break;
        }
    }
    
}

- (NSUInteger)indexOfViewControllerClass:(Class)cl
{
    __block NSUInteger index = 0;
    [self.viewControllers enumerateObjectsUsingBlock:^(UINavigationController *nav, NSUInteger idx, BOOL *stop)
     {
         if ([[nav.viewControllers firstObject] isKindOfClass:cl]) {
             index = idx;
             *stop = YES;
         }
     }];
    return index;
}


#pragma mark -
#pragma mark - UIApplicationWillChangeStatusBarFrameNotification

- (void)statusBarFrameFrameWillChange
{
    CViewSetHeight(self.view, CScreenHeight - (CStatusBarHeight - 20));
}


#pragma mark -
#pragma mark - UITabBarControllerDelegate




#pragma mark -
#pragma mark - UITabBarDelegate



@end

//
//  MITakeSurveyViewController.h
//  VoxPop
//
//  Created by mac-0007 on 23/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MITakeSurveyViewController : MIParentViewController
{
    IBOutlet UIView *viewSurvey;
    IBOutlet UIImageView *imgVSubscriber;
    IBOutlet UILabel *lblSubscriber;
    IBOutlet UILabel *lblContactName;
    IBOutlet UIButton *btnBlockCustomer;
    IBOutlet UILabel *lblSurveyTitle;
    IBOutlet UILabel *lblExpiration;
    IBOutlet UILabel *lblResponded;
    IBOutlet UICollectionView *collVTopics;
    IBOutlet UIButton *btnComments;
    
    IBOutlet UIView *viewYoutube;
    IBOutlet UIImageView *imgVYoutube;
    IBOutlet UILabel *lblYoutubeLink;
}
@end

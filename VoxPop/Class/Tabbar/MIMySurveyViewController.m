//
//  MIMySurveyViewController.m
//  VoxPop
//
//  Created by mac-0007 on 22/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIMySurveyViewController.h"
#import "MICommentsViewController.h"
#import "MISurveyResultViewController.h"
#import "MIStartSurveyViewController.h"
#import "MIHomeViewController.h"

#import "MISurveyTableViewCell.h"

#import "SurveyPremiseVC.h"

@interface MIMySurveyViewController ()
{
    NSMutableArray *arrSurveys;
    NSString *timeStamp;
    
    NSURLSessionDataTask *apiDataTask;
        
    UIRefreshControl *refreshControl;
    
    BOOL needToRefreshOnCurrentViewAppear;

}
@end

@implementation MIMySurveyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (needToRefreshOnCurrentViewAppear) [self pullToRefresh];
    else needToRefreshOnCurrentViewAppear = YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"MY SURVEYS TAKEN";
    
    needToRefreshOnCurrentViewAppear = YES;
    timeStamp = @"0";
    
    arrSurveys = [NSMutableArray new];
    
    //...UITableView
    [tblSurvey registerNib:[UINib nibWithNibName:@"MISurveyTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISurveyTableViewCell"];
    [tblSurvey setRowHeight:UITableViewAutomaticDimension];
    [tblSurvey setEstimatedRowHeight:500];
    [tblSurvey setContentInset:UIEdgeInsetsMake(8, 0, 30, 0)];
    
    //...UIRefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblSurvey addSubview:refreshControl];
    
    //
    [self loadNewSurveyDataFromServer];
}


#pragma mark -
#pragma mark - Load Data

- (void)pullToRefresh
{
    timeStamp = @"0";
    
    [self loadMySurveyDataFromServer];
}
- (void)loadMySurveyDataFromServer
{
    
    
    if([timeStamp isEqualToString:@"0"] && !arrSurveys.count && !refreshControl.refreshing)
    {
        [self startLoadingAnimationInView:tblSurvey];
    }
    
    
    if (apiDataTask && apiDataTask.state == NSURLSessionTaskStateRunning)
        [apiDataTask cancel];
    
    
    apiDataTask =  [[APIRequest request] getMySurveyWithTimestamp:timeStamp completed:^(id responseObject, NSError *error)
       {
           
           [refreshControl endRefreshing];
           
           
           if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
           {
               if([timeStamp isEqualToString:@"0"])
                   [arrSurveys removeAllObjects];
               
               [arrSurveys addObjectsFromArray:[responseObject valueForKey:CJsonData]];
               timeStamp =  [[arrSurveys lastObject] stringValueForJSON:@"created"];
               [tblSurvey reloadData];
           }
           else if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject] && [timeStamp isEqualToString:@"0"])
           {
               [arrSurveys removeAllObjects];
               [tblSurvey reloadData];
               [self showNoDataFoundInView:tblSurvey];
           }

           else
               timeStamp = @"0";

           
           
           if(!arrSurveys.count)
           {
               // ... For My Survey
               [self stopLoadingAnimationInView:tblSurvey type:error ?StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound touchUpInsideClickedEvent:^{
                   
                   [self loadMySurveyDataFromServer];
               }];
               
           }
           
           else
           {
               [self removeNodataFoundInView:tblSurvey];
               [self stopLoadingAnimationInView:tblSurvey type:StopAnimationTypeRemove touchUpInsideClickedEvent:nil];
           }
           
           
       }];
}



#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSurveys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MISurveyTableViewCell";
    MISurveyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    cell.btnPremise.hidden = YES;
    
    NSDictionary *dict = arrSurveys[indexPath.row];
    [cell.lblExpiration setText: [NSString stringWithFormat:@"Expires on: %@",[NSDate stringFromTimeStamp:[dict doubleForKey:@"expired_on"] withFormat:@"MM/dd/yyyy hh:mm a"]]];
    
    [cell configureSurveyWithData:dict andIndexPath:indexPath];
    
    [cell.btnSurvey setTitle:@"VIEW RESULT" forState:UIControlStateNormal];
    [cell.btnRefresh setHidden:NO];
    [cell.btnRefresh setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    //...Clicked Event
    [cell.btnRefresh touchUpInsideClicked:^{
        
        CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        animation.fromValue = [NSNumber numberWithFloat:0.0f];
        animation.toValue = [NSNumber numberWithFloat: 2*M_PI];
        animation.duration = 1.0f;
        animation.repeatCount = INFINITY;
    
        if([cell.btnRefresh.imageView.layer animationForKey:@"SpinAnimation"])
            [cell.btnRefresh.imageView.layer removeAnimationForKey:@"SpinAnimation"];
        
        [cell.btnRefresh.imageView.layer addAnimation:animation forKey:@"SpinAnimation"];
       
        //NSURL *url  = [[NSBundle mainBundle] URLForResource:@"refrsh" withExtension:@"gif"];
        //[cell.btnRefresh setImage:[JTSAnimatedGIFUtility animatedImageWithAnimatedGIFURL:url] forState:UIControlStateNormal];
        
        
        [cell.btnRefresh setUserInteractionEnabled:NO];
        [[APIRequest request] getUserRespondedCount:[dict numberForJson:@"id"] completed:^(id responseObject, NSError *error)
         {
             if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
             {
                 NSMutableDictionary *dictMut = [[NSMutableDictionary alloc] initWithDictionary:dict];
                 [dictMut setValue:[[responseObject valueForKey:CJsonData] stringValueForJSON:@"count"] forKey:@"total_responded_users"];
                 [arrSurveys replaceObjectAtIndex:indexPath.row withObject:dictMut];
                 
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [cell.btnRefresh setUserInteractionEnabled:YES];
                     [tblSurvey reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//                     [tblSurvey reloadData];
                 });
                 
             }
             else
                 [cell.btnRefresh setUserInteractionEnabled:YES];
            
        }];
    }];
    
    if (![[dict stringValueForJSON:@"survey_premise"] isEqualToString:@""]) {
        cell.btnPremise.hidden = NO;
        // ...Clicked event of button Premise
        [cell.btnPremise touchUpInsideClicked:^{
            
            SurveyPremiseVC *surveyPremiseVC = [SurveyPremiseVC initWithXib];
            surveyPremiseVC.strSurveyPremise = [dict stringValueForJSON:@"survey_premise"];
            surveyPremiseVC.isFromMySurveyOrCompleted = YES;
            surveyPremiseVC.dictOpenSurveyViewResult = dict;
            [self.navigationController pushViewController:surveyPremiseVC animated:YES];
            //        [self presentViewController:surveyPremiseVC animated:YES completion:nil];
        }];
    }
    
    [cell.btnBlockCustomer touchUpInsideClicked:^{
        [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:CMessageBlockCustomerTitle message:CMessageBlockCustomerMessage firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
         {
            [[APIRequest request] blockUnblockUser:[dict[@"subscriber"] numberForJson:@"id"] status:YES completed:^(id responseObject, NSError *error)
              {
                  if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                  { /*
                      NSIndexSet *indexSet = [arrSurveys indexesOfObjectsPassingTest:^(id obj, NSUInteger idx, BOOL *stop)
                                              {
                                                  return [obj[@"subscriber"][@"id"] isEqual:dict[@"subscriber"][@"id"]];
                                              }];
                      
                      [arrSurveys removeObjectsAtIndexes:indexSet];
                      
                      NSMutableArray *allIndexPaths = [[NSMutableArray alloc] init];
                      
                      [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop)
                       {
                           NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx inSection:0];
                           [allIndexPaths addObject:indexPath];
                       }];
                      
                      
                      [tblSurvey deleteRowsAtIndexPaths:allIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                      [tblSurvey performSelector:@selector(reloadData) withObject:nil afterDelay:0.3];
                      
                      if (arrSurveys.count == 0)
                      {
                          [self showNoDataFoundInView:tblSurvey];
                      }
*/
                      
                  }
              }];
         }
         secondButton:@"No" secondHandler:^(UIAlertAction *action)
         {
             
         } inView:self];
    }];
    
    [cell.btnSurvey touchUpInsideClicked:^{
        
        needToRefreshOnCurrentViewAppear = NO;
        
        MISurveyResultViewController *resultVC = [[MISurveyResultViewController alloc] initWithNibName:@"MISurveyResultViewController" bundle:nil];
        [resultVC setIObject:[dict numberForJson:@"id"]];
        [self.navigationController pushViewController:resultVC animated:YES];
        
    }];
    
    [cell.btnComments touchUpInsideClicked:^{
        
        needToRefreshOnCurrentViewAppear = NO;
        
        MICommentsViewController *commentsVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
        [commentsVC setIObject:@{@"id":[dict numberForJson:@"id"],
                                 @"can_comment":[dict numberForJson:@"can_comment"]}];
        [self.navigationController pushViewController:commentsVC animated:YES];
    }];
    
    if(indexPath.row == arrSurveys.count - 1 && ![timeStamp isEqualToString:@"0"])
        [self loadMySurveyDataFromServer];
    

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)loadNewSurveyDataFromServer
{
    [[APIRequest request] getNewSurveyWithTimestamp:@"0" completed:^(id responseObject, NSError *error)
     {
         NSMutableArray *arrSurveys = [[NSMutableArray alloc] init];
         if([responseObject valueForKey:CJsonData])
         {
             [arrSurveys addObjectsFromArray:[responseObject valueForKey:CJsonData]];
             [appDelegate.tabBarViewController.tabBarView.viewDot setHidden:!arrSurveys.count];
         }
     }];
}

@end

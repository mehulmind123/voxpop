//
//  MIBlockedListViewController.h
//  VoxPop
//
//  Created by mac-0007 on 26/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIBlockedListViewController : MIParentViewController
{
    IBOutlet UITableView *tblBlockedList;
    IBOutlet NSLayoutConstraint *cnHeightTblBlock;
    IBOutlet UIView *viewBackground;
}
@end

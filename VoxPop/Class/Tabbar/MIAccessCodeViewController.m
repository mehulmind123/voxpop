//
//  MIAccessCodeViewController.m
//  VoxPop
//
//  Created by mac-0007 on 26/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIAccessCodeViewController.h"

#import "MITableViewCell.h"

@interface MIAccessCodeViewController ()
{
    NSMutableArray *arrCodes;
    NSInteger iOffset;
}
@property (nonatomic, strong) MITableViewCell *prototypeCell;
@end

@implementation MIAccessCodeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"MANAGE ACCESS CODES";
    
    arrCodes = [NSMutableArray new];
    //....NavigationBarItem
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"add"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnBarAddCodeClicked:)];
    
    
    
    //....UITableView
    [tblCodes registerNib:[UINib nibWithNibName:@"MITableViewCell" bundle:nil] forCellReuseIdentifier:@"MITableViewCell"];
    
    [self reloadAccessCodes];
    
    iOffset = -1;
    [self loadAccessCodeFromServer];
}


#pragma mark -
#pragma mark - Load Data

- (void)reloadAccessCodes
{
    [tblCodes reloadData];
    [cnHeightTblCodes setConstant:tblCodes.contentSize.height];
    [tblCodes.layer setCornerRadius:10];
}

- (void)addAccessCode:(NSString*)accessCode
{
    [[APIRequest request] addAccessCode:accessCode completed:^(id responseObject, NSError *error)
    {
        if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            
            [arrCodes insertObject:[responseObject valueForJSON:CJsonData] atIndex:0];
            
            [self reloadAccessCodes];
            
            if(arrCodes.count)
                [self stopLoadingAnimationInView:viewBackground type:StopAnimationTypeRemove touchUpInsideClickedEvent:nil];
            [self removeNodataFoundInView:viewBackground];
        }        
    }];
}
- (void)loadAccessCodeFromServer
{
     [self startLoadingAnimationInView:viewBackground];
    
    [[APIRequest request] loadAccessCodeWithOffset:iOffset completed:^(id responseObject, NSError *error)
    {
       
        
        if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            arrCodes = [[responseObject valueForKey:CJsonData] mutableCopy];
            [self reloadAccessCodes];
            
        }
        
        [self stopLoadingAnimationInView:viewBackground type:error ? StopAnimationTypeErrorTapToRetry : !arrCodes.count ? StopAnimationTypeDataNotFound : StopAnimationTypeRemove  touchUpInsideClickedEvent:^{
            
            [self loadAccessCodeFromServer];
        }];
        
    }];

}



#pragma mark -
#pragma mark - Action Event

- (void)btnBarAddCodeClicked:(UIBarButtonItem *)barButtonItem
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Enter Access Code" message: @"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
    {
        [textField setPlaceholder:@"Access Code"];
        [textField setPlaceHolderColor:ColorBlueSky_249CFE];
        [textField setTextColor:ColorBlueSky_249CFE];
        [textField setClearButtonMode:UITextFieldViewModeWhileEditing];
        [textField setBorderStyle:UITextBorderStyleNone];
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeAllCharacters];
        
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
   
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        UITextField *txtAccessCode = alertController.textFields[0];
        if (txtAccessCode.text.isBlankValidationPassed)
        {
            [self addAccessCode:txtAccessCode.text.uppercaseString];
         
        }
        else
        {
            [CustomAlertView iOSAlert:@"Access code can not be blank." withMessage:@"" onView:self buttonTitle:@"OK" handler:^(UIAlertAction *action)
            {
                [self btnBarAddCodeClicked:nil];
            }];
        }
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}



#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrCodes.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    
    [self.prototypeCell updateConstraintsIfNeeded];
    [self.prototypeCell layoutIfNeeded];
    
    return [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MITableViewCell";
    MITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (MITableViewCell *)prototypeCell
{
    static NSString *identifier = @"MITableViewCell";
    
    if (!_prototypeCell)
        _prototypeCell = [tblCodes dequeueReusableCellWithIdentifier:identifier];
    
    return _prototypeCell;
}

- (void)configureCell:(MITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dict = arrCodes[indexPath.row];
    
    [cell.lblTitle setText:[NSString stringWithFormat:@"%@: %@",[dict stringValueForJSON:@"code_name"],[dict stringValueForJSON:@"code"]]];
    [cell.btnLockUnlock setHidden:YES];
    
    [cell.btnNextRemove touchUpInsideClicked:^{
        
        [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageRemoveAccessCode firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
         {
             [[APIRequest request] deleteAccessCode:[dict integerForKey:@"id"] completed:^(id responseObject, NSError *error)
             {
                 if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     [TBLAccessCode deleteObjects:[NSPredicate predicateWithFormat:@"access_id = %d",[dict integerForKey:@"id"]]];
                     [arrCodes removeObjectAtIndex:indexPath.row];
                     [self reloadAccessCodes];
                     
                     if(!arrCodes.count)
                         [self showNoDataFoundInView:viewBackground];
                 }
                 
             }];
             
         } secondButton:@"No" secondHandler:nil inView:self];
    }];
}


@end

//
//  MIStartSurveyViewController.m
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIStartSurveyViewController.h"
#import "MICompletedSurveyViewController.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>

#import "MIAnswerTableViewCell.h"

#define CNextButtonText @"NEXT  "
#define CDoneButtonText @"DONE  "

@interface MIStartSurveyViewController ()
{
    NSArray *arrAnswers;
    BOOL hasMultipleAnswers;
}
@property (nonatomic, strong) MIAnswerTableViewCell *prototypeCell;
@end

@implementation MIStartSurveyViewController
@synthesize surveyInfo;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.navigationItem.hidesBackButton = YES;
    
    
    //....NavigationBarItem
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"quitSurvey"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnBarItemQuitClicked:)];
    
    
    //....UISwipeGestureRecognizer
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(btnNextClicked:)];
    [leftSwipe setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(btnPreviousClicked:)];
    [rightSwipe setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [self.view addGestureRecognizer:leftSwipe];
    [self.view addGestureRecognizer:rightSwipe];
    
    
    //....UITableView
    [tblAnswers registerNib:[UINib nibWithNibName:@"MIAnswerTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIAnswerTableViewCell"];
    
    if (surveyInfo == nil)
        [self loadQADetailFromServer];  //...First QA
    else
        [self loadQADetailForSelf];
}

#pragma mark -
#pragma mark - Load Data

- (void)loadQADetailForSelf
{
    NSInteger indexOfSelf = [self.navigationController.viewControllers indexOfObject:self];
    self.title = [NSString stringWithFormat:@"QUESTION %ld", indexOfSelf + 1];
    
    SurveyQA *surveyQA = [surveyInfo.surveyQA objectAtIndex:indexOfSelf];
    
    if (surveyQA)
    {
        arrAnswers = surveyQA.answers;
        
        if ((int)surveyQA.questionType == 1)
        {
            [lblQuestion setText:surveyQA.question];
            [btnQuestion setEnabled:NO];
        }
        else
        {
            [lblQuestion setText:@"Click here to watch video for the question."];
            [btnQuestion setEnabled:YES];
        }
        
        
        hasMultipleAnswers = surveyQA.hasMultipleAnswers;
        [tblAnswers setAllowsMultipleSelection:hasMultipleAnswers];
        [lblAnswerType setText:hasMultipleAnswers?@"(Multiple Answers)":@"(Choose 1)"];
    }
    
    [tblAnswers reloadData];
    [cnHeightTblAnswers setConstant:tblAnswers.contentSize.height];
    
    
    //...Prev/Next
    if (indexOfSelf == 0) {
        [btnPrevious setHidden:YES];
        [btnNext setHidden:NO];
    } else {
        [btnPrevious setHidden:NO];
        [btnNext setHidden:NO];
    }
    
    if (indexOfSelf == ([surveyInfo.surveyQA count] - 1))
        [lblNextTitle setText:CDoneButtonText];
    else
        [lblNextTitle setText:CNextButtonText];
}

- (void)loadQADetailFromServer
{
    [self startLoadingAnimationInView:viewQAContainer];
    
    [[APIRequest request] getSurveyDetails:self.iObject completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
         {
             surveyInfo = [MISurvey modelObjectWithDictionary:[responseObject valueForKey:CJsonData]];
             [self loadQADetailForSelf];
         }
         
         if (!surveyInfo)
         {
             //... (Data not available Or error) AND (Stop loader animation)
             [self stopLoadingAnimationInView:viewQAContainer type:error ?StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound touchUpInsideClickedEvent:^
             {
                 [self loadQADetailFromServer];
             }];
         }
         else //... Stop loader animation
             [self stopLoadingAnimationInView:viewQAContainer type:StopAnimationTypeRemove touchUpInsideClickedEvent:nil];
     }];
}




#pragma mark -
#pragma mark - Action Event

- (void)btnBarItemQuitClicked:(UIBarButtonItem *)barButtonItem
{
    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:CMessageQuitSurveyTitle message:CMessageQuitSurveyMessage firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
     {
         [self.navigationController dismissViewControllerAnimated:YES completion:nil];
     } secondButton:@"No" secondHandler:nil inView:self];
}

- (IBAction)btnQuestionClicked:(UIButton *)sender
{
    NSInteger indexOfSelf = [self.navigationController.viewControllers indexOfObject:self];
    SurveyQA *surveyQA = [surveyInfo.surveyQA objectAtIndex:indexOfSelf];
    NSString *youtubeIdentifier = [[surveyQA.question componentsSeparatedByString:@"v="] lastObject];
    
    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:youtubeIdentifier];
    videoPlayerViewController.preferredVideoQualities = @[@(XCDYouTubeVideoQualitySmall240), @(XCDYouTubeVideoQualityMedium360)];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewController.moviePlayer];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
}

- (IBAction)btnPreviousClicked:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnNextClicked:(UIButton *)sender
{
//    if (tblAnswers.indexPathsForSelectedRows.count > 0)
    {
        if ([lblNextTitle.text isEqualToString:CNextButtonText])
        {
            MIStartSurveyViewController *startSurveyVC = [[MIStartSurveyViewController alloc] initWithNibName:@"MIStartSurveyViewController" bundle:nil];
            [startSurveyVC setIObject:self.iObject];
            [startSurveyVC setSurveyInfo:surveyInfo];
            [startSurveyVC setBlock:self.block];
            [self.navigationController pushViewController:startSurveyVC animated:YES];
        }
        else
        {
            MICompletedSurveyViewController *completeSurveyVC = [[MICompletedSurveyViewController alloc] initWithNibName:@"MICompletedSurveyViewController" bundle:nil];
            [completeSurveyVC setSurveyInfo:surveyInfo];
            [completeSurveyVC setBlock:self.block];
            [self.navigationController pushViewController:completeSurveyVC animated:YES];
        }
    }
//    else
//    {
//        [CustomAlertView iOSAlert:@"" withMessage:CMessageSelectAnswer onView:self];
//    }
}




#pragma mark -
#pragma mark - XCDYouTubeVideoPlayer Observer

- (void) moviePlayerPlaybackDidFinish:(NSNotification *)notification
{
    MPMovieFinishReason finishReason = [notification.userInfo[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] integerValue];
    NSString *reason = @"Unknown";
    
    switch (finishReason)
    {
        case MPMovieFinishReasonPlaybackEnded:
            reason = @"Playback Ended";
            break;
        case MPMovieFinishReasonPlaybackError:
            reason = @"Playback Error";
            break;
        case MPMovieFinishReasonUserExited:
            reason = @"User Exited";
            break;
    }
    
    NSLog(@"Finish Reason: %@", reason);
}




#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrAnswers.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    
    [self.prototypeCell updateConstraintsIfNeeded];
    [self.prototypeCell layoutIfNeeded];
    
    return [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MIAnswerTableViewCell";
    MIAnswerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    Answers *answer = arrAnswers[indexPath.row];
    if (answer.isSelected)
        [tblAnswers selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    return cell;
}

- (MIAnswerTableViewCell *)prototypeCell
{
    static NSString *identifier = @"MIAnswerTableViewCell";
    
    if (!_prototypeCell)
        _prototypeCell = [tblAnswers dequeueReusableCellWithIdentifier:identifier];
    
    return _prototypeCell;
}

- (void)configureCell:(MIAnswerTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (hasMultipleAnswers) {
        [cell.btnRadiobox setHidden:YES];
        [cell.btnCheckbox setHidden:NO];
    } else {
        [cell.btnRadiobox setHidden:NO];
        [cell.btnCheckbox setHidden:YES];
    }
    
    Answers *answer = arrAnswers[indexPath.row];
    [cell.lblAnswer setText:answer.answerText];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Answers *answer = arrAnswers[indexPath.row];
    [answer setIsSelected:YES];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Answers *answer = arrAnswers[indexPath.row];
    [answer setIsSelected:NO];
}

@end

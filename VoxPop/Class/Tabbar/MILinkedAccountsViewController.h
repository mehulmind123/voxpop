//
//  MILinkedAccountsViewController.h
//  VoxPop
//
//  Created by Mac-00014 on 05/01/18.
//  Copyright © 2018 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MILinkedAccountsViewController : MIParentViewController
{
    IBOutlet UITableView *tblAccounts;
    IBOutlet NSLayoutConstraint *cnHeightTblAccounts;
    IBOutlet UIView *viewBackground;
    IBOutlet UIView *viewTblBackground;
}
@end

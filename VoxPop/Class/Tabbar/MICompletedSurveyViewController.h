//
//  MICompletedSurveyViewController.h
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MICompletedSurveyViewController : MIParentViewController
{
    IBOutlet UIView *viewQAContainer;
    IBOutlet UILabel *lblSurveyTitle;
    IBOutlet UITableView *tblPreview;
    
    IBOutlet UIButton *btnEditAnswer;
    IBOutlet UIButton *btnSubmit;
    
    IBOutlet NSLayoutConstraint *cnHeightTblPreview;
}

@property (nonatomic, strong) MISurvey *surveyInfo;
@end

//
//  MICommentsViewController.m
//  VoxPop
//
//  Created by mac-0007 on 24/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MICommentsViewController.h"

#import "MICommentTableViewCell.h"

@interface MICommentsViewController ()
{
    NSMutableArray *arrComments;
    NSString *timeStamp;
    
    NSURLSessionDataTask *apiDataTask;
    
    UIRefreshControl *refreshControl;
}
@end

@implementation MICommentsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"COMMENTS";
    
    timeStamp = @"0";
    
    arrComments = [NSMutableArray new];
    
    [lblNotAllowed setHidden:[self.iObject booleanForKey:@"can_comment"]];
    
    //...Comment View
    [viewComment.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [viewComment.layer setShadowOffset:CGSizeMake(0, -4)];
    [viewComment.layer setShadowOpacity:0.8];
    
    [txtVComment setPlaceholder:@"Type a comment..."];
    [txtVComment setPlaceholderColor:ColorBlack_202020];
    [txtVComment setFont:CFontRobotoRegular(13)];
    [txtVComment setTextColor:ColorBlack_202020];
    [txtVComment setMaxNumberOfLines:4];
    [txtVComment setMinNumberOfLines:1];
    [txtVComment setAnimateHeightChange:YES];
    [txtVComment setDelegate:self];
    [txtVComment setBackgroundColor:[UIColor clearColor]];
    [txtVComment.internalTextView setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtVComment.layer setBorderWidth:0];
    [txtVComment.layer setBorderColor:[UIColor clearColor].CGColor];
    
    
    
    //...UITableView
    [tblComments registerNib:[UINib nibWithNibName:@"MICommentTableViewCell" bundle:nil] forCellReuseIdentifier:@"MICommentTableViewCell"];
    [tblComments setRowHeight:UITableViewAutomaticDimension];
    [tblComments setEstimatedRowHeight:100];
    
    
    //...Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    //...UIRefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblComments addSubview:refreshControl];
    
    //...
    [self loadDataFromServer];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat height = MIN(keyboardSize.height, keyboardSize.width);
    cnBottomCommentView.constant = height;
    [viewComment.superview layoutIfNeeded];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    cnBottomCommentView.constant = 0;
    [viewComment.superview layoutIfNeeded];
}


#pragma mark -
#pragma mark - Load Data

- (void)pullToRefresh
{
    timeStamp = @"0";
    
    [self loadDataFromServer];
}

- (void)loadDataFromServer
{
    if([timeStamp isEqualToString:@"0"] && !arrComments.count && !refreshControl.refreshing)
    {
        [self startLoadingAnimationInView:tblComments];
    }
    
    
    if (apiDataTask && apiDataTask.state == NSURLSessionTaskStateRunning)
        [apiDataTask cancel];
    
    
    apiDataTask =  [[APIRequest request] getSurveyCommentListWithSurveyId:[self.iObject numberForJson:@"id"] timestamp:timeStamp completed:^(id responseObject, NSError *error)
     {
         
         [refreshControl endRefreshing];
         
         //... For Valid Data
         if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
         {
             if([timeStamp isEqualToString:@"0"])
                 [arrComments removeAllObjects];
             
             [arrComments addObjectsFromArray:[responseObject valueForKey:CJsonData]];
             [tblComments reloadData];
             
             timeStamp = [[arrComments lastObject] stringValueForJSON:@"created"];
         }
         else if([[APIRequest request] isJSONStatusValidWithResponse:responseObject] && [timeStamp isEqualToString:@"0"])
         {
             [self showNoDataFoundInView:tblComments];
             [arrComments removeAllObjects];
             [tblComments reloadData];
             
         }
         else
             timeStamp = @"0";
         
         
         
         if(!arrComments.count)
         {
             // ... for Data not avaialbe and Error
             [self stopLoadingAnimationInView:tblComments type:error ?StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound touchUpInsideClickedEvent:^{
                 
                 [self loadDataFromServer];
             }];
             
         }
         
         else
         {
             // Stop loader
             [self removeNodataFoundInView:tblComments];
             [self stopLoadingAnimationInView:tblComments type:StopAnimationTypeRemove touchUpInsideClickedEvent:nil];
         }
         
         
     }];

}


#pragma mark -
#pragma mark - Action Event

- (IBAction)btnPostCommentClicked:(UIButton *)sender
{
    if (txtVComment.text.isBlankValidationPassed)
    {
        [[APIRequest request] addSurveyCommentWithSurveyId:[self.iObject numberForJson:@"id"] comment:txtVComment.text completed:^(id responseObject, NSError *error)
        {
            if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
            {
                [arrComments insertObject:[responseObject valueForKey:CJsonData] atIndex:0];
                [tblComments reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                [self stopLoadingAnimationInView:tblComments type:StopAnimationTypeRemove touchUpInsideClickedEvent:nil];
                [self removeNodataFoundInView:tblComments];
                [txtVComment resignFirstResponder];
            }
            
        }];
        [txtVComment setText:@""];
    }
}



#pragma mark -
#pragma mark - HPGrowingTextView Delegate

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    cnHeightCommentTextView.constant = MAX(35, height);
    [txtVComment layoutIfNeeded];
}

- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView
{
}



#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrComments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *comment = arrComments[indexPath.row];
    
    NSString *identifier = @"MICommentTableViewCell";
    MICommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    [cell.lblUsername setText:comment[@"user_name"]];
    [cell.lblTime setText:[NSDate timeAgoFromTimestamp:[comment stringValueForJSON:@"created"]]];
    [cell.lblComment setText:comment[@"comment"]];
    
    if ([comment integerForKey:@"user_id"] == appDelegate.loginUser.user_id)
    {
        [cell.btnDelete setHidden:NO];
        [cell.btnReport setHidden:YES];
    }
    else
    {
        [cell.btnDelete setHidden:YES];
        [cell.btnReport setHidden:NO];
    }
    
    
    //...Clicked Event
    [cell.btnDelete touchUpInsideClicked:^{
        [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:CMessageDeleteCommentTitle message:CMessageDeleteCommentMessage firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
         {
             [[APIRequest request] deleteComment:[comment numberForJson:@"id"] completed:^(id responseObject, NSError *error)
             {
                 if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     [arrComments removeObjectAtIndex:indexPath.row];
                     [tblComments deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                      [tblComments performSelector:@selector(reloadData) withObject:nil afterDelay:0.3];
                     
                     if(!arrComments.count)
                     {
                         [self showNoDataFoundInView:tblComments];
                     }
                 }
                 
             }];
             
         } secondButton:@"No" secondHandler:nil inView:self];
    }];
    
    [cell.btnReport touchUpInsideClicked:^{
        
        [appDelegate openMailComposerWithEmailId:@"voxpop@gmail.com" subject:@"Report spam" content:@"content" viewController:self];

    }];
    
    if(indexPath.row == arrComments.count-1 && ![timeStamp isEqualToString:@"0"])
    {
        [self loadDataFromServer];
    }
    return cell;
}

@end

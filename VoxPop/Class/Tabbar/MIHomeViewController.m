//
//  MIHomeViewController.m
//  VoxPop
//
//  Created by mac-0007 on 22/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIHomeViewController.h"
#import "MICommentsViewController.h"
#import "MIStartSurveyViewController.h"
#import "MISurveyResultViewController.h"

#import "MISurveyTableViewCell.h"

#import "SurveyPremiseVC.h"

@interface MIHomeViewController ()
{
    NSMutableArray *arrSurveys;
    NSString *timeStamp;
    
    NSURLSessionDataTask *apiDataTask;
    
    UIRefreshControl *refreshControl;
    
    BOOL needToRefreshOnCurrentViewAppear;
}
@end

@implementation MIHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (needToRefreshOnCurrentViewAppear) [self pullToRefresh];
    else needToRefreshOnCurrentViewAppear = YES;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize {
    
    self.title = @"NEW SURVEYS";
    
    needToRefreshOnCurrentViewAppear = YES;
    timeStamp = @"0";
    
    arrSurveys = [NSMutableArray new];
   
    
    //...UITableView
    [tblSurvey registerNib:[UINib nibWithNibName:@"MISurveyTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISurveyTableViewCell"];
    [tblSurvey setRowHeight:UITableViewAutomaticDimension];
    [tblSurvey setEstimatedRowHeight:500];
    [tblSurvey setContentInset:UIEdgeInsetsMake(8, 0, 30, 0)];
    
    //...UIRefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblSurvey addSubview:refreshControl];
}

#pragma mark -
#pragma mark - Load Data

- (void)pullToRefresh
{
    timeStamp = @"0";
    [self loadNewSurveyDataFromServer];
}
- (void)loadNewSurveyDataFromServer
{
    
    if([timeStamp isEqualToString:@"0"] && !arrSurveys.count && !refreshControl.refreshing)
    {
        [self startLoadingAnimationInView:tblSurvey];
    }
    
    
    if (apiDataTask && apiDataTask.state == NSURLSessionTaskStateRunning)
        [apiDataTask cancel];
    
    
    apiDataTask =  [[APIRequest request] getNewSurveyWithTimestamp:timeStamp completed:^(id responseObject, NSError *error)
                    {
                        [refreshControl endRefreshing];
                     
                        
                        if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                        {
                            if([timeStamp isEqualToString:@"0"])
                                [arrSurveys removeAllObjects];
                            
                            [arrSurveys addObjectsFromArray:[responseObject valueForKey:CJsonData]];
                            timeStamp =  [[arrSurveys lastObject] stringValueForJSON:@"created"];
                            [tblSurvey reloadData];
                        }
                        else if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject] && [timeStamp isEqualToString:@"0"])
                        {
                            
                            [arrSurveys removeAllObjects];
                            [tblSurvey reloadData];
                            [self showNoDataFoundInView:tblSurvey];
                        }
                        else
                            timeStamp = @"0";
                        
                        
                        if(!self->arrSurveys.count)
                        {
                            // ... For new Survey
                            [self stopLoadingAnimationInView:tblSurvey type:error ?StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound touchUpInsideClickedEvent:^{
                                
                                [self loadNewSurveyDataFromServer];
                            }];
                            
                        }
                        
                        else
                        {
                            [self removeNodataFoundInView:self->tblSurvey];
                            [self stopLoadingAnimationInView:self->tblSurvey type:StopAnimationTypeRemove touchUpInsideClickedEvent:nil];
                        }
                        
                        
                        [appDelegate.tabBarViewController.tabBarView.viewDot setHidden:!self->arrSurveys.count];
                        
                    }];
}



#pragma mark -
#pragma mark - UITableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSurveys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MISurveyTableViewCell";
    MISurveyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

    cell.btnPremise.hidden = YES;
    NSDictionary *dict = arrSurveys[indexPath.row];
    [cell.lblExpiration setText:[NSString stringWithFormat:@"Expires on: %@", [NSDate stringFromTimeStamp:[dict doubleForKey:@"expired_on"] withFormat:@"MM/dd/yyyy hh:mm a"]]];
    [cell configureSurveyWithData:dict andIndexPath:indexPath];
    [cell.btnSurvey setTitle:@"OPEN SURVEY" forState:UIControlStateNormal];
    [cell.lblResponded setHidden:YES];
    [cell.btnRefresh setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    
    if (![[dict stringValueForJSON:@"survey_premise"] isEqualToString:@""]) {
        cell.btnPremise.hidden = NO;
        // ...Clicked event of button Premise
        [cell.btnPremise touchUpInsideClicked:^{
            
            SurveyPremiseVC *surveyPremiseVC = [SurveyPremiseVC initWithXib];
            surveyPremiseVC.strSurveyPremise = [dict stringValueForJSON:@"survey_premise"];
            surveyPremiseVC.dictOpenSurveyViewResult = dict;
            [self.navigationController pushViewController:surveyPremiseVC animated:YES];
            //        [self presentViewController:surveyPremiseVC animated:YES completion:nil];
        }];
    }
    
    //...Clicked Event
    [cell.btnBlockCustomer touchUpInsideClicked:^{
        [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:CMessageBlockCustomerTitle message:CMessageBlockCustomerMessage firstButton:@"Yes" firstHandler:^(UIAlertAction *action)
         {
             [[APIRequest request] blockUnblockUser:[dict[@"subscriber"] numberForJson:@"id"] status:YES completed:^(id responseObject, NSError *error)
              {
                  if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                  {
                      
                      NSIndexSet *indexSet = [self->arrSurveys indexesOfObjectsPassingTest:^(id obj, NSUInteger idx, BOOL *stop)
                                              {
                                                  return [obj[@"subscriber"][@"id"] isEqual:dict[@"subscriber"][@"id"]];
                                              }];
                      
                      [self->arrSurveys removeObjectsAtIndexes:indexSet];
                      
                      NSMutableArray *allIndexPaths = [[NSMutableArray alloc] init];
                      
                      [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop)
                       {
                           NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx inSection:0];
                           [allIndexPaths addObject:indexPath];
                       }];
                      
                      
                      [self->tblSurvey deleteRowsAtIndexPaths:allIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                      [self->tblSurvey performSelector:@selector(reloadData) withObject:nil afterDelay:0.3];
                      
                      if (self->arrSurveys.count == 0)
                          [self showNoDataFoundInView:self->tblSurvey];
                      
                      [appDelegate.tabBarViewController.tabBarView.viewDot setHidden:!self->arrSurveys.count];
                      
                  }
              }];             
             
             
         } secondButton:@"No" secondHandler:^(UIAlertAction *action)
         {
             
         } inView:self];
    }];
    
    [cell.btnSurvey touchUpInsideClicked:^{
        
        needToRefreshOnCurrentViewAppear = NO;
        MIStartSurveyViewController *startSurveyVC = [[MIStartSurveyViewController alloc] initWithNibName:@"MIStartSurveyViewController" bundle:nil];
        [startSurveyVC setIObject:[dict numberForJson:@"id"]];
        [startSurveyVC setBlock:^(id object, NSError *error)
        {
            UINavigationController *navigation = [appDelegate.tabBarViewController.viewControllers objectAtIndex:1];
            
            MISurveyResultViewController *resultVC = [[MISurveyResultViewController alloc] initWithNibName:@"MISurveyResultViewController" bundle:nil];
            [resultVC setIObject:[dict numberForJson:@"id"]];
            [navigation pushViewController:resultVC animated:NO];
            
            [appDelegate.tabBarViewController.tabBarView.btnTab2 sendActionsForControlEvents:UIControlEventTouchUpInside];
            needToRefreshOnCurrentViewAppear = YES;
            
        }];
        
        [self presentViewController:[UINavigationController navigationControllerWithRootViewController:startSurveyVC] animated:YES completion:nil];
    }];
    
    [cell.btnComments touchUpInsideClicked:^{
        
//        [CustomAlertView iOSAlert:@"" withMessage:CMessageUnableToPostComment onView:self];
//        return ;
        needToRefreshOnCurrentViewAppear = NO;
        
        MICommentsViewController *commentsVC = [[MICommentsViewController alloc] initWithNibName:@"MICommentsViewController" bundle:nil];
        [commentsVC setIObject:@{@"id":[dict numberForJson:@"id"],
                                 @"can_comment":[dict numberForJson:@"can_comment"]}];
        [self.navigationController pushViewController:commentsVC animated:YES];
    }];
    
    
    if(indexPath.row == arrSurveys.count - 1 && ![timeStamp isEqualToString:@"0"])
        [self loadNewSurveyDataFromServer];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



@end

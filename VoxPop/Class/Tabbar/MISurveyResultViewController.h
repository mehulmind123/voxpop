//
//  MISurveyResultViewController.h
//  VoxPop
//
//  Created by mac-0007 on 28/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIParentViewController.h"

@interface MISurveyResultViewController : MIParentViewController
{
    IBOutlet UIButton *btnRefresh;
    IBOutlet UICollectionView *collVQA;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIButton *btnComments;
    IBOutlet UILabel *lblResponded;
}
@end

//
//  MIBarChartView.m
//  VoxPop
//
//  Created by mac-0007 on 28/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIBarChartView.h"
#import "MIBarCollectionViewCell.h"

@interface MIBarChartView ()

@end

@implementation MIBarChartView

+ (id)barChartViewWithAnswers:(NSArray *)answers
{
    MIBarChartView *customView = [[[NSBundle mainBundle] loadNibNamed:@"MIBarChartView" owner:nil options:nil] lastObject];

    [customView setArrAnswers:answers];
    [customView setArrColors:@[Color1_5B7EF4,
                               Color2_F671D2,
                               Color3_3FCF7D,
                               Color4_F66CAD,
                               Color5_F69016,
                               Color6_2B2B2B,
                               Color7_F42A67,
                               Color8_F8CA04,
                               Color9_6164CB,
                               Color10_2012BB]];
    
    //...UICollectionView
    [customView.collVBar registerNib:[UINib nibWithNibName:@"MIBarCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIBarCollectionViewCell"];
    
    return customView;
}


#pragma mark -
#pragma mark - UICollectionView DataSource and Delagate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrAnswers.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = CViewWidth(self)/self.arrAnswers.count;
    return CGSizeMake(width, CViewHeight(self));
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MIBarCollectionViewCell";
    MIBarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.lblPercentage.textColor = cell.viewBar.backgroundColor = _arrColors[indexPath.row];
    
    NSDictionary *dictAnswer = self.arrAnswers[indexPath.item];
    [cell setBarValue:[dictAnswer floatForKey:@"percentage"] withIndexPath:indexPath];
    
    return cell;
}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    MIBarCollectionViewCell *barCell = (MIBarCollectionViewCell *)cell;
//}

@end

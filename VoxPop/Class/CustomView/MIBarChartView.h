//
//  MIBarChartView.h
//  VoxPop
//
//  Created by mac-0007 on 28/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIBarChartView : UIView
@property (nonatomic, weak) IBOutlet UICollectionView *collVBar;
@property (nonatomic, strong) NSArray *arrAnswers;
@property (nonatomic, strong) NSArray *arrColors;
+ (id)barChartViewWithAnswers:(NSArray *)answers;
@end

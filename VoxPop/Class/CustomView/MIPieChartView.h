//
//  MIPieChartView.h
//  VoxPop
//
//  Created by mac-0007 on 01/09/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"

@interface MIPieChartView : UIView <XYPieChartDelegate, XYPieChartDataSource>
@property (nonatomic, weak) IBOutlet UIView *viewPie;
@property (nonatomic, weak) IBOutlet UITableView *tblAnswers;
//@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cnHeightTblAnswers;
@property (nonatomic, strong) XYPieChart *pieChart;
@property (nonatomic, strong) NSArray *arrAnswers;
@property (nonatomic, strong) NSArray *arrColors;
+ (id)pieChartViewWithAnswers:(NSArray *)answers;
@end

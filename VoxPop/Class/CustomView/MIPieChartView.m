//
//  MIPieChartView.m
//  VoxPop
//
//  Created by mac-0007 on 01/09/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIPieChartView.h"
#import "MIPieAnsTableViewCell.h"

@interface MIPieChartView ()

@end

@implementation MIPieChartView

+ (id)pieChartViewWithAnswers:(NSArray *)answers
{
    MIPieChartView *customView = [[[NSBundle mainBundle] loadNibNamed:@"MIPieChartView" owner:nil options:nil] lastObject];
    [customView setArrAnswers:answers];
    [customView setArrColors:@[Color1_5B7EF4,
                               Color2_F671D2,
                               Color3_3FCF7D,
                               Color4_F66CAD,
                               Color5_F69016,
                               Color6_2B2B2B,
                               Color7_F42A67,
                               Color8_F8CA04,
                               Color9_6164CB,
                               Color10_2012BB]];
    
    
    //...PieChart
    customView.pieChart = [[XYPieChart alloc] initWithFrame:CGRectZero];
    [customView.pieChart setTranslatesAutoresizingMaskIntoConstraints:NO];
    [customView.pieChart setShowPercentage:YES];
    [customView.pieChart setLabelFont:CFontRobotoBold(12)];
    [customView.pieChart setDelegate:customView];
    [customView.pieChart setDataSource:customView];
    [customView.viewPie addSubview:customView.pieChart];
    [customView.pieChart reloadData];
    
    
    
    [customView.viewPie addConstraint:[NSLayoutConstraint constraintWithItem:customView.pieChart attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:customView.viewPie attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [customView.viewPie addConstraint:[NSLayoutConstraint constraintWithItem:customView.pieChart attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:customView.viewPie attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];
    [customView.viewPie addConstraint:[NSLayoutConstraint constraintWithItem:customView.pieChart attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:customView.viewPie attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
    [customView.viewPie addConstraint:[NSLayoutConstraint constraintWithItem:customView.pieChart attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:customView.viewPie attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
    
    
//    //...UITableView
//    [customView.tblAnswers registerNib:[UINib nibWithNibName:@"MIPieAnsTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIPieAnsTableViewCell"];
//    [customView.tblAnswers reloadData];
//    [customView.cnHeightTblAnswers setConstant:customView.tblAnswers.contentSize.height];
    
    return customView;
}






# pragma mark
# pragma mark - PIE Chart //........DataSource.........

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return _arrAnswers.count;
}

- (double)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    NSDictionary *dictAnswer = self.arrAnswers[index];
    return [dictAnswer doubleForKey:@"value"];
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    return _arrColors[index];
}






//#pragma mark -
//#pragma mark - UITableView Delegate & Datasource
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return _arrAnswers.count;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 15;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *identifier = @"MIPieAnsTableViewCell";
//    MIPieAnsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    [cell.lblAns setText:[NSString stringWithFormat:@"Ans %ld", indexPath.row + 1]];
//    
//    return cell;
//}

@end

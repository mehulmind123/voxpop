//
//  SurveyPremiseVC.m
//  VoxPop
//
//  Created by mac-00018 on 09/12/19.
//  Copyright © 2019 Jignesh-0007. All rights reserved.
//

#import "SurveyPremiseVC.h"
#import "MIStartSurveyViewController.h"
#import "MISurveyResultViewController.h"

@interface SurveyPremiseVC () {
    NSMutableArray *arrSurveys;
     BOOL needToRefreshOnCurrentViewAppear;
}

@end

@implementation SurveyPremiseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//
//    if (@available(iOS 13.0, *)) {
//        self.navigationController.navigationBar.translucent = YES;
//        self.navigationController.view.backgroundColor = [UIColor clearColor];
//        self.navigationController.navigationBar.hidden = YES;
//    }
//}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = @"SURVEY PREMISE";
    [_viewBack.layer setCornerRadius:12];
    [_btnOpenSurvey.layer setCornerRadius:CViewHeight(_btnOpenSurvey)/2];
    _lblSurveyDetails.text = _strSurveyPremise;
    
    [self.scrollView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [self.scrollView.layer setShadowOffset:CGSizeMake(0, 0)];
    [self.scrollView.layer setShadowOpacity:0.8f];
    [self.scrollView.layer setShadowRadius:5];
    [self.scrollView.layer setCornerRadius:10];
    
    if (_isFromMySurveyOrCompleted) {
        [_btnOpenSurvey setTitle:@"VIEW RESULT" forState:UIControlStateNormal];
    } else {
        [_btnOpenSurvey setTitle:@"OPEN SURVEY" forState:UIControlStateNormal];
    }
    
  
}

- (IBAction)btnOpenSurveyAction:(id)sender {
    
   if ([_btnOpenSurvey.titleLabel.text isEqualToString:@"VIEW RESULT"]){
        
        needToRefreshOnCurrentViewAppear = NO;
        MISurveyResultViewController *resultVC = [[MISurveyResultViewController alloc] initWithNibName:@"MISurveyResultViewController" bundle:nil];
        [resultVC setIObject:[_dictOpenSurveyViewResult numberForJson:@"id"]];
        [self.navigationController pushViewController:resultVC animated:YES];
        
    } else {
         
        needToRefreshOnCurrentViewAppear = NO;
        MIStartSurveyViewController *startSurveyVC = [[MIStartSurveyViewController alloc] initWithNibName:@"MIStartSurveyViewController" bundle:nil];
        [startSurveyVC setIObject:[_dictOpenSurveyViewResult numberForJson:@"id"]];
        [startSurveyVC setBlock:^(id object, NSError *error)
        {
            [self.navigationController popViewControllerAnimated:true];
            
            UINavigationController *navigation = [appDelegate.tabBarViewController.viewControllers objectAtIndex:1];
            
            
            MISurveyResultViewController *resultVC = [[MISurveyResultViewController alloc] initWithNibName:@"MISurveyResultViewController" bundle:nil];
            [resultVC setIObject:[_dictOpenSurveyViewResult numberForJson:@"id"]];
            [navigation pushViewController:resultVC animated:NO];
            
            [appDelegate.tabBarViewController.tabBarView.btnTab2 sendActionsForControlEvents:UIControlEventTouchUpInside];
            needToRefreshOnCurrentViewAppear = YES;
            
           
            
        }];
        
        [self presentViewController:[UINavigationController navigationControllerWithRootViewController:startSurveyVC] animated:YES completion:nil];
    }
}
@end

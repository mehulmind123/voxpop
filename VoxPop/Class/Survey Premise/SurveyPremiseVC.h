//
//  SurveyPremiseVC.h
//  VoxPop
//
//  Created by mac-00018 on 09/12/19.
//  Copyright © 2019 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIParentViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SurveyPremiseVC : MIParentViewController {
     
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblSurveyDetails;

@property (weak, nonatomic) IBOutlet MIGenericView *viewBack;
- (IBAction)btnOpenSurveyAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnOpenSurvey;
@property (weak, nonatomic) NSString *strSurveyPremise;
@property (assign,atomic) BOOL isFromMySurveyOrCompleted;
@property (weak, nonatomic) NSDictionary *dictOpenSurveyViewResult;

@end

NS_ASSUME_NONNULL_END

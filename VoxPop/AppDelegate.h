//
//  AppDelegate.h
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <UserNotifications/UserNotifications.h>

#import "BasicAppDelegate.h"
#import "MITabBarViewController.h"

#import "TBLUser+CoreDataProperties.h"
#import "TBLUser+CoreDataClass.h"

#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

typedef void (^linkedInCallback)(id user, NSError *error);

@interface AppDelegate : BasicAppDelegate <UIApplicationDelegate, UNUserNotificationCenterDelegate>

//....General
- (void)signinUser;
- (void)logoutUser;



//....Root&Main
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) MITabBarViewController *tabBarViewController;
@property (nonatomic, strong) TBLUser *loginUser;
@property (copy, nonatomic) linkedInCallback linkedInCallback;
- (BOOL)isTabBarViewControllerNull;
- (void)setWindowRootVC:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)(BOOL finished))completed;

//....Helper
- (void)loginWithLinkedIn:(linkedInCallback)linkedInCallback;
- (NSURL *)imageURL:(NSString *)url withSize:(CGSize)size;
- (void)enableRemoteNotification;
- (void)openMailComposerWithEmailId:(NSString*)emailId subject:(NSString*)subject content:(NSString*)content viewController:(UIViewController*)viewController;

@end


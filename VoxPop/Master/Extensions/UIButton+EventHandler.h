//
//  UIButton+EventHandler.h
//  MI API Example
//
//  Created by mac-0001 on 12/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TouchUpInsideHandler)(void);
typedef void (^TouchUpInsideEventHandler)(UIButton *button, UIEvent *event);
typedef void (^TouchUpInsideViewHandler)(UIView *view);

@interface UIButton (EventHandler)

-(void)touchUpInsideClicked:(TouchUpInsideHandler)clicked;  // We Can use this on cellForRowAtIndexPath, where tag needs to be set and required on click event to get data.
- (void)touchUpInsideEventHandler:(TouchUpInsideEventHandler)handler;
- (void)touchUpInsideViewHandler:(TouchUpInsideViewHandler)handler;

- (void)setUnderlineHeight:(CGFloat)height andColor:(UIColor *)color;
@end


// To-Do required testing on cellForRowAtIndexPath with deleting and refreshing rows. it still return same nsmanagedobject or not

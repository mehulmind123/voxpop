//
//  UIViewController+Helper.m
//  MI API Example
//
//  Created by mac-0001 on 13/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "UIViewController+Helper.h"
#import "NSObject+NewProperty.h"

#define ToastDisplayTime 3
#define ToastAnimationTime 1

@implementation UIViewController (Helper)

+(UIViewController *)viewController
{
    return [[self alloc] init];
}

+(id)initWithXib
{
    return  [[self alloc] initWithNibName:NSStringFromClass([self class]) bundle:nil];
}
- (BOOL)isVisible
{
    return (self.isViewLoaded && self.view.window);
}

- (BOOL)isDismissed
{
    return ([self isBeingDismissed] || [self isMovingFromParentViewController]);
}

- (BOOL)isPresented
{
    return ([self isBeingPresented] || [self isMovingToParentViewController]);
}

-(void)presentOnTop
{
    [[UIApplication topMostController] presentViewController:self animated:YES completion:nil];
}

- (void)presentPopUp:(UIView *)view from:(PresentType)presentType shouldCloseOnTouchOutside:(BOOL)closable
{
    if (view)
    {
        UIView *underLineView = self.navigationController?self.navigationController.view:self.view;
        MIPopUpOverlay *popUpOverlay = [MIPopUpOverlay popUpOverlay];
        [popUpOverlay.blurView setUnderlyingView:underLineView];
        [popUpOverlay setShouldCloseOnClickOutside:closable];
        [popUpOverlay setPresentType:presentType];
        [underLineView addSubview:popUpOverlay];
        
        if (presentType == PresentTypeCenter)
        {
            view.center = popUpOverlay.center;
            view.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
            [UIView animateWithDuration:0.2 animations:^{
                view.transform = CGAffineTransformIdentity;
            } completion:nil];
            [popUpOverlay addSubview:view];
        }
        else if (presentType == PresentTypeBottom)
        {
            CViewSetY(view, CScreenHeight);
            
            [UIView animateWithDuration:0.2 animations:^{
                CViewSetY(view, CScreenHeight - CViewHeight(view));
            } completion:nil];
            [popUpOverlay addSubview:view];
        }
        else if (presentType == PresentTypeRight)
        {
            CViewSetX(view, CScreenWidth);
            CViewSetY(view, CScreenHeight - CViewHeight(view));
            
            [UIView animateWithDuration:0.2 animations:^{
                CViewSetX(view, 0);
            } completion:nil];
            [popUpOverlay addSubview:view];
        }
    }
}

- (void)dismissPopUp:(UIView *)view
{
    if (view && [view.superview isKindOfClass:[MIPopUpOverlay class]])
    {
        __block MIPopUpOverlay *popUpOverlay = (MIPopUpOverlay *)view.superview;
        
        if (popUpOverlay.presentType == PresentTypeCenter)
        {
            [UIView animateWithDuration:0.1 animations:^{
                view.transform = CGAffineTransformMakeScale(0.01, 0.01);
            } completion:^(BOOL finished)
            {
                [popUpOverlay removeFromSuperview];
                popUpOverlay = nil;
            }];
        }
        else
        {
            [UIView animateWithDuration:0.1 animations:^{
                CViewSetY(view, CScreenHeight);
            } completion:^(BOOL finished)
            {
                [popUpOverlay removeFromSuperview];
                popUpOverlay = nil;
            }];
        }
    }
}

- (void)alertWithAPIErrorTitle:(NSString *)title message:(NSString *)message handler:(void (^)(NSInteger index, NSString *btnTitle))handler
{
    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:title message:message firstButton:@"RETRY" firstHandler:^(UIAlertAction *action)
    {
        if (handler)
            handler (0,@"RETRY");
        
    } secondButton:@"CANCEL" secondHandler:^(UIAlertAction *action)
    {
        if (handler)
            handler (1,@"CANCEL");
        
    } inView:self];
}


@end


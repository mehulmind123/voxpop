//
//  MIPopUpOverlay.h
//  Engage
//
//  Created by mac-0007 on 18/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXBlurView.h"

typedef enum : NSUInteger
{
    PresentTypeBottom,
    PresentTypeCenter,
    PresentTypeRight
} PresentType;

@interface MIPopUpOverlay : UIView

@property (nonatomic, assign) PresentType presentType;

@property (assign, nonatomic) BOOL shouldCloseOnClickOutside;

@property (weak, nonatomic) IBOutlet FXBlurView *blurView;

@property (weak, nonatomic) IBOutlet UIButton *btnClose;

+(id)popUpOverlay;

@end

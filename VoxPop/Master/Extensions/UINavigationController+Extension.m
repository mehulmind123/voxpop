//
//  UINavigationController+Extension.m
//  MI API Example
//
//  Created by mac-0001 on 25/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "UINavigationController+Extension.h"

@implementation UINavigationController (Extension)

+(UINavigationController *)navigationController
{
   return [[self alloc] init];
}

+(UINavigationController *)navigationControllerWithRootViewController:(UIViewController *)rootViewController
{
    return [[self alloc] initWithRootViewController:rootViewController];
}




-(void)viewDidLoad
{


#if NavigationBarShouldHideBackBarButtonText
    self.navigationBar.translucent = NavigationBarTranscluent;
#endif

    
#if NavigationBarShouldHideBackBarButtonText
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
#endif

}

-(BOOL)popToViewControllerOfClass:(Class)class animated:(BOOL)animated
{
    __block UIViewController *toVC;
    
    [self.viewControllers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(UIViewController *obj, NSUInteger idx, BOOL *stop)
    {
        if ([obj isKindOfClass:class])
        {
            toVC = obj;
            *stop = YES;
        }
    }];
    
    if (toVC)
    {
        [self popToViewController:toVC animated:animated];
        return YES;
    }

    return NO;
}


@end

//
//  MIPopUpOverlay.m
//  Engage
//
//  Created by mac-0007 on 18/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "MIPopUpOverlay.h"

@implementation MIPopUpOverlay

+ (id)popUpOverlay
{
    MIPopUpOverlay *popUp = [[[NSBundle mainBundle] loadNibNamed:@"MIPopUpOverlay" owner:nil options:nil] lastObject];
    [popUp.blurView setTintColor:[UIColor clearColor]];
    [popUp.blurView setBlurRadius:2];
    
    CGSize size = [UIScreen mainScreen].bounds.size;
    popUp.frame = CGRectMake(0, 0, size.width, size.height);
    
    return popUp;
}



#pragma mark -
#pragma mark - Action Events

- (IBAction)btnCloseClicked:(UIButton *)sender
{
    if (_shouldCloseOnClickOutside)
    {
        UIView *subView = [self.subviews lastObject];
        if (self.presentType == PresentTypeCenter)
        {
            [UIView animateWithDuration:0.1 animations:^{
                subView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            } completion:^(BOOL finished)
            {
                [self removeFromSuperview];
            }];
        }
        else
        {
            [UIView animateWithDuration:0.1 animations:^{
                CViewSetY(subView, CScreenHeight);
            } completion:^(BOOL finished)
            {
                [self removeFromSuperview];
            }];
        }
    }
}

@end

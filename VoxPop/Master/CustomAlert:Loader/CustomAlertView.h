//
//  CustomAlertView.h
//  EdSmart
//
//  Created by mac-0006 on 15/03/2016.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIView

@property (strong, nonatomic) IBOutlet UILabel *lblAlert;
@property (strong, nonatomic)  NSTimer *timerAlert;

+ (instancetype)sharedInstance;

+ (void)iOSAlert:(NSString *)title withMessage:(NSString *)message onView:(UIViewController *)vc;
+ (void)showStatusBarAlert:(NSString *)message;
+ (void)dismissStatusbarAlert;

+ (void)iOSAlert:(NSString *)title withMessage:(NSString *)message onView:(UIViewController *)vc buttonTitle:(NSString*)buttonTitle handler:(void (^)(UIAlertAction *action))handler;
+ (void)iOSAlertWithTwoButton:(NSString *)title withMessage:(NSString *)message firstButtonTitle:(NSString*)firstButton secondButtonTitle:(NSString*)secondButton onView:(UIViewController *)vc handler:(void (^)(UIAlertAction *action))handler;
@end

//
//  MILoader.m
//  Example
//
//  Created by mac-0007 on 19/03/16.
//  Copyright © 2016 Babulal Poonia. All rights reserved.
//

#import "MILoader.h"

#define ROUND_TIME 1.5
#define DEFAULT_LINE_WIDTH 3.0

static MILoader *miLoader = nil;
static NSInteger count = 0;

@interface MILoader ()

@property (nonatomic, strong) CAShapeLayer *firstLayer;
@property (nonatomic, strong) CAShapeLayer *secondLayer;
@property (nonatomic, strong) CAAnimation *rotationAnimation;
@property (nonatomic) BOOL animating;

@end

@implementation MILoader

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    
    count = 0;
    
    dispatch_once(&onceToken, ^{
        
        miLoader = [[[NSBundle mainBundle]loadNibNamed:@"MILoader" owner:nil options:nil]lastObject];
        
        miLoader.frame = CGRectMake(0, 0, CScreenWidth, CScreenHeight);
        
        [miLoader.containView.layer setCornerRadius:15];
        [miLoader.containView.layer setMasksToBounds:YES];
        
        miLoader.firstLayer = [CAShapeLayer layer];
        [miLoader.loaderView.layer addSublayer:miLoader.firstLayer];
        
        miLoader.secondLayer = [CAShapeLayer layer];
        [miLoader.loaderView.layer addSublayer:miLoader.secondLayer];
        

        miLoader.firstLayer.strokeColor =
        miLoader.secondLayer.strokeColor =
        ColorBlue_3B8DF2.CGColor;
        
        miLoader.firstLayer.lineWidth =
        miLoader.secondLayer.lineWidth =
        DEFAULT_LINE_WIDTH;
        
        miLoader.firstLayer.fillColor =
        miLoader.secondLayer.fillColor = nil;
        
        [miLoader updateAnimations];
    });
    return miLoader;
}


#pragma mark
#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat width = CViewWidth(self.loaderView);
    CGFloat height = CViewHeight(self.loaderView);
    
    CGPoint center = CGPointMake(width / 2.0, height / 2.0);
    CGFloat radius = MIN(width, height) / 2.0 - DEFAULT_LINE_WIDTH / 2.0;
    
    UIBezierPath *firstPath = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:-1.25 endAngle:1.25 clockwise:YES];
    
    UIBezierPath *secondPath = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:1.89 endAngle:4.39 clockwise:YES];
    
    self.firstLayer.path = firstPath.CGPath;
    self.firstLayer.frame = self.bounds;
    
    self.secondLayer.path = secondPath.CGPath;
    self.secondLayer.frame = self.bounds;
}



#pragma mark
#pragma mark -

- (void)startAnimation
{
    if (count == 0)
    {
        _animating = YES;
        
        dispatch_async(GCDMainThread, ^{
            [self.loaderView.layer addAnimation:self.rotationAnimation forKey:@"rotationAnimation"];
            [[[UIApplication sharedApplication] keyWindow] addSubview:self];
        });
    }
    count++;
}

- (void)stopAnimation
{
    count = MAX(count - 1, 0);
    if (count == 0)
    {
        _animating = NO;
        
        dispatch_async(GCDMainThread, ^{
            [self.loaderView.layer removeAnimationForKey:@"rotationAnimation"];
            [self removeFromSuperview];
        });
    }
}

- (BOOL)isAnimating
{
    return _animating;
}

#pragma mark
#pragma mark -

- (void)updateAnimations
{
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotationAnimation.fromValue = @0;
    rotationAnimation.toValue = @(2 * M_PI);
    rotationAnimation.duration = ROUND_TIME;
    rotationAnimation.repeatCount = INFINITY;
    self.rotationAnimation = rotationAnimation;
}

#pragma mark
#pragma mark - Dealloc

- (void)dealloc
{
    [self stopAnimation];
    
    [self.firstLayer removeFromSuperlayer];
    self.firstLayer = nil;
    
    [self.secondLayer removeFromSuperlayer];
    self.secondLayer = nil;
    
    self.rotationAnimation = nil;
}

@end

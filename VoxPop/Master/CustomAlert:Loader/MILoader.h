//
//  MILoader.h
//  Example
//
//  Created by mac-0007 on 19/03/16.
//  Copyright © 2016 Babulal Poonia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MILoader : UIView

@property (nonatomic, strong) IBOutlet UIButton     *btnBack;
@property (nonatomic, strong) IBOutlet UIView       *containView;
@property (nonatomic, strong) IBOutlet UIView       *loaderView;
@property (nonatomic, strong) IBOutlet UILabel      *lblText;

+ (instancetype)sharedInstance;

- (void)startAnimation;
- (void)stopAnimation;
- (BOOL)isAnimating;


@end

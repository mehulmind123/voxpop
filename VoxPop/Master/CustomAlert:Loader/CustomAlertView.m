//
//  CustomAlertView.m
//  EdSmart
//
//  Created by mac-0006 on 15/03/2016.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#import "CustomAlertView.h"
#define DisplayDuration   4
#define AnimateDuration   0.2

static CustomAlertView *selfAlert = nil;

@implementation CustomAlertView

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        selfAlert = [[[NSBundle mainBundle]loadNibNamed:@"CustomAlertView" owner:nil options:nil]lastObject];
        CViewSetWidth(selfAlert, CScreenWidth);
    });
    
    return selfAlert;
}

+ (void)iOSAlert:(NSString *)title withMessage:(NSString *)message onView:(UIViewController *)vc
{
    [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:title message:message buttonTitle:@"Ok" handler:nil inView:vc];
}

+ (void)iOSAlert:(NSString *)title withMessage:(NSString *)message onView:(UIViewController *)vc buttonTitle:(NSString*)buttonTitle handler:(void (^)(UIAlertAction *action))handler
{
    [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:title message:message buttonTitle:buttonTitle handler:handler inView:vc];
}
+ (void)iOSAlertWithTwoButton:(NSString *)title withMessage:(NSString *)message firstButtonTitle:(NSString*)firstButton secondButtonTitle:(NSString*)secondButton onView:(UIViewController *)vc handler:(void (^)(UIAlertAction *action))handler
{
    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:title message:message firstButton:firstButton firstHandler:handler secondButton:secondButton secondHandler:handler inView:vc];
}

+ (void)showStatusBarAlert:(NSString *)message
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
#endif
    [CustomAlertView sharedInstance];

    [selfAlert.timerAlert invalidate];
    selfAlert.timerAlert = nil;
    
    selfAlert.lblAlert.text = message;
    [selfAlert.lblAlert layoutIfNeeded];
    [selfAlert.lblAlert updateConstraintsIfNeeded];
    [selfAlert.lblAlert setPreferredMaxLayoutWidth:(CScreenWidth - 20)];
    
    CGFloat height = [selfAlert systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 20;
    CViewSetHeight(selfAlert, height);

    if (![selfAlert.superview  isKindOfClass:[UIWindow class]])
    {
        selfAlert.alpha = 0;
        
        [UIView animateWithDuration:AnimateDuration animations:^
         {
             selfAlert.alpha = 1;
             [selfAlert layoutIfNeeded];
             [appDelegate.window addSubview:selfAlert];
         } completion:nil];
    }
    
    selfAlert.timerAlert = [NSTimer scheduledTimerWithTimeInterval:DisplayDuration target:selfAlert selector:@selector(hideWithAnimation) userInfo:nil repeats:NO];
}

- (void)hideWithAnimation
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
#endif
    
    
    [UIView animateWithDuration:AnimateDuration animations:^
    {
        selfAlert.lblAlert.text = @"";
        CViewSetHeight(selfAlert, 0);
        [selfAlert layoutIfNeeded];
        
    } completion:^(BOOL finished)
    {
        [selfAlert removeFromSuperview];
    }];
}

+ (void)dismissStatusbarAlert
{
    if (selfAlert)
    {
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
#endif
        [selfAlert removeFromSuperview];
    }
}

@end

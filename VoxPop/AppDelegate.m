//
//  AppDelegate.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "AppDelegate.h"
#import "MILoginViewController.h"
#import <MessageUI/MessageUI.h>
#import <SafariServices/SafariServices.h>
#import "MISFWebViewController.h"
#import "KochavaTracker.h"

@interface AppDelegate ()<MFMailComposeViewControllerDelegate, UNUserNotificationCenterDelegate>

@end
    SFSafariViewController *sfViewController;
@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //....
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    NSLog(@"didFinishLaunchingWithOptions %@",launchOptions);
    [GIDSignIn sharedInstance].clientID = GoogleClientID;
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [self initializeWithLaunchOptions:launchOptions];
    [self initiateApplicationRoot];
    
    [[MIAFNetworking sharedInstance] setDisableTracing:YES];
    if (IS_IPHONE_SIMULATOR)
    {
        [[MIAFNetworking sharedInstance] setDisableTracing:NO];
    }
    
    // kochava configure
    [KochavaTracker.shared configureWithParametersDictionary:@{kKVAParamAppGUIDStringKey: @"kovoxpop-m84qol"} delegate:nil];
    
    //... General data and CMS
    [[APIRequest request] loadGeneralData_completed:nil];
    [[APIRequest request] loadCMS_completed:nil];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    
    if([launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey])
    {
         NSLog(@"UIApplicationLaunchOptionsRemoteNotificationKey %@",launchOptions);
        [self handleDidTapRemoteNotification:[launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
        return NO;
    }
    
     NSLog(@"[[NSTimeZone defaultTimeZone] abbreviation] == %@",[[NSTimeZone defaultTimeZone] abbreviation]);
    
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSDKAppEvents activateApp];
    if(![[UIApplication sharedApplication] isRegisteredForRemoteNotifications] && self.loginUser && [[[UIApplication sharedApplication] currentUserNotificationSettings] types] != UIUserNotificationTypeNone)
    {
        [self enableRemoteNotification];
    }

}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    NSLog(@"openURL: %@", url);
    if ([[url absoluteString] rangeOfString:[NSString stringWithFormat:@"%@://oauth-response?code=",CBundleId]].location!=NSNotFound)
    {
        [self getLinkedInAccessToken:url]; // linkedIn Success
        [sfViewController dismissViewControllerAnimated:NO completion:nil];
        return YES;
    }
    else if ([[url absoluteString] rangeOfString:@"facebook"].location != NSNotFound)
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:[options valueForKey:UIApplicationOpenURLOptionsSourceApplicationKey] annotation:nil];
    }
    else if ([[GIDSignIn sharedInstance] handleURL:url sourceApplication:[options valueForKey:UIApplicationOpenURLOptionsSourceApplicationKey] annotation:nil])
        return YES;
    
    return NO;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[url absoluteString] rangeOfString:[NSString stringWithFormat:@"%@://oauth-response?code=",CBundleId]].location!=NSNotFound)
    {
       // [self getLinkedInAccessToken:url];      // linkedIn Success
        return YES;
    }
    else if ([[url absoluteString] rangeOfString:@"facebook"].location != NSNotFound)
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    else if ([[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation])
    {
        return YES;
    }
    return YES;
}

#pragma mark
#pragma mark - General Method

- (void)initializeWithLaunchOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    //.....Monitoring Internet Reachability.....
    AFNetworkActivityIndicatorManager.sharedManager.enabled = YES;
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         if (status == AFNetworkReachabilityStatusNotReachable)
         {
         }
     }];
    
    
    //.....
}

- (void)initiateApplicationRoot
{

    if ([UIApplication userId])
    {
        NSLog(@"initiateApplicationRoot signinUser");
        appDelegate.loginUser = (TBLUser *)[TBLUser findOrCreate:@{@"user_id":[UIApplication userId]}];
         [[APIRequest request] userProfile_completed:nil];
        [self signinUser];
    }
    else
    {
        MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
        [UIApplication removeUserId];
        [self.window setRootViewController:[UINavigationController navigationControllerWithRootViewController:loginVC]];
    }
}

- (void)signinUser
{
    appDelegate.tabBarViewController = nil;
    [self setWindowRootVC:self.tabBarViewController animated:YES completion:^(BOOL finished)
    {
        [self enableRemoteNotification];
    }];
}

- (void)logoutUser
{
    if ([CUserDefaults valueForKey:UserDefaultDeviceToken])
    {
        [[MILoader sharedInstance] startAnimation];
        [[APIRequest request] deleteDeviceToken:[CUserDefaults valueForKey:UserDefaultDeviceToken] completed:^(id responseObject, NSError *error)
         {
             [[MILoader sharedInstance] stopAnimation];
             
             if([[APIRequest request] isJSONStatusValidWithResponse:responseObject] || !error)
             {
                 //....General Logout
                 appDelegate.loginUser = nil;
                 [TBLCategory deleteAllObjects];
                 [UIApplication removeUserId];
                 FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                 [loginManager logOut];

                 
                 [CUserDefaults removeObjectForKey:UserDefaultLoginToken];
                 [CUserDefaults removeObjectForKey:UserDefaultDeviceToken];
                 [CUserDefaults synchronize];

                 [self disableRemoteNotification];
                 
                 //....
                 MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
                 [appDelegate setWindowRootVC:[UINavigationController navigationControllerWithRootViewController:loginVC] animated:YES completion:^(BOOL finished) {
                     self.tabBarViewController = nil;
                 }];
             }
             
         }];

    }
    else
    {
        //....General Logout
        appDelegate.loginUser = nil;
        [TBLCategory deleteAllObjects];
        [UIApplication removeUserId];
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logOut];
        
        [CUserDefaults removeObjectForKey:UserDefaultLoginToken];
        [CUserDefaults removeObjectForKey:UserDefaultDeviceToken];
        [CUserDefaults synchronize];
        
        [self disableRemoteNotification];
        
        //....
        MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
        [appDelegate setWindowRootVC:[UINavigationController navigationControllerWithRootViewController:loginVC] animated:YES completion:^(BOOL finished) {
            self.tabBarViewController = nil;
        }];
    }
    
}

- (void)loginWithLinkedIn:(linkedInCallback)linkedInCallback
{
    self.linkedInCallback = linkedInCallback;
    NSString const *LinkedInPermissions = @"r_liteprofile%20r_emailaddress%20w_member_social";
    
    NSURL *linkedInUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=%@&state=%@&redirect_uri=%@&scope=%@", CLinkedInClientId, CLinkedInState, CLinkedInRedirectURL,LinkedInPermissions]];
   
    if (IS_Ios9)
    {
        sfViewController = [[SFSafariViewController alloc] initWithURL:linkedInUrl];
        [[UIApplication topMostController]  presentViewController:sfViewController animated:YES completion:nil];
    }
    else
    {
        MISFWebViewController *sfWebViewController = [[MISFWebViewController alloc] initWithNibName:@"MISFWebViewController" bundle:nil];
        sfWebViewController.iObject = linkedInUrl;
        
        [sfWebViewController setBlock:^(id object, NSError *error)
         {
             if (object)
             {
                 [self getLinkedInAccessToken:object];
             }
         }];
        
        [[UIApplication topMostController] presentViewController:[UINavigationController navigationControllerWithRootViewController:sfWebViewController] animated:YES completion:nil];
    }
}

#pragma mark
#pragma mark - Root & Main

- (MITabBarViewController *)tabBarViewController
{
    if (_tabBarViewController == nil)
    {
        _tabBarViewController = [[MITabBarViewController alloc] init];
        [appDelegate.tabBarViewController setSelectedIndex:2];
    }
    
    return _tabBarViewController;
}

- (BOOL)isTabBarViewControllerNull
{
    return (_tabBarViewController == nil);
}

- (void)setWindowRootVC:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)(BOOL finished))completed
{
    [UIView transitionWithView:self.window
                      duration:animated?0.5:0.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
     {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         
         self.window.rootViewController = vc;
         [UIView setAnimationsEnabled:oldState];
     } completion:^(BOOL finished)
     {
         if (completed)
             completed(finished);
     }];
}


#pragma mark
#pragma mark - Helper Method

-(int)secondsFromGMT {
    
   return (int)[[NSTimeZone defaultTimeZone] secondsFromGMT];
    
//    return TimeZone.current.secondsFromGMT()
}

- (NSString *) localTimeZoneAbbreviation {
    
    return [[NSTimeZone defaultTimeZone] abbreviation];
}

- (NSString *) localTimeZoneIdentifier {
    return [[NSTimeZone defaultTimeZone] name];
}

- (NSURL *)imageURL:(NSString *)url withSize:(CGSize)size
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@?width=%d&height=%d", url, (int)size.width*2, (int)size.height*2]];
}

- (void)openMailComposerWithEmailId:(NSString*)emailId subject:(NSString*)subject content:(NSString*)content viewController:(UIViewController*)viewController
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        [mail setMailComposeDelegate:self];
        [mail setSubject:subject];
        [mail setMessageBody:content isHTML:NO];
        [mail setToRecipients:@[emailId]];
        [viewController presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        NSLog(@"This device cannot send email");
    }
}

- (void)getLinkedInAccessToken:(NSURL *)url
{
    [[MILoader sharedInstance] startAnimation];
    
    [[APIRequest request] loginWithLinkedIn:url completed:^(id responseObject, NSError *error) {

        if (responseObject &&  !error)
        {
            NSLog(@"resutl = %@",responseObject);

            [CUserDefaults setObject:[responseObject valueForKey:@"access_token"] forKey:CLinkedInAccessToken];
            [CUserDefaults synchronize];

            [[APIRequest request] getLinkedInUserProfile:^(id responseObject, NSError *error) {
                
                    [[MILoader sharedInstance] stopAnimation];
                
                     if (self.linkedInCallback)
                         self.linkedInCallback(responseObject, error);

            }];
        }
        else
        {
            self.linkedInCallback(responseObject, error);
        }
    }];
}

#pragma mark -
#pragma mark - (APNS)Remote Notification

- (void)enableRemoteNotification
{
    UNAuthorizationOptions authOptions =
    (UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge);
    [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
    }];
    
    // For iOS 10 display notification (sent via APNS)
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)disableRemoteNotification
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Unable to register for remote notifications: %@", error);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"APNs token retrieved: %@", deviceToken);
    
    const unsigned *devTokenBytes = (const unsigned *)[deviceToken bytes];
    NSString *token = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                       ntohl(devTokenBytes[0]), ntohl(devTokenBytes[1]), ntohl(devTokenBytes[2]),
                       ntohl(devTokenBytes[3]), ntohl(devTokenBytes[4]), ntohl(devTokenBytes[5]),
                       ntohl(devTokenBytes[6]), ntohl(devTokenBytes[7])];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Token: %@", token); 
    
    if (token)
    {
        [CUserDefaults setValue:token forKey:UserDefaultDeviceToken];
        [CUserDefaults synchronize];
        [[APIRequest request] addDeviceToken:token completed:nil];
    }
}

- (void)application:(UIApplication *)application
didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings;
{
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        [application registerForRemoteNotifications];
        NSLog(@"didRegisterUser");
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"didReceiveRemoteNotification %@", userInfo);
    [self handleDidTapRemoteNotification:userInfo];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler
{
    NSLog(@"didReceiveNotificationResponseWithCompletionHandler %@", response.notification.request.content.userInfo);
    [self handleDidTapRemoteNotification:response.notification.request.content.userInfo];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog(@"willPresentNotification: %@", notification.request.content.userInfo);

    completionHandler(UNNotificationPresentationOptionAlert |
                      UNNotificationPresentationOptionBadge |
                      UNNotificationPresentationOptionSound);
}

- (void)handleDidTapRemoteNotification:(NSDictionary*)dictInfo
{
    if (_tabBarViewController)
    {
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        if (state == UIApplicationStateActive)
        {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);   //...Vibrate
            AudioServicesPlaySystemSound(1315);   //...Play Sound
            
            [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:nil message:[dictInfo[@"aps"][@"alert"] stringValueForJSON:@"body"] firstButton:@"Cancel" firstHandler:nil secondButton:@"Ok" secondHandler:^(UIAlertAction *action)
            {
                [[UIApplication topMostController] dismissViewControllerAnimated:NO completion:nil];
                
                UINavigationController *nav = (UINavigationController*)[self->_tabBarViewController.viewControllers objectAtIndex:0];
                [nav popToRootViewControllerAnimated:NO];
                [self->_tabBarViewController.tabBarView.btnTab1 sendActionsForControlEvents:UIControlEventTouchUpInside];
                
            } inView:[UIApplication topMostController]];
        }
        else
        {
            [[UIApplication topMostController] dismissViewControllerAnimated:NO completion:nil];
            
            UINavigationController *nav = (UINavigationController*)[_tabBarViewController.viewControllers objectAtIndex:0];
            [nav popToRootViewControllerAnimated:NO];
            [_tabBarViewController.tabBarView.btnTab1 sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
    }
}
#pragma mark
#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    //[[UIApplication topMostController] dismissViewControllerAnimated:YES completion:NULL];
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end

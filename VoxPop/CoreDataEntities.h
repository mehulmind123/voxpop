//
//  CoreDataEntities.h
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#ifndef CoreDataEntities_h
#define CoreDataEntities_h

#import "MISurvey.h"
#import "SurveyQA.h"
#import "Category.h"
#import "SubCategories.h"
#import "Answers.h"
#import "Subscriber.h"


#endif /* CoreDataEntities_h */

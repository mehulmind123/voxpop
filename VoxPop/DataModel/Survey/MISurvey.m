//
//  MISurvey.m
//  VoxPop
//
//  Created by mac-0007 on 19/09/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MISurvey.h"
#import "Category.h"
#import "Subscriber.h"
#import "SurveyQA.h"


NSString *const kSurveyCategory = @"category";
NSString *const kSurveyId = @"id";
NSString *const kSurveyTotalRespondedUsers = @"total_responded_users";
NSString *const kSurveyTitle = @"title";
NSString *const kSurveyExpiredOn = @"expired_on";
NSString *const kSurveyCreated = @"created";
NSString *const kSurveyCanComment = @"can_comment";
NSString *const kSurveyIsCommentEnabled = @"is_comment_enabled";
NSString *const kSurveySubscriber = @"subscriber";
NSString *const kSurveySurveyQA = @"survey_QA";


@interface MISurvey ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MISurvey

@synthesize category = _category;
@synthesize surveyIdentifier = _surveyIdentifier;
@synthesize totalRespondedUsers = _totalRespondedUsers;
@synthesize title = _title;
@synthesize expiredOn = _expiredOn;
@synthesize created = _created;
@synthesize canComment = _canComment;
@synthesize isCommentEnabled = _isCommentEnabled;
@synthesize subscriber = _subscriber;
@synthesize surveyQA = _surveyQA;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.category = [Category modelObjectWithDictionary:[dict objectForKey:kSurveyCategory]];
        self.surveyIdentifier = [[self objectOrNilForKey:kSurveyId fromDictionary:dict] doubleValue];
        self.totalRespondedUsers = [[self objectOrNilForKey:kSurveyTotalRespondedUsers fromDictionary:dict] doubleValue];
        self.title = [self objectOrNilForKey:kSurveyTitle fromDictionary:dict];
        self.expiredOn = [[self objectOrNilForKey:kSurveyExpiredOn fromDictionary:dict] doubleValue];
        self.created = [[self objectOrNilForKey:kSurveyCreated fromDictionary:dict] doubleValue];
        self.canComment = [[self objectOrNilForKey:kSurveyCanComment fromDictionary:dict] doubleValue];
        self.isCommentEnabled = [[self objectOrNilForKey:kSurveyIsCommentEnabled fromDictionary:dict] doubleValue];
        self.subscriber = [Subscriber modelObjectWithDictionary:[dict objectForKey:kSurveySubscriber]];
        NSObject *receivedSurveyQA = [dict objectForKey:kSurveySurveyQA];
        NSMutableArray *parsedSurveyQA = [NSMutableArray array];
        
        if ([receivedSurveyQA isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedSurveyQA) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedSurveyQA addObject:[SurveyQA modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedSurveyQA isKindOfClass:[NSDictionary class]]) {
            [parsedSurveyQA addObject:[SurveyQA modelObjectWithDictionary:(NSDictionary *)receivedSurveyQA]];
        }
        
        self.surveyQA = [NSArray arrayWithArray:parsedSurveyQA];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.category dictionaryRepresentation] forKey:kSurveyCategory];
    [mutableDict setValue:[NSNumber numberWithDouble:self.surveyIdentifier] forKey:kSurveyId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalRespondedUsers] forKey:kSurveyTotalRespondedUsers];
    [mutableDict setValue:self.title forKey:kSurveyTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.expiredOn] forKey:kSurveyExpiredOn];
    [mutableDict setValue:[NSNumber numberWithDouble:self.created] forKey:kSurveyCreated];
    [mutableDict setValue:[NSNumber numberWithDouble:self.canComment] forKey:kSurveyCanComment];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isCommentEnabled] forKey:kSurveyIsCommentEnabled];
    [mutableDict setValue:[self.subscriber dictionaryRepresentation] forKey:kSurveySubscriber];
    NSMutableArray *tempArrayForSurveyQA = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.surveyQA) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSurveyQA addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSurveyQA addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSurveyQA] forKey:kSurveySurveyQA];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.category = [aDecoder decodeObjectForKey:kSurveyCategory];
    self.surveyIdentifier = [aDecoder decodeDoubleForKey:kSurveyId];
    self.totalRespondedUsers = [aDecoder decodeDoubleForKey:kSurveyTotalRespondedUsers];
    self.title = [aDecoder decodeObjectForKey:kSurveyTitle];
    self.expiredOn = [aDecoder decodeDoubleForKey:kSurveyExpiredOn];
    self.created = [aDecoder decodeDoubleForKey:kSurveyCreated];
    self.canComment = [aDecoder decodeDoubleForKey:kSurveyCanComment];
    self.isCommentEnabled = [aDecoder decodeDoubleForKey:kSurveyIsCommentEnabled];
    self.subscriber = [aDecoder decodeObjectForKey:kSurveySubscriber];
    self.surveyQA = [aDecoder decodeObjectForKey:kSurveySurveyQA];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_category forKey:kSurveyCategory];
    [aCoder encodeDouble:_surveyIdentifier forKey:kSurveyId];
    [aCoder encodeDouble:_totalRespondedUsers forKey:kSurveyTotalRespondedUsers];
    [aCoder encodeObject:_title forKey:kSurveyTitle];
    [aCoder encodeDouble:_expiredOn forKey:kSurveyExpiredOn];
    [aCoder encodeDouble:_created forKey:kSurveyCreated];
    [aCoder encodeDouble:_canComment forKey:kSurveyCanComment];
    [aCoder encodeDouble:_isCommentEnabled forKey:kSurveyIsCommentEnabled];
    [aCoder encodeObject:_subscriber forKey:kSurveySubscriber];
    [aCoder encodeObject:_surveyQA forKey:kSurveySurveyQA];
}

- (id)copyWithZone:(NSZone *)zone {
    MISurvey *copy = [[MISurvey alloc] init];
    
    
    
    if (copy) {
        
        copy.category = [self.category copyWithZone:zone];
        copy.surveyIdentifier = self.surveyIdentifier;
        copy.totalRespondedUsers = self.totalRespondedUsers;
        copy.title = [self.title copyWithZone:zone];
        copy.expiredOn = self.expiredOn;
        copy.created = self.created;
        copy.canComment = self.canComment;
        copy.isCommentEnabled = self.isCommentEnabled;
        copy.subscriber = [self.subscriber copyWithZone:zone];
        copy.surveyQA = [self.surveyQA copyWithZone:zone];
    }
    
    return copy;
}


@end

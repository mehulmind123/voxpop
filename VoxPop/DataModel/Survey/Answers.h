//
//  Answers.h
//
//  Created by   on 19/09/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Answers : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double answersIdentifier;
@property (nonatomic, assign) double questionId;
@property (nonatomic, strong) NSString *answerText;
@property (nonatomic, assign) BOOL isSelected;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

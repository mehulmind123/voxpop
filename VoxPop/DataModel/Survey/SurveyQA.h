//
//  SurveyQA.h
//
//  Created by   on 20/09/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SurveyQA : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double questionType;
@property (nonatomic, assign) double surveyQAIdentifier;
@property (nonatomic, strong) NSArray *answers;
@property (nonatomic, strong) NSString *question;
@property (nonatomic, assign) BOOL hasMultipleAnswers;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

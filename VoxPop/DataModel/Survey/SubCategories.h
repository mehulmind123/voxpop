//
//  SubCategories.h
//
//  Created by   on 20/09/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SubCategories : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double subCategoriesIdentifier;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, assign) double subCategoryCount;
@property (nonatomic, assign) BOOL interested;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

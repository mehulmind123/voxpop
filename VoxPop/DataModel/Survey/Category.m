//
//  Category.m
//
//  Created by   on 20/09/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Category.h"
#import "SubCategories.h"


NSString *const kCategoryId = @"id";
NSString *const kCategoryCategoryName = @"category_name";
NSString *const kCategorySubCategoryCount = @"sub_category_count";
NSString *const kCategoryInterested = @"interested";
NSString *const kCategorySubCategories = @"sub_categories";


@interface Category ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Category

@synthesize categoryIdentifier = _categoryIdentifier;
@synthesize categoryName = _categoryName;
@synthesize subCategoryCount = _subCategoryCount;
@synthesize interested = _interested;
@synthesize subCategories = _subCategories;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.categoryIdentifier = [[self objectOrNilForKey:kCategoryId fromDictionary:dict] doubleValue];
            self.categoryName = [self objectOrNilForKey:kCategoryCategoryName fromDictionary:dict];
            self.subCategoryCount = [[self objectOrNilForKey:kCategorySubCategoryCount fromDictionary:dict] doubleValue];
            self.interested = [[self objectOrNilForKey:kCategoryInterested fromDictionary:dict] boolValue];
    NSObject *receivedSubCategories = [dict objectForKey:kCategorySubCategories];
    NSMutableArray *parsedSubCategories = [NSMutableArray array];
    
    if ([receivedSubCategories isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSubCategories) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSubCategories addObject:[SubCategories modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSubCategories isKindOfClass:[NSDictionary class]]) {
       [parsedSubCategories addObject:[SubCategories modelObjectWithDictionary:(NSDictionary *)receivedSubCategories]];
    }

    self.subCategories = [NSArray arrayWithArray:parsedSubCategories];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryIdentifier] forKey:kCategoryId];
    [mutableDict setValue:self.categoryName forKey:kCategoryCategoryName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.subCategoryCount] forKey:kCategorySubCategoryCount];
    [mutableDict setValue:[NSNumber numberWithBool:self.interested] forKey:kCategoryInterested];
    NSMutableArray *tempArrayForSubCategories = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.subCategories) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSubCategories addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSubCategories addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSubCategories] forKey:kCategorySubCategories];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.categoryIdentifier = [aDecoder decodeDoubleForKey:kCategoryId];
    self.categoryName = [aDecoder decodeObjectForKey:kCategoryCategoryName];
    self.subCategoryCount = [aDecoder decodeDoubleForKey:kCategorySubCategoryCount];
    self.interested = [aDecoder decodeBoolForKey:kCategoryInterested];
    self.subCategories = [aDecoder decodeObjectForKey:kCategorySubCategories];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_categoryIdentifier forKey:kCategoryId];
    [aCoder encodeObject:_categoryName forKey:kCategoryCategoryName];
    [aCoder encodeDouble:_subCategoryCount forKey:kCategorySubCategoryCount];
    [aCoder encodeBool:_interested forKey:kCategoryInterested];
    [aCoder encodeObject:_subCategories forKey:kCategorySubCategories];
}

- (id)copyWithZone:(NSZone *)zone {
    Category *copy = [[Category alloc] init];
    
    
    
    if (copy) {

        copy.categoryIdentifier = self.categoryIdentifier;
        copy.categoryName = [self.categoryName copyWithZone:zone];
        copy.subCategoryCount = self.subCategoryCount;
        copy.interested = self.interested;
        copy.subCategories = [self.subCategories copyWithZone:zone];
    }
    
    return copy;
}


@end

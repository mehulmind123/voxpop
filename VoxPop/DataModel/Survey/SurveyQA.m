//
//  SurveyQA.m
//
//  Created by   on 20/09/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "SurveyQA.h"
#import "Answers.h"


NSString *const kSurveyQAQuestionType = @"question_type";
NSString *const kSurveyQAId = @"id";
NSString *const kSurveyQAAnswers = @"answers";
NSString *const kSurveyQAQuestion = @"question";
NSString *const kSurveyQAHasMultipleAnswers = @"has_multiple_answers";


@interface SurveyQA ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SurveyQA

@synthesize questionType = _questionType;
@synthesize surveyQAIdentifier = _surveyQAIdentifier;
@synthesize answers = _answers;
@synthesize question = _question;
@synthesize hasMultipleAnswers = _hasMultipleAnswers;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.questionType = [[self objectOrNilForKey:kSurveyQAQuestionType fromDictionary:dict] doubleValue];
            self.surveyQAIdentifier = [[self objectOrNilForKey:kSurveyQAId fromDictionary:dict] doubleValue];
    NSObject *receivedAnswers = [dict objectForKey:kSurveyQAAnswers];
    NSMutableArray *parsedAnswers = [NSMutableArray array];
    
    if ([receivedAnswers isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedAnswers) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedAnswers addObject:[Answers modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedAnswers isKindOfClass:[NSDictionary class]]) {
       [parsedAnswers addObject:[Answers modelObjectWithDictionary:(NSDictionary *)receivedAnswers]];
    }

    self.answers = [NSArray arrayWithArray:parsedAnswers];
            self.question = [self objectOrNilForKey:kSurveyQAQuestion fromDictionary:dict];
            self.hasMultipleAnswers = [[self objectOrNilForKey:kSurveyQAHasMultipleAnswers fromDictionary:dict] boolValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.questionType] forKey:kSurveyQAQuestionType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.surveyQAIdentifier] forKey:kSurveyQAId];
    NSMutableArray *tempArrayForAnswers = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.answers) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForAnswers addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForAnswers addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForAnswers] forKey:kSurveyQAAnswers];
    [mutableDict setValue:self.question forKey:kSurveyQAQuestion];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasMultipleAnswers] forKey:kSurveyQAHasMultipleAnswers];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.questionType = [aDecoder decodeDoubleForKey:kSurveyQAQuestionType];
    self.surveyQAIdentifier = [aDecoder decodeDoubleForKey:kSurveyQAId];
    self.answers = [aDecoder decodeObjectForKey:kSurveyQAAnswers];
    self.question = [aDecoder decodeObjectForKey:kSurveyQAQuestion];
    self.hasMultipleAnswers = [aDecoder decodeBoolForKey:kSurveyQAHasMultipleAnswers];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_questionType forKey:kSurveyQAQuestionType];
    [aCoder encodeDouble:_surveyQAIdentifier forKey:kSurveyQAId];
    [aCoder encodeObject:_answers forKey:kSurveyQAAnswers];
    [aCoder encodeObject:_question forKey:kSurveyQAQuestion];
    [aCoder encodeBool:_hasMultipleAnswers forKey:kSurveyQAHasMultipleAnswers];
}

- (id)copyWithZone:(NSZone *)zone {
    SurveyQA *copy = [[SurveyQA alloc] init];
    
    
    
    if (copy) {

        copy.questionType = self.questionType;
        copy.surveyQAIdentifier = self.surveyQAIdentifier;
        copy.answers = [self.answers copyWithZone:zone];
        copy.question = [self.question copyWithZone:zone];
        copy.hasMultipleAnswers = self.hasMultipleAnswers;
    }
    
    return copy;
}


@end

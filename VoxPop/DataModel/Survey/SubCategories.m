//
//  SubCategories.m
//
//  Created by   on 20/09/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "SubCategories.h"


NSString *const kSubCategoriesId = @"id";
NSString *const kSubCategoriesCategoryName = @"category_name";
NSString *const kSubCategoriesSubCategoryCount = @"sub_category_count";
NSString *const kSubCategoriesInterested = @"interested";


@interface SubCategories ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SubCategories

@synthesize subCategoriesIdentifier = _subCategoriesIdentifier;
@synthesize categoryName = _categoryName;
@synthesize subCategoryCount = _subCategoryCount;
@synthesize interested = _interested;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.subCategoriesIdentifier = [[self objectOrNilForKey:kSubCategoriesId fromDictionary:dict] doubleValue];
            self.categoryName = [self objectOrNilForKey:kSubCategoriesCategoryName fromDictionary:dict];
            self.subCategoryCount = [[self objectOrNilForKey:kSubCategoriesSubCategoryCount fromDictionary:dict] doubleValue];
            self.interested = [[self objectOrNilForKey:kSubCategoriesInterested fromDictionary:dict] boolValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.subCategoriesIdentifier] forKey:kSubCategoriesId];
    [mutableDict setValue:self.categoryName forKey:kSubCategoriesCategoryName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.subCategoryCount] forKey:kSubCategoriesSubCategoryCount];
    [mutableDict setValue:[NSNumber numberWithBool:self.interested] forKey:kSubCategoriesInterested];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.subCategoriesIdentifier = [aDecoder decodeDoubleForKey:kSubCategoriesId];
    self.categoryName = [aDecoder decodeObjectForKey:kSubCategoriesCategoryName];
    self.subCategoryCount = [aDecoder decodeDoubleForKey:kSubCategoriesSubCategoryCount];
    self.interested = [aDecoder decodeBoolForKey:kSubCategoriesInterested];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_subCategoriesIdentifier forKey:kSubCategoriesId];
    [aCoder encodeObject:_categoryName forKey:kSubCategoriesCategoryName];
    [aCoder encodeDouble:_subCategoryCount forKey:kSubCategoriesSubCategoryCount];
    [aCoder encodeBool:_interested forKey:kSubCategoriesInterested];
}

- (id)copyWithZone:(NSZone *)zone {
    SubCategories *copy = [[SubCategories alloc] init];
    
    
    
    if (copy) {

        copy.subCategoriesIdentifier = self.subCategoriesIdentifier;
        copy.categoryName = [self.categoryName copyWithZone:zone];
        copy.subCategoryCount = self.subCategoryCount;
        copy.interested = self.interested;
    }
    
    return copy;
}


@end

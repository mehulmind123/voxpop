//
//  Subscriber.h
//
//  Created by   on 20/09/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Subscriber : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *contactName;
@property (nonatomic, assign) double subscriberIdentifier;
@property (nonatomic, strong) NSString *profilePicture;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) BOOL isBlock;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

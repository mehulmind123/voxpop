//
//  Subscriber.m
//
//  Created by   on 20/09/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Subscriber.h"


NSString *const kSubscriberContactName = @"contact_name";
NSString *const kSubscriberId = @"id";
NSString *const kSubscriberProfilePicture = @"profile_picture";
NSString *const kSubscriberName = @"name";
NSString *const kSubscriberIsBlock = @"is_block";


@interface Subscriber ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Subscriber

@synthesize contactName = _contactName;
@synthesize subscriberIdentifier = _subscriberIdentifier;
@synthesize profilePicture = _profilePicture;
@synthesize name = _name;
@synthesize isBlock = _isBlock;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.contactName = [self objectOrNilForKey:kSubscriberContactName fromDictionary:dict];
            self.subscriberIdentifier = [[self objectOrNilForKey:kSubscriberId fromDictionary:dict] doubleValue];
            self.profilePicture = [self objectOrNilForKey:kSubscriberProfilePicture fromDictionary:dict];
            self.name = [self objectOrNilForKey:kSubscriberName fromDictionary:dict];
            self.isBlock = [[self objectOrNilForKey:kSubscriberIsBlock fromDictionary:dict] boolValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.contactName forKey:kSubscriberContactName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.subscriberIdentifier] forKey:kSubscriberId];
    [mutableDict setValue:self.profilePicture forKey:kSubscriberProfilePicture];
    [mutableDict setValue:self.name forKey:kSubscriberName];
    [mutableDict setValue:[NSNumber numberWithBool:self.isBlock] forKey:kSubscriberIsBlock];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.contactName = [aDecoder decodeObjectForKey:kSubscriberContactName];
    self.subscriberIdentifier = [aDecoder decodeDoubleForKey:kSubscriberId];
    self.profilePicture = [aDecoder decodeObjectForKey:kSubscriberProfilePicture];
    self.name = [aDecoder decodeObjectForKey:kSubscriberName];
    self.isBlock = [aDecoder decodeBoolForKey:kSubscriberIsBlock];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_contactName forKey:kSubscriberContactName];
    [aCoder encodeDouble:_subscriberIdentifier forKey:kSubscriberId];
    [aCoder encodeObject:_profilePicture forKey:kSubscriberProfilePicture];
    [aCoder encodeObject:_name forKey:kSubscriberName];
    [aCoder encodeBool:_isBlock forKey:kSubscriberIsBlock];
}

- (id)copyWithZone:(NSZone *)zone {
    Subscriber *copy = [[Subscriber alloc] init];
    
    
    
    if (copy) {

        copy.contactName = [self.contactName copyWithZone:zone];
        copy.subscriberIdentifier = self.subscriberIdentifier;
        copy.profilePicture = [self.profilePicture copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.isBlock = self.isBlock;
    }
    
    return copy;
}


@end

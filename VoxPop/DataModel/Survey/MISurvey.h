//
//  MISurvey.h
//  VoxPop
//
//  Created by mac-0007 on 19/09/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Category, Subscriber;

@interface MISurvey : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) Category *category;
@property (nonatomic, assign) double surveyIdentifier;
@property (nonatomic, assign) double totalRespondedUsers;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double expiredOn;
@property (nonatomic, assign) double created;
@property (nonatomic, assign) double canComment;
@property (nonatomic, assign) double isCommentEnabled;
@property (nonatomic, strong) Subscriber *subscriber;
@property (nonatomic, strong) NSArray *surveyQA;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  Answers.m
//
//  Created by   on 19/09/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Answers.h"


NSString *const kAnswersId = @"id";
NSString *const kAnswersQuestionId = @"question_id";
NSString *const kAnswersAnswerText = @"answer_text";
NSString *const kAnswerSelected = @"is_selected";


@interface Answers ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Answers

@synthesize answersIdentifier = _answersIdentifier;
@synthesize questionId = _questionId;
@synthesize answerText = _answerText;
@synthesize isSelected = _isSelected;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.answersIdentifier = [[self objectOrNilForKey:kAnswersId fromDictionary:dict] doubleValue];
        self.questionId = [[self objectOrNilForKey:kAnswersQuestionId fromDictionary:dict] doubleValue];
        self.answerText = [self objectOrNilForKey:kAnswersAnswerText fromDictionary:dict];
        self.isSelected = NO;
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.answersIdentifier] forKey:kAnswersId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.questionId] forKey:kAnswersQuestionId];
    [mutableDict setValue:self.answerText forKey:kAnswersAnswerText];
    [mutableDict setValue:[NSNumber numberWithBool:self.isSelected] forKey:kAnswerSelected];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.answersIdentifier = [aDecoder decodeDoubleForKey:kAnswersId];
    self.questionId = [aDecoder decodeDoubleForKey:kAnswersQuestionId];
    self.answerText = [aDecoder decodeObjectForKey:kAnswersAnswerText];
    self.isSelected = [aDecoder decodeBoolForKey:kAnswerSelected];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_answersIdentifier forKey:kAnswersId];
    [aCoder encodeDouble:_questionId forKey:kAnswersQuestionId];
    [aCoder encodeObject:_answerText forKey:kAnswersAnswerText];
    [aCoder encodeBool:_isSelected forKey:kAnswerSelected];
}

- (id)copyWithZone:(NSZone *)zone {
    Answers *copy = [[Answers alloc] init];
    
    
    
    if (copy) {

        copy.answersIdentifier = self.answersIdentifier;
        copy.questionId = self.questionId;
        copy.answerText = [self.answerText copyWithZone:zone];
        copy.isSelected = self.isSelected;
    }
    
    return copy;
}


@end
